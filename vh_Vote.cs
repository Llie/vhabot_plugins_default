using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Data;
using AoLib.Utils;
using VhaBot;

namespace VhaBot.Plugins
{
    public class VhVote : PluginBase
    {
        private BotShell _bot;

        private Config _database;

        public VhVote()
        {
            this.Name = "Vote";
            this.InternalName = "VhVote";
            this.Version = 200;
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Description = "Manage Anonymous Voting / Elections";
            this.Commands = new Command[] {
                new Command("vote", true, UserLevel.Guest),
                new Command("elections", "vote"),
                new Command("vote start", true, UserLevel.Leader),
                new Command("vote yes", true, UserLevel.Guest),
                new Command("vote no", true, UserLevel.Guest),
                new Command("vote abstain", true, UserLevel.Guest ),
                new Command("vote stop", true, UserLevel.Leader),
                new Command("vote abort", true, UserLevel.Leader),
                new Command("vote clear", "vote abort" )
            };
        }

        public override void OnLoad(BotShell bot)
        {
            this._bot = bot;

            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS elections ( Admin VARCHAR(14), Description VARCHAR(256), ElectionID INTEGER, Quorum INTEGER, Percentage INTEGER, Running BOOLEAN, PRIMARY KEY ( ElectionID AUTOINCREMENT ) )");
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS votes ( Username VARCHAR(14), ElectionID INTEGER, Vote INTEGER, PRIMARY KEY ( Username , ElectionID ) )");

        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            lock (this)
            {
                switch (e.Command)
                {
                    case "vote":
                        this.OnVoteStatusCommand(bot, e);
                        break;
                    case "vote start":
                        this.OnVoteStartCommand(bot, e);
                        break;
                    case "vote stop":
                        this.OnVoteStopCommand(bot, e);
                        break;
                    case "vote abort":
                        this.OnVoteAbortCommand(bot, e);
                        break;
                    case "vote yes":
                    case "vote no":
                    case "vote abstain":
                        this.OnVoteCastCommand(bot, e);
                        break;
                }
            }
        }

        private bool ElectionsRunning()
        {
            int elections_running = 0;
            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM elections WHERE Running = 'true'";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    elections_running++;
                if ( elections_running > 0 )
                    return true;
                else
                    return false;
            }
        }

        private bool Elections()
        {
            int elections = 0;
            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM elections";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    elections++;
                if ( elections > 0 )
                    return true;
                else
                    return false;
            }
        }

        private int LookupElection( int id,
                                    ref string admin, ref string description,
                                    ref int quorum, ref int percentage,
                                    ref bool running )
        {
            int found = 0;
            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM elections WHERE ElectionID = " + Convert.ToString( id );
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    admin = reader.GetString(0);
                    description = reader.GetString(1);
                    quorum = reader.GetInt32(3);
                    percentage = reader.GetInt32(4);
                    running = reader.GetBoolean(5);
                    found ++;
                }
            }
            return found;
        }

        private void Tally( int id, ref int yes, ref int no, ref int abstain )
        {
            yes = 0;
            no = 0;
            abstain = 0;
            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM votes WHERE ElectionID = " + Convert.ToString( id );
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    switch ( reader.GetInt32(2) )
                    {
                    case -1:
                        no ++;
                        break;
                    case 1:
                        yes ++;
                        break;
                    case 0:
                        abstain ++;
                        break;
                    }
                }
            }
        }

        private int VerifyVote( int id, string Username )
        {
            using (IDbCommand command=this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM votes WHERE ElectionID = " + Convert.ToString( id ) + " AND Username = '" + Username + "'";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    return reader.GetInt32(2);
                }
            }
            return -2;
        }

	    private void SendAllChannels( string Message )
        {
            this._bot.SendPrivateChannelMessage( Message );
            this._bot.SendOrganizationMessage( Message );
        }

        private void OnVoteStatusCommand(BotShell bot, CommandArgs e)
        {

            if ( ( bot.Users.GetUser(e.Sender) < UserLevel.Leader ) && 
                 ! ElectionsRunning() )
                bot.SendPrivateMessage(e.SenderID,
                                       "There is nothing to vote on.");
            else if ( ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) && 
                      ! Elections() )
                bot.SendPrivateMessage(e.SenderID,
                                       "There are no elections to view.");
            else
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle( "Elections" );

                using (IDbCommand command=this._database.Connection.CreateCommand())
                {

                    string all_elections = string.Empty;
                    if ( bot.Users.GetUser(e.Sender) < UserLevel.Leader )
                        all_elections = " WHERE Running = 'true'";
                    command.CommandText = "SELECT * FROM elections" +
                            all_elections;

                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string admin = reader.GetString(0);
                        string description = reader.GetString(1);
                        int id = reader.GetInt32(2);
                        int quorum = reader.GetInt32(3);
                        int percentage = reader.GetInt32(4);
                        bool running = reader.GetBoolean(5);

                        window.AppendHighlight( admin + " initiated the following election:");
                        window.AppendLineBreak();
                        window.AppendString( "Topic: " + description);
                        window.AppendLineBreak();

                        int verify = VerifyVote( id, e.Sender );
                        if ( ( verify < -1 ) && running )
                        {
                            window.AppendNormalStart();
                            window.AppendString("[");
                            window.AppendBotCommand("Yes", "vote yes " +
                                                    Convert.ToString(id));
                            window.AppendString("] [");
                            window.AppendBotCommand("No", "vote no " +
                                                    Convert.ToString(id));
                            window.AppendString("] [");
                            window.AppendBotCommand("Abstain",
                                                    "vote abstain " +
                                                    Convert.ToString(id));
                            window.AppendString("]");
                        }

                        if ( running &&
                             ((admin == e.Sender) ||
                              (bot.Users.GetUser(e.Sender)>=UserLevel.Leader)) )
                        {
                            window.AppendString("[");
                            window.AppendBotCommand("Stop", "vote stop " + Convert.ToString(id));
                            window.AppendString("] [");
                            window.AppendBotCommand("Abort", "vote abort " + Convert.ToString(id));
                            window.AppendString("]");
                        }
                        else if ( ! running )
                        {
                            window.AppendString("[Election ended] [");
                            window.AppendBotCommand("Clear", "vote abort " + Convert.ToString(id));
                            window.AppendString("]");
                        }


                        int yes = 0, no = 0, abstain = 0;
                        Tally( id, ref yes, ref no, ref abstain );
                        window.AppendLineBreak();
                        if ( running )
                        {
                            window.AppendString( "Votes cast: " +
                                                 Convert.ToString(yes + no) );
                            window.AppendLineBreak();
                            if ( (quorum > 0) && (yes + no + abstain < quorum) )
                                window.AppendString( "Quorum has not yet been met." );
                            else
                                window.AppendString( "Quorum has been met." );
                            window.AppendLineBreak();
                            window.AppendString("Quorum is currently: " + Convert.ToString ( quorum ) + " with a " + Convert.ToString ( percentage ) + "% approval in order to win this election." );
                        }
                        else if ( ( quorum > 0 ) &&
                                  ( yes + no + abstain < quorum ) )
                            window.AppendString("No Result: Failed to meet quorum." );
                        else
                        {
                            double require = (yes + no) * (percentage / 100.0);
                            if ( yes >= require )
                                window.AppendString("Result: Approved." );
                            else
                                window.AppendString("Result: Rejected." );
                        }
                        window.AppendLineBreak();
                        window.AppendLineBreak();
                    }
                }

                string output = string.Empty;
                if ( bot.Users.GetUser(e.Sender) < UserLevel.Leader )
                    output = "Elections in progress »» " + window.ToString();
                else
                    output = "All elections »» " + window.ToString();

                bot.SendPrivateMessage(e.SenderID, output );
            }
        }

        private void OnVoteStartCommand(BotShell bot, CommandArgs e)
        {
            int quorum = 0;
            int percentage = 0;
            string description = string.Empty;
            if ( e.Words.Length >= 3 )
            {
                if ( ! int.TryParse( e.Args[0], out quorum ) )
                    quorum = 0;
                if ( ! int.TryParse( e.Args[1], out percentage ) )
                {
                    switch ( e.Args[1] )
                    {
                    case "majority":
                        percentage = 51;
                        break;
                    case "supermajority":
                        percentage = 67;
                        break;
                    case "unanimous":
                        percentage = 100;
                        break;
                    }
                }
                if ( percentage == 0 )
                    percentage = 51;
                description = e.Words[2];

                string insert_command =
                    string.Format("INSERT INTO elections ( Admin, Description, Quorum , Percentage, Running ) VALUES ( '{0}', '{1}', {2}, '{3}', 'true' )",
                                  e.Sender, description, quorum, percentage );
                this._database.ExecuteNonQuery( insert_command );
            }
            else
            {
                bot.SendPrivateMessage(e.SenderID, "Usage: /tell " + bot.Character + " vote start [quorum] [requirement] [description]" );
            }

            this.OnVoteStatusCommand(bot, e);
        }

        private void OnVoteAbortCommand(BotShell bot, CommandArgs e)
        {

            if ( e.Words.Length != 1 )
            {
                bot.SendPrivateMessage(e.SenderID, "You did not specify an election ID number.  See active elections using the !vote command" );
                return;
            }

            string admin = string.Empty, description = string.Empty;
            int id = Convert.ToInt32(e.Args[0]), quorum = 0, percentage = 0;
            bool running = false;
            if ( LookupElection(id, ref admin, ref description,
                                ref quorum, ref percentage, ref running )==0 )
            {
                bot.SendPrivateMessage(e.SenderID, "No election ID number: " +
                    e.Args[0] );
                return;
            }
    
            if ( ( admin != e.Sender ) &&
                 ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) )
            {
                bot.SendPrivateMessage(e.SenderID, e.Sender + " was not the person who started the election and/or does not have sufficient privileges to abort the election.");
                return;
            }

            this._database.ExecuteNonQuery("DELETE FROM elections WHERE ElectionID = " + e.Args[0] );
            this._database.ExecuteNonQuery("DELETE FROM votes WHERE ElectionID = " + e.Args[0] );

            SendAllChannels( bot.ColorHighlight + HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) + " has cleared the election for: " + description );
            bot.SendPrivateMessage(e.SenderID, "You cleared the election");
        }

        private void OnVoteCastCommand(BotShell bot, CommandArgs e)
        {

            if ( e.Words.Length != 1 )
            {
                bot.SendPrivateMessage(e.SenderID, "You did not specify an election ID number.  See active elections using the !vote command" );
                return;
            }

            string admin = string.Empty, description = string.Empty;
            int id = Convert.ToInt32(e.Args[0]), quorum = 0, percentage = 0;
            bool running = false;
            if (LookupElection( id, ref admin, ref description,
                                ref quorum, ref percentage, ref running ) == 0)
            {
                bot.SendPrivateMessage(e.SenderID, "No election ID number: " +
                    e.Args[0] );
                return;
            }

            int verify = VerifyVote( id, e.Sender );
            if ( verify >= -1 )
            {
                bot.SendPrivateMessage(e.SenderID, "You have already voted in this election." );
                return;
            }

            int vote = 0;
            switch (e.Command)
            {
            case "vote yes":
                vote = 1;
                break;
            case "vote no":
                vote = -1;
                break;
            case "vote abstain":
                vote = 0;
                break;
            default:
                bot.SendPrivateMessage(e.SenderID, "Error: Unknown vote!" );
                return;
            }

            string insert_command =
                string.Format("INSERT INTO votes ( Username, ElectionID, Vote ) VALUES ( '{0}', {1}, {2} )",
                              e.Sender, id, vote  );
            this._database.ExecuteNonQuery( insert_command );
            bot.SendPrivateMessage(e.SenderID, "Your vote has been cast.");

        }

        private void OnVoteStopCommand(BotShell bot, CommandArgs e)
        {

            if ( e.Words.Length != 1 )
            {
                bot.SendPrivateMessage(e.SenderID, "You did not specify an election ID number.  See active elections using the !vote command" );
                return;
            }

            string admin = string.Empty, description = string.Empty;
            int id = Convert.ToInt32(e.Args[0]), quorum = 0, percentage = 0;
            bool running = false;
            if (LookupElection( id, ref admin, ref description,
                                ref quorum, ref percentage, ref running ) == 0 )
            {
                bot.SendPrivateMessage(e.SenderID, "No election ID number: " +
                    e.Args[0] );
                return;
            }

            if ( ( admin != e.Sender ) &&
                 ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) )
            {
                bot.SendPrivateMessage(e.SenderID, e.Sender + " was not the person who started the vote or does not have sufficient privileges to end the voting.");
                return;
            }

            if ( ! running )
            {
                bot.SendPrivateMessage(e.SenderID, "This election has already ended." );
                return;                
            }

            SendAllChannels( bot.ColorHighlight + "The election has ended for: " + description );
            bot.SendPrivateMessage( e.SenderID, bot.ColorHighlight +"The election has ended for: " + description );

            int yes = 0, no = 0, abstain = 0;
            string result = "no";
            Tally( id, ref yes, ref no, ref abstain );
            if ( ( quorum > 0 ) && ( yes + no + abstain < quorum ) )
            {
                SendAllChannels( bot.ColorHighlight + "The election has been stopped due to lack of quorum." );
                bot.SendPrivateMessage( e.SenderID, bot.ColorHighlight + "The election has been stopped due to lack of quorum." );
            }
            else
            {
                double require = (yes + no) * (percentage / 100.0);
                if ( yes >= require )
                    result = "yes";

                SendAllChannels( bot.ColorHighlight + "Election result is: " + result );
                bot.SendPrivateMessage( e.SenderID, bot.ColorHighlight + "Election result is: " + result );
            }

            this._database.ExecuteNonQuery("UPDATE elections SET Running = 'false' WHERE ElectionID = " + Convert.ToString(id) );
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
            case "vote":
                return "Displays currently running elections and allows you to cast votes in currently running elections.\n\n" +
                    "Usage: /tell " + bot.Character + " vote";

            case "vote start":
                return "Starts an election.  Quorum indicates the minimum number of people that must vote in order for a valid election.  Requirement is the percentage of votes required to win the election.  You may also specify majority, supermajority, or unanimous for the requirement.\n\n" +
                    "Usage: /tell " + bot.Character + " vote start [quorum] [requirement] [description]";
            }
            return null;
        }

    }
}
