using System;
using System.Collections.Generic;
using System.Data;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class Notify : PluginBase
    {
        private bool _sendgc = true;
        private bool _sendpg = true;
        private bool _sendirc = false;
        private bool _showfullname = false;
        private string _displayMode = "medium";
        private Config _database;

        private List<string> _stealth;

        public Notify()
        {
            this.Name = "Notifier";
            this.InternalName = "vhNotify";
            this.Author = "Vhab / Kilmanagh";
            this.Description = "Displays a notification when a member of the bot logs on or off";
            this.Version = 102;
            this.DefaultState = PluginState.Installed;
            this.Commands = new Command[] {
                new Command("stealth", true, UserLevel.Guest),
                new Command("logon", true, UserLevel.Guest),
                new Command("logon clear", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
            bot.Events.UserLogoffEvent += new UserLogoffHandler(UserLogoffEvent);
            bot.Events.ConfigurationChangedEvent +=new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "sendgc", "Send notifications to the organization channel", this._sendgc);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "sendpg", "Send notifications to the private channel", this._sendpg);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "sendirc", "Send notifications to IRC", this._sendirc);
            bot.Configuration.Register(ConfigType.String, this.InternalName, "mode", "Display mode", this._displayMode, "compact", "medium", "large");
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "fullname", "Show full character names", this._showfullname);
            
            this.LoadConfiguration(bot);
            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS logon (Username VARCHAR(14) PRIMARY KEY, Date INTEGER, Message TEXT)");

            
            this._stealth = new List<string>();

        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            bot.Events.UserLogoffEvent -= new UserLogoffHandler(UserLogoffEvent);
            bot.Events.ConfigurationChangedEvent -=new ConfigurationChangedHandler(ConfigurationChangedEvent);
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            if ( !bot.FriendList.IsFriend("notify", e.Sender) &&
                 !bot.FriendList.IsFriend("guestlist", e.Sender ) )
            {
                bot.SendReply(e, "You're required to be on the notify list before using this action");
                return;
            }
            switch (e.Command)
            {

            case "logon":
                if (e.Args.Length < 1)
                {
                    bot.SendReply(e, "Correct Usage: logon [message]");
                    return;
                }
                this._database.ExecuteNonQuery("REPLACE INTO logon VALUES('" + e.Sender + "', " + TimeStamp.Now + ", '" + Config.EscapeString(e.Words[0]) + "')");
                bot.SendReply(e, "Your logon message has set");
                break;

            case "logon clear":
                this._database.ExecuteNonQuery("DELETE FROM logon WHERE Username = '" + e.Sender + "'");
                bot.SendReply(e, "Your logon message has been cleared");
                break;

            case "stealth":
                lock (this._stealth)
                {
                    if (this._stealth.Contains(bot.Users.GetMain(e.Sender)))
                    {
                        this._stealth.Remove( bot.Users.GetMain(e.Sender) );
                        bot.SendReply(e, "Stealth " + HTML.CreateColorString(RichTextWindow.ColorGreen, "disabled") + " for " + bot.Users.GetMain(e.Sender) );
                    }
                    else
                    {
                        this._stealth.Add( bot.Users.GetMain(e.Sender) );
                        bot.SendReply(e, "Stealth " + HTML.CreateColorString(RichTextWindow.ColorRed, "enabled") + " for " + bot.Users.GetMain(e.Sender) );
                    }
                }
                break;

            }
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._sendgc = bot.Configuration.GetBoolean(this.InternalName, "sendgc", this._sendgc);
            this._sendpg = bot.Configuration.GetBoolean(this.InternalName, "sendpg", this._sendpg);
            this._sendirc = bot.Configuration.GetBoolean(this.InternalName, "sendirc", this._sendirc);
            this._displayMode = bot.Configuration.GetString(this.InternalName, "mode", this._displayMode);
            this._showfullname = bot.Configuration.GetBoolean(this.InternalName, "fullname", this._showfullname);
        }

        public string GetUserLogon( string user )
        {
            string logon = string.Empty;
            using (IDbCommand command = this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT Message FROM logon WHERE Username = '" + user + "'";
                IDataReader reader = command.ExecuteReader();
                if (reader.Read()) logon = reader.GetString(0);
                reader.Close();
            }
            return logon;
        }

        private void UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (e.First) return;
            if (!e.Sections.Contains("notify")) return;
            string logon = "";
            string sender = HTML.CreateColorString(bot.ColorHeaderHex, e.Sender);
            string ircsender = "";
            if (e.SenderWhois != null)
            {
                switch (this._displayMode)
                {
                    case "compact":
                        sender = HTML.CreateColorString(bot.ColorHeaderHex, Format.Whois(e.SenderWhois, FormatStyle.Compact, this._showfullname));
                        if (this._sendirc) ircsender = Format.Whois(e.SenderWhois, FormatStyle.Compact, this._showfullname);
                        break;
                    case "large":
                        sender = HTML.CreateColorString(bot.ColorHeaderHex, Format.Whois(e.SenderWhois, FormatStyle.Large, this._showfullname));
                        if (this._sendirc) ircsender = Format.Whois(e.SenderWhois, FormatStyle.Large, this._showfullname);
                        break;
                    default:
                        sender = HTML.CreateColorString(bot.ColorHeaderHex, Format.Whois(e.SenderWhois, FormatStyle.Medium, this._showfullname));
                        if (this._sendirc) ircsender = Format.Whois(e.SenderWhois, FormatStyle.Medium, this._showfullname);
                        break;
                }
            }
            string logon_msg = GetUserLogon( e.Sender );
            if ( ! string.IsNullOrEmpty(logon_msg) )
                logon = " - " + logon_msg;
            string alts = "";
            if (bot.Plugins.IsLoaded("vhMembersViewer"))
            {
                RichTextWindow window = ((MembersViewer)bot.Plugins.GetPlugin("vhMembersViewer")).GetAltsWindow(bot, e.Sender);
                if (window != null)
                    alts = " - " + window.ToString(bot.Users.GetMain(e.Sender) + "'s Alts");
            }
            if (!this._stealth.Contains(bot.Users.GetMain(e.Sender)))
                this.SendNotify(bot, sender + " has logged on" + alts + logon);
            if (this._sendirc &&
                !this._stealth.Contains(bot.Users.GetMain(e.Sender)))
                this.SendIrcNotify(bot, ircsender + " has logged on " + logon);
        }

        private void UserLogoffEvent(BotShell bot, UserLogoffArgs e)
        {
            if (e.First) return;
            if (!e.Sections.Contains("notify")) return;
            if (!this._stealth.Contains(bot.Users.GetMain(e.Sender)))
                this.SendNotify(bot, HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) + " has logged off");
            if (!this._stealth.Contains(bot.Users.GetMain(e.Sender)))
                this.SendIrcNotify(bot, e.Sender + " has logged off");
        }

        private void SendNotify(BotShell bot, string message)
        {
            if (this._sendgc) bot.SendOrganizationMessage(bot.ColorHighlight + message);
            if (this._sendpg) bot.SendPrivateChannelMessage(bot.ColorHighlight + message);
        }

        private void SendIrcNotify(BotShell bot, string message)
        {
            if (this._sendirc) bot.SendIrcMessage(message);
        }

        public string GetLogon( string user )
        {
            string logon = "";
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT Message FROM logon WHERE Username = '" + user + "'";
                IDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    logon = " - " + reader.GetString(0);
                reader.Close();
            }
            return logon;
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "logon":
                return "Allows you to set a message which will be displayed upon logging on.\n" +
                    "Usage: /tell " + bot.Character + " logon [message]";

            case "logon clear":
                return "Clears your logon message.\n" +
                    "Usage: /tell " + bot.Character + " logon clear";

            case "stealth":
                return "Toggles stealth.  If stealth is enabled, neither you nor your alts will trigger logon messages.\n" +
                    "Usage: /tell " + bot.Character + " stealth";
                
                
            }
            return null;
        }
    }
}
