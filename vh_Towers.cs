using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Data;
using System.Xml.Serialization;
using System.Timers;
using AoLib.Utils;
using AoLib.Net;

namespace VhaBot.Plugins
{

	#region LCA XML
    [XmlRoot("data")]
    public class VhTowers_LCAs
    {
        [XmlAttribute("name")]
        public string Name;
        [XmlAttribute("revision")]
        public string Revision;
        [XmlAttribute("updated")]
        public string Updated;
        [XmlElement("entry")]
        public VhTowers_LCAs_Entry[] Entries;
    }

    public class VhTowers_LCAs_Entry
    {
        [XmlAttribute("number")]
        public int Number;
        [XmlAttribute("min")]
        public int Min;
        [XmlAttribute("max")]
        public int Max;
        [XmlAttribute("zone")]
        public string Zone;
        [XmlAttribute("x")]
        public int X;
        [XmlAttribute("y")]
        public int Y;
        [XmlAttribute("name")]
        public string Name;
    }
    #endregion

	#region TowerWars.Info XML
    [XmlRoot("TowerSites")]
    public class VhTowers_TowerSites
    {
        [XmlElement("TowerSite")]
        public VhTowers_TowerSite[] TowerSite;
    }

 	public class VhTowers_TowerSite
    {
        [XmlAttribute("zoneID")]
        public int ZoneID;
        [XmlAttribute("zone")]
        public string Zone;
        [XmlAttribute("faction")]
        public string Faction;
        [XmlAttribute("guild")]
        public string Guild;
        [XmlAttribute("minlevel")]
        public int Min;
        [XmlAttribute("maxlevel")]
        public int Max;
        [XmlAttribute("centerX")]
        public int X;
        [XmlAttribute("centerY")]
        public int Y;
    }
    #endregion

	#region Zerg
    public class Zerg
    {
        public DateTime AttackTime;
        public string Victim;
        public string VictimSide;
        public int ZoneID;
        public string Zone;
        public List<string> AttackingOrgs;
        public List<string> Attackers;
        public List<int> Levels;
        public DateTime LatestAttackTime;

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
                return false;

            // If parameter cannot be cast to Point return false.
            Zerg p = obj as Zerg;
            if ((Zerg)p == null)
                return false;

            // Return true if the fields match:
            return (this.AttackTime == p.AttackTime) &&
                ( this.Victim == p.Victim ) &&
                ( this.VictimSide == p.VictimSide ) &&
                ( this.ZoneID == p.ZoneID ) &&
                ( this.Zone == p.Zone );

        }

    	public bool Equals(Zerg p)
        {
            // If parameter is null return false:
            if ((object)p == null)
                return false;

            // Return true if the fields match:
            return  (this.AttackTime == p.AttackTime) &&
                ( this.Victim == p.Victim ) &&
                ( this.VictimSide == p.VictimSide ) &&
                ( this.ZoneID == p.ZoneID ) &&
                ( this.Zone == p.Zone );
        }

	    public override int GetHashCode()
        {
            string hasher = this.AttackTime + this.Victim + this.VictimSide +
                this.ZoneID.ToString() + this.Zone;
            return hasher.GetHashCode();
        }

		public static bool operator ==( Zerg a, Zerg b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
                return true;

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
                return false;

            // Return true if the fields match:
            return a.Equals(b);

        }

		public static bool operator !=(Zerg a, Zerg b)
        {
            return !(a == b);
        }

    }
    #endregion

    public class VhTowers : PluginBase
    {

        private Config _database;
        private bool _sendgc = true;
        private bool _sendpg = true;
        private bool _first_only = true;
        private bool _use_twi = false;
        private const int _limit = 290;
        private BotShell _bot;

        private CascadingTimer[] _timers = new CascadingTimer[7];
        private List<Zerg> _zergs = new List<Zerg>();

        private const string _towerwars_url = "http://towerwars.info/m/TowerDistribution.php?output=xml&dimension={0}&minlevel=1&maxlevel=300&site_zone={1}&site_number={2}";

        private Timer _welcome;
        private bool _org_attacked = false;
        private DateTime _last_attack_time = DateTime.Now;
        
        public VhTowers()
        {
            this.Name = "Tower-Wars Tracker";
            this.InternalName = "VhTowers";
            this.Author = "Tsuyoi / Llie";
            this.DefaultState = PluginState.Installed;
            this.Description = "Tracks and displays Tower-Wars notifications to Guildchat and PrivateGroup channels.";
            this.Version = 165;
            this.Commands = new Command[] {
                new Command("battle", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("victory", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("zergs", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("repelled", true, UserLevel.Leader),
                new Command("zerg", "zergs" ),
                new Command("lca", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("lca set", true, UserLevel.Leader),
                new Command("lca timer", true, UserLevel.Leader),
                new Command("lca rebuild", true, UserLevel.Admin, UserLevel.Admin, UserLevel.Admin)
            };
        }

        public override void OnLoad(BotShell bot)
        {

            this._bot = bot;

            bot.Events.ChannelMessageEvent +=
                new ChannelMessageHandler(OnChannelMessageEvent);
            bot.Events.ConfigurationChangedEvent +=
                new ConfigurationChangedHandler(ConfigurationChangedEvent);

            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS towerhistory (history_id integer PRIMARY KEY AUTOINCREMENT, victory INT NOT NULL, time INT NOT NULL, atkrSide VARCHAR(10), atkrOrg VARCHAR(50), atkrName VARCHAR(20), defSide VARCHAR(10) NOT NULL, defOrg VARCHAR(50) NOT NULL, zone VARCHAR(50) NOT NULL, xCoord INT, yCoord INT, LCA INT)");
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS towerstatus ( site INT, zone VARCHAR(50), time REAL, side VARCHAR(7), owner VARCHAR(50), exact BOOLEAN, PRIMARY KEY ( site, zone ) )");

            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "sendgc", "Send notifications to the organization channel", this._sendgc);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "sendpg", "Send notifications to the private channel", this._sendpg);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "firstonly", "Only record the first attack on a tower field.  (Also silences zergs.)", this._first_only );
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "usetwi", "Return info from towerwars.info when there is no local information.", this._use_twi );

            Updater.Update( "lca.xml" );
            Updater.Update( "pfid.xml" );

            this.LoadConfiguration(bot);

            for ( int i=0; i<this._timers.Length; i++ )
            {
                this._timers[i] = new CascadingTimer();
                this._timers[i].IntermediateEvent +=
                    new CascadingTimerEvent(this.IntermediateTowerTimerEvent);
                this._timers[i].FinalEvent +=
                    new CascadingTimerEvent(this.FinalTowerTimerEvent);
            }

            bot.Events.UserLogonEvent +=
                new UserLogonHandler(UserLogonEvent);
            bot.Events.UserJoinChannelEvent +=
                new UserJoinChannelHandler(UserJoinChannelEvent);

            this._welcome = new Timer(7000);
            this._welcome.Elapsed +=
                new ElapsedEventHandler(WelcomeTimerElapsed);
            this._welcome.AutoReset = false;
            this._welcome.Enabled = true;

        }

        public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
            {
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= TowersAttackedWindow;
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += TowersAttackedWindow;
                this._bot.Events.UserLogonEvent -=
                    new UserLogonHandler(UserLogonEvent);
                this._bot.Events.UserJoinChannelEvent -=
                    new UserJoinChannelHandler(UserJoinChannelEvent);
            }
        }

        public void WelcomeUnload( BotShell bot )
        {
            this._bot.Events.UserLogonEvent -=
                new UserLogonHandler(UserLogonEvent);
            this._bot.Events.UserJoinChannelEvent -=
                new UserJoinChannelHandler(UserJoinChannelEvent);
        }

        private void TowerAttackCheck()
        {
            DateTime LastAttackTimeout = DateTime.Now.AddHours(-2);
            if ( this._org_attacked &&
                 ( this._last_attack_time <= LastAttackTimeout ) )
                this._org_attacked = false;
        }

        public void TowersAttackedWindow( ref RichTextWindow window,
                                          string user )
        {
            this.TowerAttackCheck();

            if ( this._org_attacked )
            {
                window.AppendHeader( "Tower Status");
                window.AppendLineBreak();
                window.AppendColorString( RichTextWindow.ColorRed, "Attention: One of our tower fields has been attacked.  Please rally a defense.  (Use the !zerg command for more information.)" );
                window.AppendLineBreak(2);
                window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
                window.AppendLineBreak();
            }

        }

        private void UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (e.First) return;
            if (!e.Sections.Contains("notify")) return;

            this.TowerAttackCheck();
            if ( this._org_attacked )
            {
                bot.SendPrivateMessage( e.Sender, HTML.CreateColorString(RichTextWindow.ColorRed, "Attention: One of our tower fields has been attacked.  Please rally a defense.  (Use the !zerg command for more information.)" ) );
            }

        }

        private void UserJoinChannelEvent(BotShell bot, UserJoinChannelArgs e)
        {
            // do not send another message if user is also on notify list
            if ( Array.IndexOf(bot.FriendList.Online("notify"), e.Sender) >= 0 )
                return;

            this.TowerAttackCheck();
            if ( this._org_attacked )
            {
                bot.SendPrivateMessage( e.Sender, HTML.CreateColorString(RichTextWindow.ColorRed, "Attention: One of our tower fields has been attacked.  Please rally a defense.  (Use the !zerg command for more information.)" ) );
            }

        }

        public override void OnUnload(BotShell bot)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= TowersAttackedWindow;
            bot.Events.ChannelMessageEvent -=
                new ChannelMessageHandler(OnChannelMessageEvent);
            bot.Events.ConfigurationChangedEvent -=
                new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Events.UserLogonEvent -=
                new UserLogonHandler(UserLogonEvent);
            bot.Events.UserJoinChannelEvent -=
                new UserJoinChannelHandler(UserJoinChannelEvent);
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._sendgc = bot.Configuration.GetBoolean(this.InternalName, "sendgc", this._sendgc);
            this._sendpg = bot.Configuration.GetBoolean(this.InternalName, "sendpg", this._sendpg);
            this._first_only = bot.Configuration.GetBoolean(this.InternalName, "firstonly", this._first_only);
            this._use_twi = bot.Configuration.GetBoolean(this.InternalName, "usetwi", this._use_twi);
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "battle":
                this.OnBattleCommand(bot, e);
                break;

            case "victory":
                this.OnVictoryCommand(bot, e);
                break;

            case "lca":
                this.OnLandControlCommand(bot, e);
                break;

            case "lca set":
                this.OnLandControlSetCommand(bot, e);
                break;

            case "lca timer":
                this.OnLandControlTimerCommand(bot, e);
                break;

            case "lca rebuild":
                this.OnLandControlRebuildCommand(bot, e);
                break;

            case "zerg":
                this.OnZergCommand(bot, e);
                break;

            case "repelled":
                this._org_attacked = false;
                break;

            }
        }

        private float DateTimeToHourOfDay( DateTime stamp )
        {
            float Time = stamp.Hour + stamp.Minute / 60.0f +
                stamp.Second / 3600.0f;
            return Time;
        }

        public int SiteOfLastAttack ( string zone, string owner )
        {

            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                if ( owner == string.Empty )
                    command.CommandText = "SELECT [LCA] from [towerhistory] WHERE victory = 0 AND zone = '" + zone + "' ORDER by [time] DESC";
                else
                    command.CommandText = "SELECT [LCA] from [towerhistory] WHERE victory = 0 AND zone = '" + zone + "' AND owner = '" + owner + " ORDER by [time] DESC LIMIT 1";
                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() && !reader.IsDBNull(0) )
                    return reader.GetInt32(0);
            }
            return 0;
        }

        public string GuessSiteOwner( string zone, int site )
        {
            
            long last_attack = 0;
            string defender = string.Empty;

            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT [atkrOrg], [defOrg], [time] from [towerhistory] WHERE victory = 0 AND zone = '" + zone + "' AND LCA = " + site + " ORDER by [time] DESC";
                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() && !reader.IsDBNull(0) )
                {
                    if ( last_attack == 0 )
                    {
                        last_attack = reader.GetInt64(2);
                        defender = reader.GetString(1);
                    }
                }
            }

            bool victorious = false;
            string attacker = string.Empty;

            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT [atkrOrg], [time] from [towerhistory] WHERE victory = 1 AND zone = '" + zone + "' ORDER by [time] DESC";
                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() && !reader.IsDBNull(0) )
                {
                    if ( reader.GetInt64(1) < last_attack + (3600 * 6) )
                    {
                        victorious = true;
                        attacker = reader.GetString(0);
                    }
                }
            }

            if ( victorious )
                return attacker;
            else
                return defender;
        }

        public bool BattleInProgress( string defSide, string defOrg,
                                      string zone, int xCoord, int yCoord )
        {
            long time = TimeStamp.Now;
            long time_back =  time - 108000;

            bool v = new bool();
            int x = 0;
            int y = 0;
            
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                command.CommandText = "SELECT [victory], [xCoord], [yCoord] from [towerhistory] WHERE defOrg = '" + Config.EscapeString(defOrg) + "' AND zone = '" + zone + "' AND time BETWEEN " + time_back.ToString() + " AND " + time.ToString() + " ORDER by [time] DESC";
                IDataReader reader = command.ExecuteReader();
                while ( reader.Read() && !reader.IsDBNull(0) )
                {
                    if ( v == false )
                        v = reader.GetBoolean(0);
                    x = reader.GetInt32(1);
                    y = reader.GetInt32(2);
                }
                if ( ( x == xCoord ) && ( y == yCoord ) && ( v == false ) )
                    return true;
            }
            return false;
        }

        private void InsertBattle( string atkrSide, string atkrOrg,
                                   string atkrName, string defSide,
                                   string defOrg, string zone,
                                   int x_coord, int y_coord, int LCA )
        {
            if ( ( this._first_only == true ) &&
                 BattleInProgress( defSide, defOrg, zone, x_coord, y_coord ) )
                return;
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                if ( atkrOrg != string.Empty )
                    command.CommandText = "INSERT INTO [towerhistory] (victory, time, atkrSide, atkrOrg, atkrName, defSide, defOrg, zone, xCoord, yCoord, LCA) VALUES (0," +
                        TimeStamp.Now + ", '" + atkrSide + "', '" + Config.EscapeString(atkrOrg) + "', '" + atkrName + "', '" + defSide + "', '" + Config.EscapeString(defOrg) + "', '" + zone + "', " + x_coord + ", " +
                        y_coord + ", " + LCA + ")";
                else
                    command.CommandText = "INSERT INTO [towerhistory] (victory, time, atkrSide, atkrName, defSide, defOrg, zone, xCoord, yCoord, LCA) VALUES (0," +
                        TimeStamp.Now + ", '" + atkrSide + "', '" + atkrName + "', '" + defSide + "', '" + Config.EscapeString(defOrg) + "', '" + zone + "', " + x_coord + ", " +
                        y_coord + ", " + LCA + ")";
                command.ExecuteReader();
            }
        }

        private void InsertVictory( string atkrSide, string atkrOrg,
                                    string defSide, string defOrg,
                                    string zone )
        {
            using (IDbCommand command =
                   this._database.Connection.CreateCommand())
            {
                if ( atkrOrg != string.Empty )
                    command.CommandText = "INSERT INTO [towerhistory] (victory, time, atkrSide, atkrOrg, defSide, defOrg, zone) VALUES (1, " +
                        TimeStamp.Now + ", '" + atkrSide.ToLower() + "', '" + Config.EscapeString(atkrOrg) + "', '" + defSide.ToLower() + "', '" + Config.EscapeString(defOrg) + "', '" + zone + "')";

                else
                    command.CommandText = "INSERT INTO [towerhistory] (victory, time, defSide, defOrg, zone) VALUES (1, " +
                        TimeStamp.Now + ", '" + defSide.ToLower() + "', '" + Config.EscapeString(defOrg) + "', '" + zone + "')";
                command.ExecuteReader();
            }
        }

        private void OnChannelMessageEvent(BotShell bot, ChannelMessageArgs e)
        {
            if (e.Channel == "All Towers" || e.Channel == "Tower Battle Outcome")
            {
                if (e.IsDescrambled)
                {
                    if (e.Descrambled.CategoryID == 506)
                    {
                        switch (e.Descrambled.EntryID)
                        {
                            case 12753364: // The %s organization %s just entered a state of war! %s attacked the %s organization %s's tower in %s at location (%d, %d).
                                string atkrSide = e.Descrambled.Arguments[0].Message;
                                string atkrOrg = e.Descrambled.Arguments[1].Message;
                                string atkrName = e.Descrambled.Arguments[2].Message;
                                string defSide  = e.Descrambled.Arguments[3].Message;
                                string defOrg = e.Descrambled.Arguments[4].Message;
                                string zone = e.Descrambled.Arguments[5].Message;
                                int x_coord   = e.Descrambled.Arguments[6].Integer;
                                int y_coord   = e.Descrambled.Arguments[7].Integer;
                                int LCA = this.grabLCA(zone, x_coord, y_coord);

                                int pfid = PluginShared.GetPFIDExact(zone);

                                WhoisResult whois = XML.GetWhois(atkrName.ToLower(), bot.Dimension);
                                int atkrLvl = whois.Stats.Level;
                                int atkrAiLvl = whois.Stats.DefenderLevel;
                                string atkrProf = whois.Stats.Profession;

                                string message = HTML.CreateColorString(bot.ColorHeaderHex, atkrOrg) + " " + HTML.CreateColorString(bot.ColorHeaderHex, "(") +
                                    this.sideColor(atkrSide, atkrSide) + HTML.CreateColorString(bot.ColorHeaderHex, ")") + " has attacked " +
                                    HTML.CreateColorString(bot.ColorHeaderHex, defOrg) + " " + HTML.CreateColorString(bot.ColorHeaderHex, "(") +
                                    this.sideColor(defSide, defSide) + HTML.CreateColorString(bot.ColorHeaderHex, ")") + " in " + zone + " at " +
                                    HTML.CreateColorString(bot.ColorHeaderHex, "(x" + LCA.ToString() + " " + x_coord.ToString() + "x" + y_coord.ToString() +
                                    ")") + ". Attacker: " + HTML.CreateColorString(bot.ColorHeaderHex, atkrName) +
                                    " " + HTML.CreateColorString(bot.ColorHeaderHex, "(") + this.sideColor(atkrLvl + "/" + atkrAiLvl + " " + atkrProf, atkrSide) +
                                    HTML.CreateColorString(bot.ColorHeaderHex, ")") + PluginShared.CreateWaypoint( x_coord, y_coord, pfid, "Waypoint" );

                                AddZerg( defOrg, defSide, LCA, zone,
                                         atkrName, atkrOrg, atkrLvl );

                                if ( defOrg == bot.Organization )
                                {
                                    this._org_attacked = true;
                                    this._last_attack_time = DateTime.Now;
                                    this.sendMessage(bot, HTML.CreateColorString(RichTextWindow.ColorRed, "Attention: One of our tower fields has been attacked.  Please rally a defense.  (Use the !zerg command for more information.)"));
                                }

                                if(((this._first_only == true) &&
                                    !BattleInProgress(defSide, defOrg, zone,
                                                      x_coord, y_coord)) ||
                                   (this._first_only == false))
                                {
                                    this.sendMessage(bot, message);
                                    try
                                    {
                                        this.InsertBattle( atkrSide, atkrOrg,
                                                           atkrName,
                                                           defSide, defOrg,
                                                           zone, x_coord,
                                                           y_coord, LCA );
                                    }
                                    catch
                                    {
                                        this.sendMessage(bot, "Error during tower history archiving!");
                                    }
                                }
                                UpdateFieldStatus( zone, LCA, defOrg, defSide );
                                break;
                            case 147506468: // Notum Wars Update: The %s organization %s lost their base in %s.
                                string lostSide = e.Descrambled.Arguments[0].Message;
                                string lostOrg = e.Descrambled.Arguments[1].Message;
                                string lostZone = e.Descrambled.Arguments[2].Message;
                                try
                                {
                                    this. InsertVictory( string.Empty,
                                                         string.Empty,
                                                         lostSide, lostOrg,
                                                         lostZone );
                                }
                                catch
                                {
                                    this.sendMessage(bot, "Error during tower history archiving!");
                                }
                                UpdateFieldStatus( lostZone, 0, string.Empty,
                                                   string.Empty );
                                break;
                        }
                    }
                }
                else
                {
                    //%s just attacked the %s organization %s's tower in %s at location (%d, %d).
                    Regex soloAttack = new Regex(@"(.+)\sjust\sattacked\sthe\s([a-zA-Z]+)\sorganization\s(.+)'s\stower\sin\s(.+)\sat\slocation\s\(([0-9]+),\s([0-9]+)\)");
                    if (soloAttack.IsMatch(e.Message))
                    {
                        Match m = soloAttack.Match(e.Message);
                        string atkrName = m.Groups[1].Value;
                        string defSide = m.Groups[2].Value;
                        string defOrg = m.Groups[3].Value;
                        string zone = m.Groups[4].Value;
                        int x_coord;
                        int y_coord;
                        int.TryParse(m.Groups[5].Value, out x_coord);
                        int.TryParse(m.Groups[6].Value, out y_coord);
                        int LCA = this.grabLCA(zone, x_coord, y_coord);

                        int pfid = PluginShared.GetPFIDExact(zone);

                        WhoisResult whois = XML.GetWhois(atkrName.ToLower(), bot.Dimension);
                        int atkrLvl = whois.Stats.Level;
                        int atkrAiLvl = whois.Stats.DefenderLevel;
                        string atkrProf = whois.Stats.Profession;
                        string atkrSide = whois.Stats.Faction;

                        string message = HTML.CreateColorString(bot.ColorHeaderHex, atkrName) + " " + HTML.CreateColorString(bot.ColorHeaderHex, "(") +
                            this.sideColor(atkrLvl + "/" + atkrAiLvl + " " + atkrProf, atkrSide) + HTML.CreateColorString(bot.ColorHeaderHex, ")") +
                            " has attacked " + HTML.CreateColorString(bot.ColorHeaderHex, defOrg) + " " + HTML.CreateColorString(bot.ColorHeaderHex, "(") +
                            this.sideColor(defSide, defSide) + HTML.CreateColorString(bot.ColorHeaderHex, ")") + " in " + zone + " at " +
                            HTML.CreateColorString(bot.ColorHeaderHex, "(x" + LCA.ToString() + " " + x_coord.ToString() + "x" + y_coord.ToString() + ")") +
                            HTML.CreateColorString(bot.ColorHeaderHex, ")") + "." + PluginShared.CreateWaypoint( x_coord, y_coord, pfid, "Waypoint" );

                        AddZerg( defOrg, defSide, LCA, zone,
                                 atkrName, string.Empty, atkrLvl );

                        if ( defOrg == bot.Organization )
                        {
                            this._org_attacked = true;
                            this._last_attack_time = DateTime.Now;
                                    this.sendMessage(bot, HTML.CreateColorString(RichTextWindow.ColorRed, "Attention: One of our tower fields has been attacked.  Please rally a defense.  (Use the !zerg command for more information.)"));
                        }

                        if(((this._first_only == true) &&
                            !BattleInProgress(defSide, defOrg, zone,
                                              x_coord, y_coord)) ||
                           (this._first_only == false))
                        {
                            this.sendMessage(bot, message);
                            try
                            {
                                this.InsertBattle( atkrSide, string.Empty, atkrName,
                                                   defSide, defOrg, zone,
                                                   x_coord, y_coord, LCA );
                            }
                            catch
                            {
                                this.sendMessage(bot, "Error during tower history archiving!");
                            }
                        }
                        UpdateFieldStatus( zone, LCA, string.Empty,
                                           string.Empty );
                    }

                    //The %s organization %s attacked the %s %s at %s. The attackers won!!
                    Regex Victory = new Regex(@"The\s(.*)\sorganization\s(.*)\sattacked\sthe\s([a-zA-Z]+)\s(.*)\sat\stheir\sbase\sin\s(.*)\.\sThe\sattackers\swon!!");
                    if (Victory.IsMatch(e.Message))
                    {
                        Match m = Victory.Match(e.Message);
                        string atkrSide = m.Groups[1].Value;
                        string atkrOrg = m.Groups[2].Value;
                        string defSide = m.Groups[3].Value;
                        string defOrg = m.Groups[4].Value;
                        string zone = m.Groups[5].Value;
                        try
                        {
                            this. InsertVictory( atkrSide, atkrOrg,
                                                 defSide, defOrg, zone );
                            
                        }
                        catch
                        {
                            this.sendMessage(bot, "Error during tower history archiving!");
                        }
                        UpdateFieldStatus( zone, 0, atkrOrg, atkrSide );
                    }
                }
            }
        }

        public void OnBattleCommand( BotShell bot, CommandArgs e )
        {
            try
            {
                using (IDbCommand command =
                       this._database.Connection.CreateCommand())
                {
                    RichTextWindow window = new RichTextWindow(bot);
                    window.AppendTitle("Tower Battle History");
                    window.AppendLineBreak();
                    command.CommandText = "SELECT [time], [atkrSide] , [atkrOrg], [atkrName], [defSide], [defOrg], [zone], [xCoord], [yCoord], [LCA] FROM [towerhistory] WHERE victory = 0 ORDER BY [time] DESC LIMIT 10";
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        window.AppendHeader(Format.DateTime(reader.GetInt64(0), FormatStyle.Compact) + " GMT");
                        if (!reader.IsDBNull(2))
                        {
                            window.AppendHighlight("Attacker: ");
                            window.AppendColorString(this.sideColorWindow(reader.GetString(3), reader.GetString(1)), reader.GetString(3));
                            window.AppendNormal(" - (");
                            window.AppendColorString(this.sideColorWindow(reader.GetString(2), reader.GetString(1)), reader.GetString(2));
                            window.AppendNormal(")");
                            window.AppendLineBreak();
                        }
                        else
                        {
                            WhoisResult whois = XML.GetWhois(reader.GetString(3), bot.Dimension);
                            window.AppendHighlight("Attacker: ");
                            window.AppendColorString(this.sideColorWindow(reader.GetString(3),whois.Stats.Faction), reader.GetString(3));
                            window.AppendNormal(" - (Unguilded)");
                            window.AppendLineBreak();
                        }
                        window.AppendHighlight("Defender: ");
                        window.AppendColorString(this.sideColorWindow(reader.GetString(5), reader.GetString(4)), reader.GetString(5));
                        window.AppendLineBreak();
                        VhTowers_LCAs_Entry LCA_Entry = Get_LCA_Info( reader.GetInt32(9), reader.GetString(6) );
                        window.AppendHighlight("Level range: ");
                        window.AppendNormal( LCA_Entry.Min.ToString() + " - " +
                            LCA_Entry.Max.ToString() );
                        window.AppendLineBreak();
                        window.AppendHighlight("Location: ");
                        window.AppendNormal(reader.GetString(6) + " (x");
                        window.AppendColorString("FF0000", reader.GetInt64(9).ToString());
                        window.AppendNormal(" " + reader.GetInt64(7) + "x" + reader.GetInt64(8) + ") ");
                        int pfid = PluginShared.GetPFIDExact(reader.GetString(6));
                        window.AppendCommand(  "»»", "/waypoint " + reader.GetInt64(7).ToString() + " " + reader.GetInt64(8).ToString() + " " + pfid.ToString() );
 
                        window.AppendLineBreak(2);
                    }
                    bot.SendReply(e, " Tower Battle Results »» ", window);
                    return;
                }
            }
            catch
            {
                bot.SendReply(e, "Error retrieving battle history!");
            }
        }

        public void OnVictoryCommand(BotShell bot, CommandArgs e)
        {
            try
            {
                using (IDbCommand command =
                       this._database.Connection.CreateCommand())
                {
                    RichTextWindow window = new RichTextWindow(bot);
                    window.AppendTitle("Tower Victory History");
                    window.AppendLineBreak();
                    command.CommandText = "SELECT [time], [atkrSide], [atkrOrg], [defSide], [defOrg], [zone] FROM [towerhistory] WHERE victory = 1 ORDER BY [time] DESC LIMIT 10";
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        window.AppendHeader(Format.DateTime(reader.GetInt64(0), FormatStyle.Compact) + " GMT");
                        if (!reader.IsDBNull(1))
                        {
                            window.AppendHighlight("Attacker: ");
                            window.AppendColorString(this.sideColorWindow(reader.GetString(2), reader.GetString(1)), reader.GetString(2));
                            window.AppendLineBreak();
                            window.AppendHighlight("Defender: ");
                            window.AppendColorString(this.sideColorWindow(reader.GetString(4), reader.GetString(3)), reader.GetString(4));
                            window.AppendLineBreak();
                            window.AppendHighlight("Location: ");
                            window.AppendNormal(reader.GetString(5));
                            long vtime = reader.GetInt64(0);
                            long vtime_back = vtime - 10800; // back 3 hours
                            using (IDbCommand subcommand =
                                   this._database.Connection.CreateCommand())
                            {
                                subcommand.CommandText = "SELECT [time], [atkrSide] , [atkrOrg], [atkrName], [defSide], [defOrg], [zone], [xCoord], [yCoord], [LCA] FROM [towerhistory] WHERE victory = 0 AND time BETWEEN " + vtime_back + " AND " + vtime + " AND atkrOrg = '" + Config.EscapeString(reader.GetString(2)) + "' AND defOrg = '" + Config.EscapeString(reader.GetString(4)) + "' ORDER BY [time] ASC LIMIT 1";
                                IDataReader subreader =
                                    subcommand.ExecuteReader();
                                if ( subreader.Read() &&
                                     !subreader.IsDBNull(0) )
                                {
                                    VhTowers_LCAs_Entry LCA_Entry =
                                        Get_LCA_Info( subreader.GetInt32(9),
                                                      subreader.GetString(6) );
                                    window.AppendLineBreak();
                                    window.AppendHighlight("Level range: ");
                                    window.AppendNormal( LCA_Entry.Min.ToString() +
                                                         " - " +
                                                         LCA_Entry.Max.ToString() );
                                    window.AppendLineBreak();
                                    window.AppendHighlight("Location: ");
                                    window.AppendNormal( subreader.GetString(6) +
                                                         " (x");
                                    window.AppendColorString("FF0000",
                                                             subreader.GetInt64(9).ToString());
                                    window.AppendNormal( " " +
                                                         subreader.GetInt64(7) +
                                                         "x" +
                                                         subreader.GetInt64(8) +
                                                         ") ");
                                }
                            }
                            window.AppendLineBreak(2);
                        }
                        else
                        {
                            window.AppendColorString(this.sideColor(reader.GetString(4), reader.GetString(3)), reader.GetString(4));
                            window.AppendNormal(" lost their base in " + reader.GetString(5) + ".");
                            window.AppendLineBreak(2);
                        }
                    }
                    bot.SendReply(e, " Tower Victory Results »» ", window);
                    return;
                }
            }
            catch
            {
                bot.SendReply(e, "Error retrieving battle victory history!");
            }
        }

		public bool UpdateFieldStatus( string Zone, int Site,
                                       string Owner, string Side )
        {
            return  UpdateFieldStatus( Zone, Site, Owner, Side, 0, false );
        }

		public bool UpdateFieldStatus( string Zone, int Site,
                                       string Owner, string Side,
                                       float Back_Time, bool Exact )
        {

            float Time = DateTimeToHourOfDay( DateTime.Now );

            if ( Back_Time > 0.0f )
                Time = Back_Time;

            if ( Side == string.Empty )
                Side = "Unknown";

            if ( Owner == string.Empty )
                Owner = "Unknown";

            if ( Site == 0 )
                Site = SiteOfLastAttack ( Zone, Owner ) ;

            if ( Site == 0 )
                return false;

            string current_side = "Unknown";
            string current_owner = "Unknown";
            float current_time = 0.0f;
            bool current_exact = false;
            RetrieveCurrentOwner( Zone, Site,
                                  ref current_side, ref current_owner,
                                  ref current_time, ref current_exact );

            // Do not change the time if table already has an exact
            // time and towerfield has not changed ownership
            if ( ( current_exact == true ) && ( current_owner == Owner ) )
            {
                Time = current_time;
                Exact = true;
            }

            int exact_val = 0;
            if( Exact )
                exact_val = 1;

            try
            {
                if ( this._database.ExecuteNonQuery( "INSERT OR REPLACE INTO [towerstatus] VALUES( " + Site + ", '" + Zone + "', " + Time + ", '" + Side + "', '" + Owner + "', " + exact_val + "  )" ) >= 0 )
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private string ComposeZoneRegex( CommandArgs e, int start_parse,
                                         int args_to_parse )
        {
            string zone_regex_str = "^";
            for ( int i=start_parse; i<args_to_parse; i++ )
            {

                if ( e.Args[i].Length == 1 )
                    zone_regex_str += e.Args[i].ToLower();
                else
                {
                    zone_regex_str += char.ToLower(e.Args[i][0]);
                    for ( int j=1; j<e.Args[i].Length; j++ )
                    {
                        if (e.Args[i][j] == char.ToUpper(e.Args[i][j]))
                            zone_regex_str += ".*[ \\-]+" +
                                char.ToLower(e.Args[i][j]);
                        else
                            zone_regex_str +=
                                char.ToLower(e.Args[i][j]);
                    }
                }
                zone_regex_str += ".*";
            }
            return zone_regex_str;
        }

        public void RetrieveCurrentOwner( string Zone, int Number,
                                          ref string Side, ref string Owner,
                                          ref float Time, ref bool Exact )
        {

            Side = "Unknown";
            Owner = "Unknown";

            // first try local database
            try
            {
                using (IDbCommand command =
                       this._database.Connection.CreateCommand())
                {
                    command.CommandText = "SELECT [side], [owner], [time], [exact] FROM [towerstatus] WHERE site = " + Number.ToString() + " AND zone = '" + Zone + "'";
                    IDataReader reader = command.ExecuteReader();
                    if ( reader.Read() && !reader.IsDBNull(0) )
                    {
                        Side = reader.GetString(0);
                        Owner = reader.GetString(1);
                        Time = reader.GetFloat(2);
                        Exact = reader.GetBoolean(3);
                    }
                }
            }
            catch { }

            if ( ( Side == "Unknown" ) && ( Owner == "Unknown" ) &&
                 ( this._use_twi ) )
            {
                try
                {
                    int dim = (int)this._bot.Dimension;
                    VhTowers_TowerSites tsr =
                        (VhTowers_TowerSites)PluginShared.ParseXmlUrl(
                            typeof(VhTowers_TowerSites),
                            string.Format( VhTowers._towerwars_url, dim,
                                           HttpUtility.UrlEncode(Zone),
                                           Number ) );

                    if ( ( tsr != null ) &&
                         ( tsr.TowerSite != null ) &&
                         ( tsr.TowerSite.Length == 1 ) )
                    {
                        if ( (tsr.TowerSite[0].Guild != null) &&
                             (tsr.TowerSite[0].Guild != string.Empty) )
                            Owner = tsr.TowerSite[0].Guild;
                        Side = tsr.TowerSite[0].Faction;
                    }
                }
                catch { }
            }

        }

        public void RemoveStaleZergs()
        {
            lock ( this._zergs )
            {
                DateTime staleTime = DateTime.Now.AddHours(-6);
                List<Zerg> attacksToRemove = new List<Zerg>();
                foreach ( Zerg attack in this._zergs )
                    if( attack.AttackTime <= staleTime )
                        attacksToRemove.Add( attack );
                foreach ( Zerg attack in attacksToRemove )
                    this._zergs.Remove( attack );
            }
        }

        public void AddZerg( string victim, string side, int id, string zone,
                             string attacker, string org, int level )
        {
            // clear out all zergs older than 6 hrs.
            RemoveStaleZergs();

            lock ( this._zergs )
            {

                // check if victim part of existing zerg
                Zerg current = new Zerg();
                current.Victim = victim;
                current.VictimSide = side;
                current.ZoneID = id;
                current.Zone = zone;
                bool found = false;
                Zerg inProgress = null;
                foreach ( Zerg attack in this._zergs )
                {
                    current.AttackTime = attack.AttackTime;
                    if( attack == current )
                    {
                        found = true;
                        inProgress = attack;
                    }
                }

                if ( !found )
                {
                    // if not create new entry
                    current.AttackTime = DateTime.Now;
                    current.LatestAttackTime = DateTime.Now;
                    current.AttackingOrgs = new List<string>();
                    current.Attackers = new List<string>();
                    current.Levels = new List<int>();
                    current.AttackingOrgs.Add(org);
                    current.Attackers.Add(attacker);
                    current.Levels.Add(level);
                    this._zergs.Add( current );
                }
                else
                {
                    // add attacker
                    int index = this._zergs.IndexOf( inProgress );
                    this._zergs[index].LatestAttackTime = DateTime.Now;
                    this._zergs[index].AttackingOrgs.Add( org );
                    this._zergs[index].Attackers.Add( attacker );
                    this._zergs[index].Levels.Add( level );
                }
            }
        }

        public void OnLandControlCommand( BotShell bot, CommandArgs e )
        {

            int level = -1;
            string zone_regex_str = string.Empty;
            int max_sites = 25;
            Regex zone_regex = null;
            int site_number = 0;

            bool found = false;

            if ( e.Args.Length == 0 )
                level = 190;
            else if ( ( e.Args.Length == 1 ) &&
                      ( ! int.TryParse( e.Args[0], out level ) ) )
                zone_regex_str = ComposeZoneRegex( e, 0, 1 );
            else if ( ( e.Args.Length > 1 ) &&
                      ( int.TryParse( e.Args[e.Args.Length-1],
                                      out site_number ) ) )
                zone_regex_str = ComposeZoneRegex( e, 0, e.Args.Length-1 );
            else if ( e.Args.Length > 1 )
                zone_regex_str = ComposeZoneRegex( e, 0, e.Args.Length );

            if ( zone_regex_str != string.Empty )
                zone_regex = new Regex ( zone_regex_str,
                                         RegexOptions.IgnoreCase );

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Land Control Status");
            window.AppendLineBreak();
                
            VhTowers_LCAs lcas = GetLCAs();
            foreach ( VhTowers_LCAs_Entry Entry in lcas.Entries )
            {

                if (

                    ( ( zone_regex_str != string.Empty ) &&
                      ( site_number == 0 ) &&
                      ( zone_regex.IsMatch( Entry.Zone ) ) ) ||

                    ( ( zone_regex_str != string.Empty ) &&
                      ( site_number != 0 ) &&
                      ( Entry.Number == site_number ) &&
                      ( zone_regex.IsMatch( Entry.Zone ) ) ) ||

                    ( ( level >= 0 ) &&
                      ( Entry.Min <= level ) && 
                      ( Entry.Max >= level ) )

                    )
                {
                    found = true;

                    window.AppendHighlight( Entry.Name );
                    window.AppendLineBreak();
                    window.AppendNormal( "  " + Entry.Zone );
                    window.AppendNormal( " x" );
                    window.AppendColorString( "FF0000",
                                              Entry.Number.ToString() );
                    window.AppendNormal( " - Level: " +
                                         Entry.Min.ToString() + "-" +
                                         Entry.Max.ToString() +
                                         " (" + Entry.X.ToString() + "x" +
                                         Entry.Y.ToString() + ") " );
                    int pfid = PluginShared.GetPFIDExact( Entry.Zone );
                    window.AppendCommand( "»»", "/waypoint " +
                                          Entry.X.ToString() + " " +
                                          Entry.Y.ToString() + " " +
                                          pfid.ToString() );

                    string side = "Unknown";
                    string owner = "Unknown";
                    float time = -1.0f;
                    bool exact = false;

                    RetrieveCurrentOwner( Entry.Zone, Entry.Number,
                                          ref side, ref owner, ref time,
                                          ref exact );

                    string heat = "Unknown";

                    if ( time > 0.0f )
                    {
                        float cold = time + 6.0f;
                        float curhr = DateTimeToHourOfDay( DateTime.Now );

                        if (((curhr >= time) && (curhr <= cold)) ||
                            ((curhr+24 >= time) && (curhr+24 <= cold)))
                            heat = "Hot";
                        else
                            heat = "Cold";
                    }

                    window.AppendLineBreak();
                    window.AppendNormal( "  Owner: " );
                    window.AppendColorString(
                        this.sideColorWindow(owner, side),
                        CultureInfo.CurrentCulture.TextInfo.ToTitleCase(owner));

                    window.AppendNormal( " Side: " );
                    window.AppendColorString(
                        this.sideColorWindow(string.Empty, side),
                        CultureInfo.CurrentCulture.TextInfo.ToTitleCase(side));
                    window.AppendNormal( " Status: " );
                    if ( heat == "Hot" )
                        window.AppendColorString( RichTextWindow.ColorRed,
                                                  heat );
                    else if( heat == "Cold" )
                        window.AppendColorString( RichTextWindow.ColorBlue,
                                                  heat );
                    else
                        window.AppendColorString( "808080", heat );

                    window.AppendLineBreak(2);
                    max_sites --;
                    if ( max_sites == 0 )
                        break;
                }

            }

            if ( found )
                bot.SendReply(e, " Land Control Status »» ", window);
            else
                bot.SendReply(e, " No land control sites matching your search.");
            return;

        }

        public void OnLandControlSetCommand( BotShell bot, CommandArgs e )
        {
            string zone_regex_str = string.Empty;
            Regex zone_regex = null;
            int Site = 0;
            string Zone = string.Empty;
            int start_zone = 1;
            int percent = 25;

            if( ( e.Args.Length < 3 ) ||
                 ! int.TryParse( e.Args[e.Args.Length-1], out Site ) )
            {
                bot.SendReply(e, this.OnHelp( bot, "lca set"));
                return;
            }

            if ( e.Args[1].IndexOf( "%" ) >= 0 )
            {
                int.TryParse( e.Args[1].Substring(0, e.Args[1].IndexOf("%")),
                                                  out percent );
                start_zone ++;
            }

            zone_regex_str = ComposeZoneRegex( e, start_zone,
                                               e.Args.Length -1 );
            if ( zone_regex_str != string.Empty )
                zone_regex = new Regex ( zone_regex_str,
                                         RegexOptions.IgnoreCase );
            VhTowers_LCAs lcas = GetLCAs();
            foreach ( VhTowers_LCAs_Entry Entry in lcas.Entries )
                if ( ( zone_regex_str != string.Empty ) &&
                     ( zone_regex.IsMatch( Entry.Zone ) ) )
                    Zone = Entry.Zone;

            if ( Zone == string.Empty )
            {
                bot.SendReply(e, "Unknown zone specified." );
                return;
            }

            int hrs = 0;
            int min = 0;
            int sec = 0;
            bool parse_status = true;

            try
            {
                parse_status&=int.TryParse( e.Args[0].Substring(0,2), out hrs );
                parse_status&=int.TryParse( e.Args[0].Substring(3,2), out min );
                parse_status&=int.TryParse( e.Args[0].Substring(6,2), out sec );
            }
            catch
            {
                parse_status = false;
            }

            if ( ! parse_status )
            {
                bot.SendReply(e, "Time format must be exactly HH:MM:SS relative to now.  (Pad with zeroes.  For example: 01:02:03" );
                return;
            }

            float Time = DateTimeToHourOfDay( DateTime.Now );
            Time += hrs + (min / 60.0f) + (sec / 3600.0f);

            switch ( percent )
            {

            case 75:
                Time += 18.0f;
                break;

            case 5:
                Time -= 5.0f;
                break;

            }

            while ( Time > 24.0f )
                Time -= 24.0f;

            string Owner = "Unknown";
            string Side = "Unknown";
            float old_time = 0.0f;
            bool old_exact = false;
            RetrieveCurrentOwner( Zone, Site, ref Side, ref Owner,
                                  ref old_time, ref old_exact );
    
            if ( UpdateFieldStatus( Zone, Site, Owner, Side, Time, true ) )
                bot.SendReply(e, "Site " + Site + " in " + Zone + " has been updated." );
            else
                bot.SendReply(e, "An error occurred.  Specified site not updated." );

        }

        public void OnLandControlTimerCommand( BotShell bot, CommandArgs e )
        {

            string Zone = string.Empty;
            int Site = 0;

            if ( ( e.Args.Length < 2 ) ||
                 ! int.TryParse( e.Args[e.Args.Length-1], out Site ) )
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Land Control Timers");
                window.AppendLineBreak(2);

                for ( int i=0; i<this._timers.Length; i++ )
                {
                    window.AppendNormal( (i+1).ToString() + ". " );
                    if ( this._timers[i].IsRunning() )
                    {
                        window.AppendHighlight( this._timers[i].Name() );
                        window.AppendNormal( " [" );
                        window.AppendBotCommand( "Cancel",
                                                 "lca timer cancel " +
                                                 i.ToString() );
                        window.AppendNormal( "]" );
                    }
                    else
                        window.AppendNormal( "(Available)" );
                    window.AppendLineBreak();                    
                }

                bot.SendReply(e, " Land Control Timers »» ", window);

                return;
            }
            else if ( e.Args[0] == "cancel" )
            {
                int.TryParse( e.Args[e.Args.Length-1], out Site );
                string timer_name = this._timers[Site].Name();
                this._timers[Site].CancelCascade();
                bot.SendReply(e, " Land Control Timer for Zone: " +
                              timer_name + " cancelled." );
                return;
            }

            string zone_regex_str = ComposeZoneRegex( e, 0, e.Args.Length - 1 );
            Regex zone_regex = new Regex ( "NoZone", RegexOptions.IgnoreCase );
            if ( zone_regex_str != string.Empty )
                zone_regex = new Regex ( zone_regex_str,
                                         RegexOptions.IgnoreCase );
            VhTowers_LCAs lcas = GetLCAs();
            foreach ( VhTowers_LCAs_Entry Entry in lcas.Entries )
                if ( ( zone_regex_str != string.Empty ) &&
                     ( zone_regex.IsMatch( Entry.Zone ) ) )
                    Zone = Entry.Zone;

            if ( Zone == string.Empty )
            {
                bot.SendReply(e, "Unknown zone specified." );
                return;
            }

            // get an open timer while making sure there's not already
            // a timer for this field
            int open = -1;
            bool found = false;

            string Name = Zone + " " + Site.ToString() + "x";
            for ( int i=0; i<this._timers.Length; i++ )
            {
                if ( ( this._timers[i].Name() == Name ) &&
                     ( this._timers[i].IsRunning() ) )
                    found = true;
                if ( ( open < 0 ) &&
                     ( ! this._timers[i].IsRunning() ) )
                    open = i;
            }

            if ( found )
            {
                bot.SendReply(e, "There is already a timer running for " +
                    Name );
                return;
            }

            if ( open < 0 )
            {
                bot.SendReply(e, "All timers in use." );
                return;
            }

            string Side = "Unknown";
            string Owner = "Unknown";
            float Time = -1;
            bool Exact = false;
            RetrieveCurrentOwner( Zone, Site, ref Side, ref Owner, ref Time,
                                  ref Exact );

            if ( ! Exact )
                bot.SendReply(e, "Warning: Zone hot time is inexact!" );

            if ( Time < 0 )
            {
                bot.SendReply(e, "Unknown hot time for Zone: " + Name );
                return;
            }

            float current_hour = DateTimeToHourOfDay( DateTime.Now );
            if ( Time < current_hour )
                Time += 24;

            Time -= current_hour;

            int hours = Convert.ToInt32(Math.Floor(Time));
            int mins = Convert.ToInt32(Math.Floor((Time - hours) * 60.0));
            int secs = Convert.ToInt32(Math.Round((Time - hours - mins/60.0) *
                                                  3600));

            this._timers[open].StartCascade( hours, mins, secs, Name );

            bot.SendReply(e, "Timer added for Zone: " + Name );

        }

        public void OnLandControlRebuildCommand( BotShell bot, CommandArgs e )
        {

            try
            {
                using (IDbCommand command =
                       this._database.Connection.CreateCommand())
                {
                    command.CommandText = "SELECT [victory], [time], [atkrSide], [atkrOrg], [defSide], [defOrg], [zone], [LCA] FROM [towerhistory] ORDER BY [time] ASC ";
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {

                        bool victory = reader.GetBoolean(0);
                        long time = reader.GetInt64(1);
                        string atkrSide = reader.GetString(2);
                        string atkrOrg = reader.GetString(3);
                        string defSide = reader.GetString(4);
                        string defOrg = reader.GetString(5);
                        string zone = reader.GetString(6);
                        int lca = 0;
                        if ( victory == false )
                            lca = reader.GetInt32(7);

                        if ( victory )
                        {

                            long time_back = time - 10800; // back 3 hours
                            using (IDbCommand subcommand =
                                   this._database.Connection.CreateCommand())
                            {
                                subcommand.CommandText = "SELECT [time], [atkrSide], [atkrOrg], [atkrName], [defSide], [defOrg], [zone], [LCA] FROM [towerhistory] WHERE victory = 0 AND time BETWEEN " + time_back + " AND " + time + " AND atkrOrg = '" + Config.EscapeString(atkrOrg) + "' AND defOrg = '" + Config.EscapeString(defOrg) + "' ORDER BY [time] ASC LIMIT 1";
                                IDataReader subreader =
                                    subcommand.ExecuteReader();
                                if ( subreader.Read() &&
                                     !subreader.IsDBNull(1) )
                                    lca = subreader.GetInt32(7);
                            }

                            if ( lca == 0 )
                                continue;

                            UpdateFieldStatus( zone, lca, atkrOrg, atkrSide, DateTimeToHourOfDay(TimeStamp.ToDateTime(time)), false );

                        }
                        else
                        {
                            UpdateFieldStatus( zone, lca, defOrg, defSide, DateTimeToHourOfDay(TimeStamp.ToDateTime(time)), false );
                        }
                    }
                }
            }
            catch
            {
                bot.SendReply(e, "An error occurred attempting to rebuild the Land Control Database.");
                return;
            }
            bot.SendReply(e, "Land Control Database rebuilt.");
        }

        public void OnZergCommand( BotShell bot, CommandArgs e )
        {

            RemoveStaleZergs();

            if ( this._zergs.Count == 0 )
            {
                bot.SendReply(e, " No Current Activity to Report " );
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Recent Attack Summary");
            window.AppendLineBreak();

            foreach ( Zerg zerg in this._zergs )
            {

                window.AppendHighlight( "Defender: " );
                window.AppendColorString( sideColorWindow( zerg.Victim,
                                                           zerg.VictimSide ),
                                          zerg.Victim + " " );
                VhTowers_LCAs_Entry LCA_Entry =
                    Get_LCA_Info( zerg.ZoneID, zerg.Zone );
                int pfid = PluginShared.GetPFIDExact( zerg.Zone );
                PluginShared.AppendWaypoint( window, LCA_Entry.X, LCA_Entry.Y,
                                             pfid, "»»" );
                window.AppendLineBreak();

                window.AppendHighlight( "Duration of attack: " );
                TimeSpan duration = zerg.LatestAttackTime - zerg.AttackTime;
                window.AppendNormal( duration.ToString() );
                window.AppendLineBreak();

                window.AppendHighlight( "Attackers: " );
                window.AppendNormal( zerg.Attackers.Count.ToString() );
                window.AppendLineBreak();
                for ( int i=0; i<zerg.Attackers.Count; i++ )
                {
                    window.AppendNormal( "  "  + zerg.Attackers[i] + " (" +
                                         zerg.Levels[i].ToString() + ") of " +
                                         zerg.AttackingOrgs[i] );
                    window.AppendLineBreak();
                }
                window.AppendLineBreak(2);
            }
            
            bot.SendReply(e, " Recent Attack Summary »» ", window );
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "battle":
                return "Displays recent tower battle messages from [All Towers].\n" +
                    "Usage: /tell " + bot.Character + " battle";

            case "victory":
                return "Displays recent victory messages from [Tower Battle Outcome].\n" +
                    "Usage: /tell " + bot.Character + " victory";

            case "zerg":
                return "Displays information about attackers of current or recent attacks.\n" +
                    "Usage: /tell " + bot.Character + " zerg";

            case "lca":
                return "Displays land control information by zone or level.\n" +
                    "Usage: /tell " + bot.Character + " lca [zone|level]";

            case "lca set":
                return "Sets the time when a given site will go to 25% suppression gas.\n" +
                    "Usage: /tell " + bot.Character + " lca set [time] [[#%]] [zone] [site]\n" +
                    "Time is specified relatively (ie. HH:MM:SS until site goes to ##%).  If not specified, then the time refers when the site will go to 25% suppression gas.";

            case "lca timer":
                return "Sets a timer that will announce a countdown until the specified tower field goes to 25% suppression gas.\n" +
                    "Usage: /tell " + bot.Character + " lca timer [[zone] [site]|cancel #]\n" +
                    "Without any arguments, this command will display active timers.  A timer can be canceled by specifying \"cancel timer#\", but a timer is most easily canceled using the timer window.  There is a maximum of " + _timers.Length.ToString() + " timers.";

            }
            return null;
        }

        #region Other Functions
        private void sendMessage(BotShell bot, string message)
        {
            if (this._sendpg) { bot.SendPrivateChannelMessage(bot.ColorHighlight + message); }
            if (this._sendgc) { bot.SendOrganizationMessage(bot.ColorHighlight + message); }
        }

        private string sideColor(string orgName, string orgSide)
        {
            orgSide = orgSide.ToLower();
            switch (orgSide)
            {
                case "clan":
                    return HTML.CreateColorString(RichTextWindow.ColorOrange, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(orgName));
                case "neutral":
                    return HTML.CreateColorString("A0A0A0", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(orgName));
                case "omni":
                    return HTML.CreateColorString(RichTextWindow.ColorBlue, CultureInfo.CurrentCulture.TextInfo.ToTitleCase(orgName));
                default:
                    return HTML.CreateColorString("080808", CultureInfo.CurrentCulture.TextInfo.ToTitleCase(orgName));
            }
        }

        private string sideColorWindow(string orgName, string orgSide)
        {
            orgSide = orgSide.ToLower();
            switch (orgSide)
            {
                case "clan":
                    return RichTextWindow.ColorOrange;
                case "neutral":
                    return "A0A0A0";
                case "omni":
                    return RichTextWindow.ColorBlue;
                default:
                    return "808080";
            }
        }

        public VhTowers_LCAs GetLCAs()
        {
            return (VhTowers_LCAs)PluginShared.ParseXml(typeof(VhTowers_LCAs), "lca.xml");
        }

        private int grabLCA(string zone, int x, int y)
        {
            foreach (VhTowers_LCAs_Entry Entry in this.GetLCAs().Entries)
            {
                if (Entry.Zone == zone)
                {
                    if (Entry.X >= (x - VhTowers._limit) &&
                        Entry.X <= (x + VhTowers._limit))
                    {
                        if (Entry.Y >= (y - VhTowers._limit) &&
                            Entry.Y <= (y + VhTowers._limit))
                        {
                            return Entry.Number;
                        }
                    }
                }
            }
            return 0;
        }

        private VhTowers_LCAs_Entry Get_LCA_Info( int LCA, string Zone )
        {
            foreach (VhTowers_LCAs_Entry Entry in this.GetLCAs().Entries)
            {
                if ( ( Entry.Number == LCA ) && ( Entry.Zone == Zone ) )
                {
                    return Entry;
                }
            }
            VhTowers_LCAs_Entry EmptyEntry = new VhTowers_LCAs_Entry();
            EmptyEntry.Number = 0;
            return EmptyEntry;
        }

        private void IntermediateTowerTimerEvent( double time_remaining,
                                                  string name )
        {
            string message = CascadingTimer.TimeRemaining( time_remaining,
                                                           name ) +
                " becomes hot.";
            this.sendMessage( this._bot, message );
        }

        private void FinalTowerTimerEvent( double time_remaining,
                                           string name )
        {
            this.sendMessage( this._bot, "The land control zone: " + name +
                              " is now hot." );
        }

        #endregion
    }
}
