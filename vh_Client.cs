using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Globalization;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class VhClient : PluginBase
    {
        public VhClient()
        {
            this.Name = "Client controls";
            this.InternalName = "VhClient";
            this.Author = "Jurosik / Teknologist / Naturalistic";
            this.Version = 105;
            this.Description = "Various client controls settable/viewable using info window links.";
            this.DefaultState = PluginState.Installed;
            this.Commands = new Command[] {
                new Command("lag", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("stats", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot) { }

        public override void OnUnload(BotShell bot) { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {
            case "lag":
                this.OnLagCommand(bot, e);
                break;

            case "stats":
                this.OnStatsCommand(bot, e);
                break;
            }

        }

        public void OnLagCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Clien Lag Tweak");
            window.AppendLineBreak();
            window.AppendHighlight("Environment and Visual (No Zoning Required)");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option ShowAllNames 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option ShowAllNames 0");
            window.AppendNormal (" Character names show over head");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option SimpleClouds 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option SimpleClouds 0");
            window.AppendNormal (" Display Simple Clouds ");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option RealisticClouds 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option RealisticClouds 0");
            window.AppendNormal (" Display Realistic Clouds ");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option RealisticMoons 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option RealisticMoons 0");
            window.AppendNormal (" Display Realistic Moons");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option StarRotation 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option StarRotation 0");
            window.AppendNormal (" Star rotation at night");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option Wildlife 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option Wildlife 0");
            window.AppendNormal (" Show wildlife");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option IsSpaceShipsShown 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option IsSpaceShipsShown 0");
            window.AppendNormal (" Show Sace Ships");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option Shadows 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option Shadows 0");
            window.AppendNormal (" Show Ground Shadows");
            window.AppendLineBreak(2);
            window.AppendHighlight("Environment and Visual (Zoning Required)");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option BuffsFX 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option BuffsFX 0");
            window.AppendNormal (" Show Buff Effects");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option EnvironmentFX 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option EnvironmentFX 0");
            window.AppendNormal (" Show Environment Effects");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option MuzzleFlashFX 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option MuzzleFlashFX 0");
            window.AppendNormal (" Show Muzzle Flash");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option NanoEffectFX 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option NanoEffectFX 0");
            window.AppendNormal (" Show Nano Effects (Calms, Damage Shields, etc.)");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option TracersFX 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option TracersFX 0");
            window.AppendNormal (" Show Tracers");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option OthersFX 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option OthersFX 0");
            window.AppendNormal (" Show Others Effects");
            window.AppendLineBreak(2);
            window.AppendHighlight("Audio Settings (No Zoning Required)");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option SoundOnOff 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option SoundOnOff 0");
            window.AppendNormal (" Sound Master ");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option SoundFXOn 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option SoundFXOn 0");
            window.AppendNormal (" Sound Effects");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option MusicOn 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option MusicOn 0");
            window.AppendNormal (" Music");
            window.AppendLineBreak();
            window.AppendCommand ("Enable", "/option VoiceSndFxOn 1");
            window.AppendNormal ("  ");
            window.AppendCommand ("Disable", "/option VoiceSndFxOn 0");
            window.AppendNormal (" Voices");
            window.AppendLineBreak(2);
            window.AppendHighlight("View Distance Settings (No Zoning Required)");
            window.AppendLineBreak();
            window.AppendCommand ("Min", "/option ViewDistance 0.05");
            window.AppendNormal ("  ");
            window.AppendCommand ("Low", "/option ViewDistance 0.30");
            window.AppendNormal ("  ");
            window.AppendCommand ("Medium", "/option ViewDistance 0.50");
            window.AppendNormal ("  ");
            window.AppendCommand ("High", "/option ViewDistance 0.75");
            window.AppendNormal ("  ");
            window.AppendCommand ("Max", "/option ViewDistance 1");
            window.AppendNormal (" View Distance");
            window.AppendLineBreak();
            window.AppendCommand ("Min", "/CharDist 5");
            window.AppendNormal ("  ");
            window.AppendCommand ("Low", "/CharDist 30");
            window.AppendNormal ("  ");
            window.AppendCommand ("Medium", "/CharDist 50");
            window.AppendNormal ("  ");
            window.AppendCommand ("High", "/CharDist 75");
            window.AppendNormal ("  ");
            window.AppendCommand ("Max", "/CharDist 100");
            window.AppendNormal (" Character View Distance");
            window.AppendLineBreak(2);
            bot.SendReply(e, " Lag Tweak »» ", window);
        }

        public void OnStatsCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Character Stats");
            window.AppendLineBreak();
            window.AppendHighlight("Offense / Defense"); 
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://276>Offense (Addall-Off)</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://277>Defense (Addall-Def)</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://51>Aggdef-Slider</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://4>Attack Speed</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Critical Strike");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://379>Crit increase</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://391>Crit decrease</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Heal"); 
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://342>Heal delta (interval)</a> <font color=#FFFFFF>(tick in secs)</font>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://343>Heal delta (amount)</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://535>Heal modifier</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Nano");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://363>Nano delta (interval)</a> <font color=#FFFFFF>(tick in secs)</font>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://364>Nano delta (amount)</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://318>Nano execution cost</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://536>Nano modifier</a>"); 
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://383>Interrupt modifier</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://381>Range Increase Nanoformula</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Add Damage (Amount)");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://279>+Damage - Melee</a>"); 
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://280>+Damage - Energy</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://281>+Damage - Chemical</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://282>+Damage - Radiation</a>"); 
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://278>+Damage - Projectile</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://311>+Damage - Cold</a>"); 
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://315>+Damage - Nano</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://316>+Damage - Fire</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://317>+Damage - Poison</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Reflect Shield (Percentage)");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://205>ReflectProjectileAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://206>ReflectMeleeAC</a>"); 
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://207>ReflectEnergyAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://208>ReflectChemicalAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://216>ReflectRadiationAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://217>ReflectColdAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://218>ReflectNanoAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://219>ReflectFireAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://225>ReflectPoisonAC</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Reflect Shield (Amount)");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://475>MaxReflectedProjectileDmg</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://476>MaxReflectedMeleeDmg</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://477>MaxReflectedEnergyDmg</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://278>MaxReflectedChemicalDmg</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://479>MaxReflectedRadiationDmg</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://480>MaxReflectedColdDmg</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://481>MaxReflectedNanoDmg</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://482>MaxReflectedFireDmg</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://483>MaxReflectedPoisonDmg</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Damage Shield (Amount)");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://226>ShieldProjectileAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://227>ShieldMeleeAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://228>ShieldEnergyAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://229>ShieldChemicalAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://230>ShieldRadiationAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://231>ShieldColdAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://232>ShieldNanoAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://233>ShieldFireAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://234>ShieldPoisonAC</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Damage Absorb (Amount)");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://238>AbsorbProjectileAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://239>AbsorbMeleeAC</a>"); 
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://240>AbsorbEnergyAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://241>AbsorbChemicalAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://242>AbsorbRadiationAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://243>AbsorbColdAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://244>AbsorbFireAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://245>AbsorbPoisonAC</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://246>AbsorbNanoAC</a>");
            window.AppendLineBreak();
            window.AppendLineBreak();
            window.AppendHighlight("Misc");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://319>XP Bonus</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://382>SkillLockModifier</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://380>Weapon Range Increase</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://517>Special Attack Blockers</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://199>Reset Points</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://360>Scale</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://669>Victory Points</a>");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://201>Char base aggro</a> (unknown)");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://202>Char stability</a> (unknown)");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://203>Char extroverty</a> (unknown)");
            window.AppendLineBreak();
            window.AppendRawString("<a href=skillid://457>Hate List Size</a> (unknown)");
            window.AppendNormal("  ");
            bot.SendReply(e, "Stats »» ", window);
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "lag":
                return "Client Lag Tweaks\n" +
                    "Usage: /tell " + bot.Character + " lag";

            case "stats":
                return "Shows all the hidden stats and their actual values\n" +
                    "Usage: /tell " + bot.Character + " stats\n" +
                    "No parameters required.";

            }
            return null;
        }
    }
}
