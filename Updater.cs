using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Net;
using System.Net.Security;
using System.Data;
using System.Security.Cryptography.X509Certificates;

namespace VhaBot.Plugins
{

    public class Updater
    {

        public const string Bitbucket = "https://bitbucket.org";

        public static string Repo_Root = "/Llie/llie_vhabot";
        public static string Default_Plugin_Repo = "/Llie/vhabot_plugins_default";
        public static string Llie_Plugin_Repo = "/Llie/vhabot_plugins_llie";

        public const string BitbucketAPIDirectory = "https://bitbucket.org/!api/1.0/repositories{0}/directory/";
        public const string BitbucketAPIChangesets = "https://bitbucket.org/api/1.0/repositories{0}/changesets/";

        public const string Data_Dir = "data";
        private const string Version_File = "version.xml";

        // Set the following variable to true to disable the updater
        private static bool isDisabled = false;

        public Updater()
        {
            // test the Repo_Root upon creation
            string URL = Bitbucket + Repo_Root;
            try
            {
                string test = GetURL ( URL );
                if ( test.Length == 0 )
                {
                    isDisabled = true;
                    Console.WriteLine("[Updater] Unable to contact repository. Disabling Updater");
                }
            }
            catch
            {
                isDisabled = true;
                Console.WriteLine("[Updater] Repository URL incorrect. Disabling Updater");
            }            
        }

	    public static bool CertValidator (object sender,
                                          X509Certificate certificate,
                                          X509Chain chain, 
                                          SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static string GetJsonField( string key, string buffer,
                                           int start_at )
        {
            string return_value = string.Empty;
            string full_key =  "\"" + key + "\": ";
            if ( buffer.IndexOf( full_key, start_at ) < 0 )
                return return_value;
            return_value = buffer.Substring( buffer.IndexOf( full_key,
                                                             start_at ) +
                                             full_key.Length + 1 );
            return_value = return_value.Substring( 0,
                                                   return_value.IndexOf("\"") );
            return return_value;
        }

        public static string GetJsonField( string key, string buffer )
        {
            return GetJsonField( key, buffer, 0 );
        }

        public static string GetURL( string URL )
        {
            ServicePointManager.ServerCertificateValidationCallback = CertValidator;
            StringBuilder sb  = new StringBuilder();
            HttpWebRequest request  = (HttpWebRequest)WebRequest.Create( URL );
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();
            byte[] buf = new byte[8192];
            string tempString = null;
            int count = 0;
            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    tempString = Encoding.UTF8.GetString(buf, 0, count);
                    sb.Append(tempString);
                }
            }
            while (count > 0);
            return sb.ToString();
        }

        public static string Tip()
        {
            string RSS = Bitbucket + Repo_Root + "/rss";
            string xml = GetURL ( RSS );
            if (string.IsNullOrEmpty(xml))
            {
                Console.WriteLine( "[Updater] Failed to download RSS feed" );
                return string.Empty;
            }

            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlSerializer serializer = new XmlSerializer(typeof(hg_rss_feed));
                hg_rss_feed rss = (hg_rss_feed)serializer.Deserialize(stream);
                stream.Close();

                int found = rss.Channel.Items[0].Link.IndexOf( "changeset/" );
                if ( found > 0 ) 
                    return rss.Channel.Items[0].Link.Substring(found + 10);
            }
            catch
            {
                Console.WriteLine( "[Updater] Failed to parse RSS feed" );
                return string.Empty;
            }

            return string.Empty;
        }

        public static string LatestVersion( string fn )
        {
            string URL = Bitbucket + Repo_Root +
                "/history-node/tip/Extra/data/" + fn +
                "?at=default";

            string history_page = string.Empty;
            try
            {
                history_page = GetURL( URL );
            }
            catch
            {
                return string.Empty;
            }

            // search for first 'class="execute"' which contains the
            // revision strings in URL of the subsequent <a href> tag

            int idx = history_page.IndexOf("class=\"hash execute\"");
            try
            {
                string substr = history_page.Substring( idx );
                idx = substr.IndexOf("href=\"");
                substr = substr.Substring( idx + 6 );
                idx = substr.IndexOf("\"");
                substr = substr.Remove( idx, substr.Length - idx );

                idx = substr.IndexOf( Repo_Root + "/src/" );
                substr = substr.Substring( idx + Repo_Root.Length + 5 );
                idx = substr.IndexOf( "/" );
                substr = substr.Remove( idx, substr.Length - idx );

                return substr;
            }
            catch
            {
                return "tip";
            }

        }

        public static Version ReadVersion()
        {
            string VER = Data_Dir + '/' + Version_File;
            if ( ! File.Exists ( VER ) )
                return new Version();

            // open in shared mode
            FileStream fs = new FileStream( VER, FileMode.Open, FileAccess.Read, FileShare.ReadWrite );
            System.IO.StreamReader verFile = new System.IO.StreamReader(fs);
            string verxml = verFile.ReadToEnd();
            verFile.Close();

            MemoryStream stream = null;
            try
            {
                stream = new MemoryStream(Encoding.UTF8.GetBytes(verxml));
                XmlSerializer serializer = new XmlSerializer(typeof(Version));
                Version V = (Version)serializer.Deserialize(stream);
                stream.Close();
                return V;
            }
            catch
            {
                Console.WriteLine( "[Updater] Failed to parse version.xml" );
                return new Version();
            }            
        }

        public static string CheckVersion( string fn )
        {
            Version V = ReadVersion( );
            for( int i=0; i<V.File.Length; i++ )
                if ( fn == V.File[i].Name )
                    return V.File[i].Version;
            return string.Empty;
        }

        public static bool DownloadFile ( string fn, string version )
        {
            string RAW = Bitbucket + Repo_Root +
                "/raw/" + version + "/Extra/data/" + fn;
            string xml_file = GetURL ( RAW );
            if ( xml_file.Length > 0 )
            {
                if ( ! Directory.Exists ( Data_Dir ) )
                    Directory.CreateDirectory( Data_Dir );
                string out_path = Data_Dir + '/' + fn;
                using (StreamWriter outfile = new StreamWriter(out_path))
                    outfile.Write(xml_file);
                return true;
            }
            return false;
        }

	    public static void UpdateVersion( string fn, string version )
        {
            string VER = Data_Dir + '/' + Version_File;
            if ( ! Directory.Exists ( Data_Dir ) )
                Directory.CreateDirectory( Data_Dir );

            if ( ! File.Exists ( VER ) )
            {
                Version V = new Version( fn, version );
                XmlSerializer SerializerObj = new XmlSerializer(typeof(Version));
                TextWriter WriteFileStream = new StreamWriter(VER);
                SerializerObj.Serialize(WriteFileStream, V);
                WriteFileStream.Close();
            }
            else
            {
                Version V = ReadVersion( );
                for( int i=0; i<V.File.Length; i++ )
                    if ( fn == V.File[i].Name )
                    {
                        V.File[i].Version = version;
                        XmlSerializer SerializerObj = new XmlSerializer(typeof(Version));
                        TextWriter WriteFileStream = new StreamWriter(VER);
                        SerializerObj.Serialize(WriteFileStream, V);
                        WriteFileStream.Close();
                        return;
                    }
                    Version V2 = new Version(V.File.Length + 1);
                    for( int i=0; i<V.File.Length; i++ )
                    {
                        V2.File[i].Name = V.File[i].Name;
                        V2.File[i].Version = V.File[i].Version;
                    }
                    V2.File[V.File.Length].Name = fn;
                    V2.File[V.File.Length].Version = version;
                    try
                    {
                        XmlSerializer SerializerObj2 = new XmlSerializer(typeof(Version));
                        TextWriter WriteFileStream2 = new StreamWriter(VER);
                        SerializerObj2.Serialize(WriteFileStream2, V2);
                        WriteFileStream2.Close();
                    }
                    catch
                    {
                        Console.WriteLine( "[Updater] Version file write error." );
                        return;
                    }
                return;
            }

        }

	    public static bool Disabled( )
        {
            return isDisabled;
        }

	    public static void Update( string fn )
        {
            if ( isDisabled == true )
                return;

            string local_version = CheckVersion ( fn );
            string repo_version = LatestVersion( fn );
            if ( ( repo_version != string.Empty ) &&
                 ( local_version != repo_version ) )
            {
                Console.WriteLine( "[Updater] Downloading " + fn );
                if( DownloadFile ( fn, repo_version ) )
                    UpdateVersion( fn, repo_version );
            }
            else if ( string.IsNullOrEmpty(repo_version) )
            {
                Console.WriteLine ( "[Updater] " + fn + " is not in repository." );
            }
            else
            {
                Console.WriteLine ( "[Updater] " + fn + " is up to date." );
            }
        }

		public static void GetPluginUpdates ( string repo,
                                              ref List<string> Plugins,
                                              ref List<string> Updates )
        {
            string uri = string.Empty;
            string html = string.Empty;

            uri = String.Format( Updater.BitbucketAPIDirectory, repo );
            html = Updater.GetURL(uri);
            string[] tokens = html.Split(new string[] { "," },
                                         StringSplitOptions.None);
            foreach ( string token in tokens )
            {
                string plugin = string.Empty;
                plugin = token.Remove( 0, token.IndexOf("\"") + 1 );
                plugin = plugin.Remove( plugin.IndexOf("\"") );
                if ( ( plugin.IndexOf("/") == -1 ) &&
                     ( ( plugin.IndexOf( ".cs" ) == plugin.Length - 3  ) ||
                       ( plugin.IndexOf( ".vb" ) == plugin.Length - 3  ) ) )
                {
                    Plugins.Add( plugin );
                    Updates.Add( "No Recent Updates" );
                }
            }

            int n_plugins = Updates.Count;
            uri = String.Format( Updater.BitbucketAPIChangesets, repo );
            html = Updater.GetURL(uri);
            while ( html.IndexOf( "\"size\":" ) > 0 )
            {
                string timestamp = Updater.GetJsonField( "timestamp", html );
                int stop_at = html.IndexOf( timestamp );
                int next = 0;
                string file = string.Empty;
                while ( html.IndexOf( "\"file\":", next ) < stop_at )
                {
                    file = Updater.GetJsonField( "file", html, next );
                    next = html.IndexOf( file, next ) + file.Length;
                    if ( file != string.Empty )
                    {
                        int index = Plugins.IndexOf( file );
                        if ( ( index >= 0 ) &&
                             ( index < n_plugins ) )
                            Updates[index] = timestamp;
                    }
                    else
                        break;
                }
                html = html.Remove( 0, html.IndexOf( "\"size\":" ) + 8 );
            }

        }

    }

    [XmlRoot("version")]
    public class Version
    {
        [XmlElement("file")]
        public file_i[] File;
        public Version( ) { File = new file_i[0]; }
        public Version( int x ) 
        {
            File = new file_i[x];
            for( int i=0; i<x; i++ )
                File[i] = new file_i( );
        }
        public Version( params string[] inputs )
        {
            File = new file_i[inputs.Length/2];
            for( int i=0; i<inputs.Length/2; i+=2 )
                File[i/2] = new file_i( inputs[i], inputs[i+1] );
        }
    }

    public class file_i
    {
        [XmlElement("name")]
        public string Name;
        [XmlElement("version")]
        public string Version;
        public file_i( ) { Name = string.Empty; Version = string.Empty; }
        public file_i( string n, string v )
        {
            Name = n;
            Version = v;
        }
    }

    [XmlRoot("rss")]
    public class hg_rss_feed
    {
        [XmlElement("channel")]
        public hg_rss_channel Channel;
    }

    public class hg_rss_channel
    {
        [XmlElement("title")]
        public string Title;
        [XmlElement("link")]
        public string Link;
        [XmlElement("language")]
        public string Language;
        [XmlElement("generator")]
        public string Generator;
        [XmlElement("webMaster")]
        public string WebMaster;
        [XmlElement("ttl")]
        public string TTL;
        [XmlElement("item")]
        public hg_rss_item[] Items;
    }

    public class hg_rss_item
    {
        [XmlElement("title")]
        public string Title;
        [XmlElement("link")]
        public string Link;
        // [XmlElement("description")]
        // public string Description;
        [XmlElement("author")]
        public string Author;
        [XmlElement("pubDate")]
        public string PubDate;
        [XmlElement("guid")]
        public string GUID;
    }

	// A basic untar class -- This class provides a basic untar (it
	// does not handle symbolic links or special files), a gunzip, and
	// a function that operates on a URL.
    public class Untar
    {
    
		private const int TSIZE = 512;
        private static Mutex mutex = new Mutex();
        private static int outsize;
        private static BinaryWriter outfp;
        private static bool EOA = false;

        public static byte[] Decompress(byte[] gzip)
        {
            // Create a GZIP stream with decompression mode.
            // ... Then create a buffer and write into while reading
            // from the GZIP stream.
            using (GZipStream stream =
                   new GZipStream(new MemoryStream(gzip), 
                                  CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }

        private static int checksum(byte[] blk, bool sunny)
        {
            int sum = 0;

            // compute the sum of the first 148 bytes -- everything up
            // to but not including the checksum field itself.
            for ( int i=0; i<148; i++ )
            {
                sum += blk[i] & 0xff;
                if (sunny && (blk[i] & 0x80) != 0)
                    sum -= 256;
            }

            // for the 8 bytes of the checksum field, add blanks to the sum 
            sum += ' ' * 8;

            // finish counting the sum of the rest of the block
            for( int i=156; i<512; i++ )
            {
                sum += blk[i] & 0xff;
                if (sunny && (blk[i] & 0x80) != 0)
                    sum -= 256;
            }

            return sum;
        }

        // list files in an archive, and optionally extract them as well 
        public static void Extract( byte[] blk )
        {
            string name; // prefix and name, combined
            bool first = true; // Boolean: first block of archive?
            int sum; // checksum for this block

            // process each type of tape block differently
            if (outsize > TSIZE)
            {
                // data block, but not the last one
                if ( outfp != null )
                {
                    outfp.Write(blk);
                }
                outsize -= TSIZE;
            }
            else if (outsize > 0)
            {
                // last data block of current file
                if ( outfp != null )
                {
                    byte[] sblk = new byte[outsize];
                    Array.Copy( blk, 0, sblk, 0, outsize );
                    outfp.Write(sblk);
                    outfp.Close();
                    outfp = null;
                }
                outsize = 0;
            }
            else if (blk[0] == 0)
            {
                // end-of-archive marker
                EOA = true;
                return;
            }
            else
            {
                /* file header */

                byte[] fn = new byte[100];
                byte[] px = new byte[155];

                Array.Copy( blk, 0, fn, 0, 100 );
                Array.Copy( blk, 345, px, 0, 155 );

                string filename = ASCIIEncoding.ASCII.GetString(fn);
                string prefix = ASCIIEncoding.ASCII.GetString(px);

                // trim padded nulls off end of string
                filename = filename.Substring(0,filename.IndexOf('\0'));
                prefix = prefix.Substring(0,prefix.IndexOf('\0'));

                // combine prefix and filename
                if( (px.Length > 0) && (px[0] != 0) )
                {
                    name = prefix + "/" + filename;
                }
                else
                {
                    name = filename;
                }

                // Convert any backslashes to forward slashes, and guard
                // against doubled-up slashes. (Some DOS versions of "tar"
                // get this wrong.)  Also strip off leading slashes.

                if ( name[0] == '/' )
                    name = name.Substring(1);

                name = name.Replace( '\\', '/' );

                name = name.Replace( "//", "/" );

                name = name.Replace( '/', Path.DirectorySeparatorChar );

                // verify the checksum
                sum = 0;
                for( int i=148; i<148+8; i++ )
                    if ( ( blk[i] >= '0' ) && ( blk[i] <= '7') )
                        sum = sum * 8 + blk[i] - '0';

                if ( (sum != checksum(blk, false)) &&
                     (sum != checksum(blk, true)) )
                {
                    if (!first)
                        Console.WriteLine("[Updater] WARNING: Garbage detected in download stream.  Preceding file may be damaged\n");
                    Console.WriteLine( String.Format( "[Updater] WARNING: Header has bad checksum for {0}", name ));
                    return;
                }

                // From this point on, we don't care whether this is
                // the first block or not.  Might as well reset the
                // "first" flag now.

                first = false;

                byte[] sz = new byte[12];
                Array.Copy( blk, 124, sz, 0, 12 );
                string size = ASCIIEncoding.ASCII.GetString(sz);

                // convert file size
                for( int i=0; i<sz.Length; i++ )
                    if( (size[i] >= '0') && (size[i] <= '7') )
                        outsize = outsize * 8 + size[i] - '0';

                // If directory, then make a weak attempt to create
                // it.  Ideally we would do the "create path" thing,
                // but that seems like more trouble than it's worth
                // since traditional tar archives don't contain
                // directories anyway.

                // if last character of name is '/' then assume directory

                if ( ( blk[156] == '5' ) ||
                     ( name[name.Length-1] == Path.DirectorySeparatorChar ) )
                {
                    if ( ! Directory.Exists( name ) )
                        Directory.CreateDirectory( name );
                    return;
                }

                // if not a regular file, then skip it
                if ( ( blk[156] >= '1' ) && ( blk[156] <= '6' ) )
                {
                    outsize = 0;
                    return;
                }

                // create all interediate sub-directories
                if ( name.LastIndexOf(Path.DirectorySeparatorChar) >= 0 )
                {
                    string dir = name.Substring( 0, name.LastIndexOf(Path.DirectorySeparatorChar) );
                    if ( ! Directory.Exists( dir ) )
                        Directory.CreateDirectory( dir );
                }
                
                outfp = new BinaryWriter( File.Open(name, FileMode.Create ) );

                // if file is 0 bytes long, then we're done already!
                if ( ( outsize == 0)  && ( outfp != null ) )
                    outfp.Close();

            }

        }

        public static void DownloadExpandExtract( string URL )
        {
            ServicePointManager.ServerCertificateValidationCallback =
                Updater.CertValidator;
            HttpWebRequest request  = (HttpWebRequest)WebRequest.Create( URL );
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();

            byte[] buf = new byte[8192];
            int count = 0;
            int total = 0;
 
            mutex.WaitOne();

            byte[] gzip = new byte[response.ContentLength];

            total = 0;
            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    Array.Copy( buf, 0, gzip, total, count );
                    total += count;
                }
            }
            while (count > 0);

            byte[] uzip = Decompress( gzip );

            total = uzip.Length;
            byte[] block = new byte[TSIZE];
            while ( total > 0 )
            {
                int csz = Math.Min( TSIZE, total );
                Array.Copy( uzip, uzip.Length-total, block, 0, csz );
                Extract(block);
                total -= csz;
            }
            
            mutex.ReleaseMutex();

            if ( ( outsize > 0 ) || ( EOA == false ) )
                Console.WriteLine( "[Updater] Warning.  Download may be incomplete." );

        }

    }

    public class BotUpdater : PluginBase
    {

        private int _num_backups = 0;
        private BotShell _bot = null;

        private string _repo_root = Updater.Repo_Root;
        private string _def_root = Updater.Default_Plugin_Repo;
        private string _llie_root = Updater.Llie_Plugin_Repo;

        public BotUpdater()
        {
            this.Name = "VhaBot LE Update system";
            this.InternalName = "BotUpdater";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 101;
            this.Commands = new Command[] {
                new Command("backup", true, UserLevel.SuperAdmin),
                new Command("backup users", true, UserLevel.SuperAdmin),
                new Command("restore", true, UserLevel.SuperAdmin),
                new Command("restore users", true, UserLevel.SuperAdmin),
                new Command("update plugins", true, UserLevel.SuperAdmin),
                new Command("update plugin", true, UserLevel.SuperAdmin),
                new Command("update", true, UserLevel.SuperAdmin)
            };
        }

        public override void OnLoad(BotShell bot)
        {

            this._bot = bot;

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Configuration.Register(ConfigType.Integer, this.InternalName, "num_backups", "Number of backups to keep", this._num_backups, 0, 1, 2, 3, 4, 5 );
            bot.Configuration.Register(ConfigType.String, this.InternalName, "repo_root", "Packages repository", this._repo_root );
            bot.Configuration.Register(ConfigType.String, this.InternalName, "def_root", "Default plug-in subrepository", this._def_root );
            bot.Configuration.Register(ConfigType.String, this.InternalName, "llie_root", "Llie's plug-in subrepository", this._llie_root );

            this.LoadConfiguration(bot);

            bot.Timers.Hour += new EventHandler(Backup_Event);
        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Timers.Hour -= new EventHandler(Backup_Event);
        }

        private void LoadConfiguration(BotShell bot)
        {

            this._num_backups = bot.Configuration.GetInteger(this.InternalName, "num_backups", this._num_backups);

            this._repo_root = bot.Configuration.GetString(this.InternalName, "repo_root", this._repo_root );
            this._def_root = bot.Configuration.GetString(this.InternalName, "def_root", this._def_root );
            this._llie_root = bot.Configuration.GetString(this.InternalName, "llie_root", this._llie_root );

            bool fixed_repo_root = false;
            bool fixed_def_repo = false;
            bool fixed_llie_repo = false;
            // all repos should start with a "/" but not end with one
            if ( this._repo_root[0] != '/' )
            {
                this._repo_root = "/" + this._repo_root;
                fixed_repo_root = true;
            }
            if ( this._def_root[0] != '/' )
            {
                this._def_root = "/" + this._def_root;
                fixed_def_repo = true;
            }
            if ( this._llie_root[0] != '/' )
            {
                this._llie_root = "/" + this._llie_root;
                fixed_llie_repo = true;
            }

            if ( this._repo_root[this._repo_root.Length-1] == '/' )
            {
                this._repo_root = this._repo_root.Substring(0,this._repo_root.Length-1);
                fixed_repo_root = true;
            }
            if ( this._def_root[this._def_root.Length-1] == '/' )
            {
                this._def_root = this._def_root.Substring(0,this._def_root.Length-1);
                fixed_def_repo = true;
            }
            if ( this._llie_root[this._llie_root.Length-1] == '/' )
            {
                this._llie_root = this._llie_root.Substring(0, this._llie_root.Length-1);
                fixed_llie_repo = true;
            }

            if ( fixed_repo_root )
                bot.Configuration.SetString(this.InternalName, "repo_root", this._repo_root );
            if ( fixed_def_repo )
                bot.Configuration.SetString(this.InternalName, "def_root", this._def_root );
            if ( fixed_llie_repo )
                bot.Configuration.SetString(this.InternalName, "llie_root", this._llie_root );

        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void Backup_Event(object sender, EventArgs e)
        {
            DateTime Now = DateTime.UtcNow;
            if ( Now.Hour != 23 ) return;
            BackupUsers();
            if ( ! Backup() )
            {
                Console.WriteLine( "[Updater] Backup failed.  Try clearing the backup directory." );
            }
            else
            {
                Console.WriteLine( "[Updater] Backup completed." );
            }
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {

            case "update":
                this.OnCheckCommand(bot, e);
                break;

            case "update plugins":
                this.OnUpdateCommand(bot, e);
                break;

            case "update plugin":
                this.OnUpdatePluginCommand(bot, e);
                break;

            case "backup":
                this.OnBackupCommand(bot, e);
                break;

            case "backup users":
                this.OnBackupUsersCommand(bot, e);
                break;

            case "restore":
                this.OnRestoreCommand(bot, e);
                break;

            case "restore users":
                this.OnRestoreUsersCommand(bot, e);
                break;

            }
        }

        private int GetRepoVersions( ref string bot_release_version,
                                     ref List<string> plugin_versions,
                                     ref List<string> plugin_urls )
        {

            string download_page;
            string URL = Updater.Bitbucket + this._repo_root + "/downloads";
            try
            {
                download_page = Updater.GetURL ( URL );
            }
            catch
            {
                return - 1;
            }

            string[] download_lines = download_page.Split( '\n' );

            for ( int i=0; i<download_lines.Length; i++ )
                if ( download_lines[i].IndexOf( "download-" ) >= 0 )
                {
                    if ( download_lines[i+1].IndexOf( "VhaBot_" ) >= 0 )
                    {
                        bot_release_version = download_lines[i+1].Substring(
                            download_lines[i+1].IndexOf( "VhaBot_" ) + 7 );
                        bot_release_version = bot_release_version.Remove(
                            bot_release_version.IndexOf( "_" ) );
                    }
                    else if ( ( download_lines[i+1].IndexOf( "Plugins_Only_" ) >= 0 ) && ( download_lines[i+1].IndexOf( ".tar.gz" ) >= 0 ) )
                    {
                        string url = download_lines[i+1].Substring(
                            download_lines[i+1].IndexOf( "href=\"" ) + 6 );
                        url = url.Remove( url.IndexOf( "\"" ) );
                        plugin_urls.Add( url );
                        string version = download_lines[i+3].Substring(
                            download_lines[i+3].IndexOf( "datetime=\"") + 10 );
                        version = version.Remove( version.IndexOf( "\"" ) );
                        plugin_versions.Add( version );
                    }
                }

            string latest_version = plugin_versions[0];
            int latest_idx = 0;
            for( int i=1; i<plugin_versions.Count; i++ )
                if ( string.Compare( plugin_versions[i], latest_version ) > 0 )
                {
                    latest_idx = i;
                    latest_version = plugin_versions[i];
                }

            return latest_idx;

        }

        public void OnCheckCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow window = new RichTextWindow(bot);

            window.AppendTitle( "System Updates" );
            window.AppendLineBreak();

            string bot_release_version = string.Empty;
            List<string> plugin_versions = new List<string>();
            List<string> plugin_urls = new List<string>();
            int idx = GetRepoVersions( ref bot_release_version,
                                       ref plugin_versions,
                                       ref plugin_urls );

            if ( idx < 0 )
            {
                bot.SendReply(e, "Unable to connect to update repository." );
                return;
            }

            bool update_available = false;

            string local_plugin_version = Updater.CheckVersion( "Plugins" );
            if ( local_plugin_version == string.Empty )
            {
                window.AppendNormal( "This appears to be a fresh install of Llie's Edition of VhaBot.  You must run plug-in update to initialize system.  WARNING: Any code changes you made to your plug-ins will be overwitten!" );
                window.AppendLineBreak(2);
                window.AppendNormal( "[" );
                window.AppendBotCommand( "Update Plug-in Package Now", "update plugins" );
                window.AppendNormal( "]" );
                update_available = true;
            }
            else if (string.Compare(bot_release_version,BotShell.VERSION)>0)
            {
                window.AppendNormal( "Your version of VhaBot is: \"" +
                                     BotShell.VERSION +
                                     "\" but the latest release is: \"" +
                                     bot_release_version + "\"" );
                window.AppendLineBreak(2);
                window.AppendNormal( "You should download the latest release package of VhaBot LE from " );
                window.AppendCommand( Updater.Bitbucket + this._repo_root + "/downloads", "/start " + Updater.Bitbucket + this._repo_root + "/downloads" );
                update_available = true;
            }
            else if ( string.Compare( plugin_versions[idx],
                                      local_plugin_version ) > 0 )
            {
                window.AppendNormal( "There is an updated plug-in package available.  WARNING: Any code changes you made to your plug-ins will be overwitten!" );
                window.AppendLineBreak(2);
                window.AppendNormal( "[" );
                window.AppendBotCommand( "Update Plug-in Package Now", "update plugins" );
                window.AppendNormal( "]" );

                window.AppendLineBreak(2);
                window.AppendNormal( "Your plug-in package version: " +
                                     local_plugin_version );
                window.AppendLineBreak();
                window.AppendNormal( "Current plug-in package version: " +
                                     plugin_versions[idx] );

                try
                {
                    string uri = Updater.Bitbucket + this._repo_root + "/downloads/ChangeLog.txt";
                    string changelog = Updater.GetURL ( uri );
                    window.AppendLineBreak(2);
                    window.AppendHighlight( "Changes in this update:" );
                    window.AppendLineBreak();
                    string[] lines = changelog.Split(new char[] { '\r', '\n' });
                    foreach( string line in lines )
                    {
                        window.AppendNormal( line );
                        window.AppendLineBreak();
                    }
                }
                catch { }
                update_available = true;
            }

            window.AppendLineBreak(2);
            window.AppendNormal( "[" );
            window.AppendBotCommand( "Update Individual Plug-ins", "update plugin" );
            window.AppendNormal( "]" );

            if ( update_available )
                bot.SendReply(e, "Update information »» ", window );
            else
                bot.SendReply(e, "No updates available." );
        }

        public void OnUpdateCommand(BotShell bot, CommandArgs e)
        {

            string bot_release_version = string.Empty;
            List<string> plugin_versions = new List<string>();
            List<string> plugin_urls = new List<string>();
            int idx = GetRepoVersions( ref bot_release_version,
                                       ref plugin_versions,
                                       ref plugin_urls );

            string local_plugin_version = Updater.CheckVersion( "Plugins" );

            if ( string.Compare( plugin_versions[idx],
                                 local_plugin_version ) == 0 )
            {
                bot.SendReply(e, "Plug-ins are up to date." );
                return;
            }

            bot.SendReply(e, "Downloading and installing latest plug-ins..." );

            string URL = Updater.Bitbucket + plugin_urls[idx];
            Console.WriteLine( "[Updater] Downloading: " + URL  );

            Untar.DownloadExpandExtract( URL );

            Updater.UpdateVersion( "Plugins", plugin_versions[idx] );

            bot.SendReply(e, "Please restart your bot to activate the latest plug-ins." );

        }

        public void OnUpdatePluginCommand(BotShell bot, CommandArgs e)
        {

            List<string> Default_Plugins = new List<string>();
            List<string> Default_Updates = new List<string>();
            List<string> Llies_Plugins = new List<string>();
            List<string> Llies_Updates = new List<string>();

            Updater.GetPluginUpdates ( this._def_root,
                                       ref Default_Plugins,
                                       ref Default_Updates );

            Updater.GetPluginUpdates ( this._llie_root,
                                       ref Llies_Plugins,
                                       ref Llies_Updates );

            if ( e.Args.Length == 0 )
            {
                RichTextWindow window = new RichTextWindow(bot);

                window.AppendTitle( "Individual Plug-in Update" );
                window.AppendLineBreak();

                window.AppendNormal( "This feature updates an individual plug-in to the latest version in the source code repository.  The plug-in in the source code repository is less rigorously tested than the plug-in packages.  Use this feature only if you know there is a specific fix to a plug-in that has been committed to the repository that has not yet been rolled-up into an update package.");
                window.AppendLineBreak(2);

                window.AppendLineBreak();
                window.AppendHighlight( Updater.Bitbucket + this._def_root );
                window.AppendLineBreak();
                int n = 0;
                foreach( string plugin in Default_Plugins )
                {
                    window.AppendNormal( "[" );
                    window.AppendBotCommand( "Update", "update plugin " +
                                             plugin );
                    window.AppendNormal( "] " );
                    window.AppendHighlight( plugin.Substring( 0, plugin.IndexOf( "." ) ) );
                    window.AppendNormal( " " + Default_Updates[n++] );
                    window.AppendLineBreak();
                }

                window.AppendLineBreak();
                window.AppendHighlight( Updater.Bitbucket + this._llie_root );
                window.AppendLineBreak();
                n = 0;
                foreach( string plugin in Llies_Plugins )
                {
                    window.AppendNormal( "[" );
                    window.AppendBotCommand( "Update", "update plugin " +
                                             plugin );
                    window.AppendNormal( "] " );
                    window.AppendHighlight( plugin.Substring( 0, plugin.IndexOf( "." ) ) );
                    window.AppendNormal( " " + Llies_Updates[n++] );
                    window.AppendLineBreak();                        
                }

                bot.SendReply(e, "Plug-ins »» ", window );
                return;
            }

            if ( Updater.Disabled() == true )
            {
                bot.SendReply(e, "The updater system has been disabled due to an error.  It's possible that Bitbucket has changed their web site or is down.  Please check your logs and configuration.  You will have to restart the bot to reactivate the updater system." );
                return;
            }

            string plugin_file = e.Args[0];

            bot.SendReply(e, "Downloading and installing " + plugin_file + "..." );

            string URL = Updater.Bitbucket + this._def_root + "/raw/tip/";
            if ( Llies_Plugins.Contains(plugin_file) )
                URL = Updater.Bitbucket + this._llie_root + "/raw/tip/";

            URL += plugin_file;

            Console.WriteLine( "[Updater] Downloading: " + URL  );

            try
            {

                string PluginSource = Updater.GetURL ( URL );
                Encoding utf8WithoutBom = new System.Text.UTF8Encoding(false);
                using ( StreamWriter writer =
                        new StreamWriter( "plugins" +
                                          Path.DirectorySeparatorChar +
                                          plugin_file , false,
                                          utf8WithoutBom ) )
                {
                    writer.Write( PluginSource );
                }
                bot.SendReply(e, "Please restart your bot to activate the updated plug-in." );
            }
            catch
            {
                bot.SendReply(e, "An error occurred attempting to update " +
                              plugin_file );
            }
        }

        private bool Backup()
        {

            if ( this._num_backups == 0 )
                return true;

            string config_dir = "config" + Path.DirectorySeparatorChar +
                _bot.Character.ToLower() + "@" +
                _bot.Dimension.ToString().ToLower();

            string backup_root = config_dir + Path.DirectorySeparatorChar +
                "backups";

            // Create backup directory, if it doesn't exist
            if ( ! Directory.Exists( backup_root ) )
                Directory.CreateDirectory( backup_root );

            // Look in backup directory for old backups and trim to
            // one-minus the number of backups specified
            string[] backup_dirs = Directory.GetDirectories( backup_root );
            if ( backup_dirs.Length > this._num_backups )
                for ( int i=this._num_backups-1; i<backup_dirs.Length; i++ )
                {
                    Directory.Delete( backup_root +
                                      Path.DirectorySeparatorChar +
                                      "Backup" + i.ToString(), true );
                }

            // Rotate backup directories
            for( int i=this._num_backups-1; i>0; i-- )
            {
                int j=i-1;
                if ( Directory.Exists( backup_root +
                                       Path.DirectorySeparatorChar +
                                       "Backup" + j.ToString() ) )
                    Directory.Move( backup_root +
                                    Path.DirectorySeparatorChar +
                                    "Backup" + j.ToString(),
                                    backup_root +
                                    Path.DirectorySeparatorChar +
                                    "Backup" + i.ToString() );
            }

            string [] databases = Directory.GetFiles( config_dir );

            string backup_dir = backup_root + Path.DirectorySeparatorChar +
                "Backup0";
            if ( Directory.Exists( backup_dir ) )
                return false;
                
            Directory.CreateDirectory( backup_dir );
            for ( int i=0; i<databases.Length; i++ )
            {
                string dest_file = backup_dir + Path.DirectorySeparatorChar +
                    databases[i].Substring(databases[i].LastIndexOf(Path.DirectorySeparatorChar)+1);
                File.Copy( databases[i], dest_file );
            }

            return true;
        }

        public void OnBackupCommand(BotShell bot, CommandArgs e)
        {
            if ( ! Backup() )
            {
                Console.WriteLine( "[Updater] Error in backup system.  Clear the backup directory and retry." );
                _bot.SendReply(e, "Error in backup system.  Clear the backup directory and retry." );
            }
            else
            {
                Console.WriteLine( "[Updater] Backup completed." );
                _bot.SendReply(e, "Backup completed." );
            }
            
        }

        public bool CheckBackupIntgegrity( int backup_n )
        {
            string backup = "Backup" + backup_n.ToString();
            return CheckBackupIntgegrity( backup );
        }

        public bool CheckBackupIntgegrity( string backup )
        {

            string config_dir = "config" + Path.DirectorySeparatorChar +
                _bot.Character.ToLower() + "@" +
                _bot.Dimension.ToString().ToLower();

            string backup_root = config_dir + Path.DirectorySeparatorChar +
                "backups";

            string backup_sub = "backups" + Path.DirectorySeparatorChar +
                backup + Path.DirectorySeparatorChar;
            
            string [] databases =
                Directory.GetFiles( backup_root + Path.DirectorySeparatorChar +
                                    backup );

            string front =
                databases[0].Substring( 0,
                                        databases[0].LastIndexOf(
                                            Path.DirectorySeparatorChar)-1 );

            for( int i=0; i<databases.Length; i++ )
             {
                string back = databases[i].Substring(front.Length + 2);
                back = back.Remove( back.IndexOf(".") );
                Config test_db = new Config( _bot.ID, backup_sub + back);

                using (IDbCommand command=test_db.Connection.CreateCommand())
                {
                    command.CommandText = "PRAGMA integrity_check;";
                    IDataReader reader = command.ExecuteReader();
                    while ( reader.Read() )
                    {
                        if ( reader.GetString(0) != "ok" )
                            return false;
                    }
                }
             }

            return true;
        }

        public void OnRestoreCommand(BotShell bot, CommandArgs e)
        {
            int backup_n = 0;
            if ( e.Args.Length > 0 )
                try
                {
                    backup_n = Convert.ToInt32( e.Args[0] );
                }
                catch { }

            string config_dir = "config" + Path.DirectorySeparatorChar +
                _bot.Character.ToLower() + "@" +
                _bot.Dimension.ToString().ToLower();

            string backup_root = config_dir + Path.DirectorySeparatorChar +
                "backups";

            string backup = "Backup" + backup_n.ToString();

            if ( ! Directory.Exists( backup_root + Path.DirectorySeparatorChar +
                                     backup ) )
            {
                _bot.SendReply(e, "Backup " + backup_n.ToString() +
                               " does not exist.  Can not restore." );
                return;
            }

            if ( CheckBackupIntgegrity( backup_n ) != true )
            {
                Console.WriteLine( "[Updater] Backup " + backup_n.ToString() +
                                   " failed integrity check.  Not restoring." );
                _bot.SendReply(e, "Backup " + backup_n.ToString() +
                               " failed integrity check.  Not restoring." );
                return;
            }

            // Restore the Nth backup

            string [] backup_databases =
                Directory.GetFiles( backup_root + Path.DirectorySeparatorChar +
                                    backup );

            for ( int i=0; i<backup_databases.Length; i++ )
            {
                string dbname = backup_databases[i].Substring(
                    backup_databases[i].LastIndexOf(
                        Path.DirectorySeparatorChar));
                File.Copy( backup_databases[i], config_dir + dbname,
                    true );
            }

            Console.WriteLine( "[Updater] Backup " + backup_n.ToString() +
                               " restored.  Please restart bot." );
            _bot.SendReply(e, "Backup " + backup_n.ToString() +
                           " Restored.  Please restart bot." );

        }

        private void BackupUsers()
        {

            Config BackupUserDB = new Config(_bot.ToString(), "backups" +
                                             Path.DirectorySeparatorChar +
                                             "backup");

            BackupUserDB.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS CORE_Members (Username VARCHAR(14) UNIQUE, UserID INTEGER UNIQUE, UserLevel VARCHAR(255), AddedBy VARCHAR(14), AddedOn INTEGER)");
            BackupUserDB.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS CORE_Alts (Username VARCHAR(14) UNIQUE, UserID INTEGER UNIQUE, Main VARCHAR(14))");
            BackupUserDB.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS CORE_Notify (Section VARCHAR(255), Username VARCHAR(255), LastOnline INTEGER)");
            
            Config UserDB = new Config(_bot.ToString(), "users");
            string query = String.Format("SELECT * FROM CORE_Members" );
            using (IDbCommand command = UserDB.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        string insert_command = string.Format( "REPLACE INTO CORE_Members VALUES ('{0}', {1}, '{2}', '{3}', {4})", reader.GetString(0), reader.GetInt64(1), reader.GetString(2), reader.GetString(3), reader.GetInt64(4) );
                        BackupUserDB.ExecuteNonQuery(insert_command);
                    }
                    catch { }
                }
            }

            query = String.Format("SELECT * FROM CORE_Alts" );
            using (IDbCommand command = UserDB.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        string insert_command = string.Format( "REPLACE INTO CORE_Alts VALUES ('{0}', {1}, '{2}')", reader.GetString(0), reader.GetInt64(1), reader.GetString(2) );
                        BackupUserDB.ExecuteNonQuery(insert_command);
                    }
                    catch { }
                }
            }

            Config FriendDB = new Config(_bot.ToString(), "notify");
            query = String.Format("SELECT * FROM CORE_Notify WHERE Section = 'guestlist'" );
            using (IDbCommand command = FriendDB.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        string insert_command = string.Format( "REPLACE INTO CORE_Notify VALUES ('{0}', '{1}', {2})", reader.GetString(0), reader.GetString(1), reader.GetInt64(2) );
                        BackupUserDB.ExecuteNonQuery(insert_command);
                    }
                    catch { }
                }
            }

        }

        private void RestoreUsers()
        {

            Config BackupUserDB = new Config(_bot.ToString(), "backups" +
                                             Path.DirectorySeparatorChar +
                                             "backup");

            Config UserDB = new Config(_bot.ToString(), "users");
            UserDB.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS CORE_Members (Username VARCHAR(14) UNIQUE, UserID INTEGER UNIQUE, UserLevel VARCHAR(255), AddedBy VARCHAR(14), AddedOn INTEGER)");
            UserDB.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS CORE_Alts (Username VARCHAR(14) UNIQUE, UserID INTEGER UNIQUE, Main VARCHAR(14))");

            string query = String.Format("SELECT * FROM CORE_Members" );
            using (IDbCommand command = BackupUserDB.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        string insert_command = string.Format( "REPLACE INTO CORE_Members VALUES ('{0}', {1}, '{2}', '{3}', {4})", reader.GetString(0), reader.GetInt64(1), reader.GetString(2), reader.GetString(3), reader.GetInt64(4) );
                        UserDB.ExecuteNonQuery(insert_command);
                    }
                    catch {}
                }
            }

            query = String.Format("SELECT * FROM CORE_Alts" );
            using (IDbCommand command = BackupUserDB.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        string insert_command = string.Format( "REPLACE INTO CORE_Alts VALUES ('{0}', {1}, '{2}')", reader.GetString(0), reader.GetInt64(1), reader.GetString(2) );
                        UserDB.ExecuteNonQuery(insert_command);
                    }
                    catch {}
                }
            }

            Config FriendDB = new Config(_bot.ToString(), "notify");
            FriendDB.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS CORE_Notify (Section VARCHAR(255), Username VARCHAR(255), LastOnline INTEGER)");

            query = String.Format("SELECT * FROM CORE_Notify" );
            using (IDbCommand command = BackupUserDB.Connection.CreateCommand())
            {
                command.CommandText = query;
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    try
                    {
                        string insert_command = string.Format( "REPLACE INTO CORE_Notify VALUES ('{0}', '{1}', {2})", reader.GetString(0), reader.GetString(1), reader.GetInt64(2) );
                        FriendDB.ExecuteNonQuery(insert_command);
                    }
                    catch {}
                }
            }

        }

        public void OnBackupUsersCommand(BotShell bot, CommandArgs e)
        {
            try
            {
                BackupUsers();
            }
            catch
            {
                Console.WriteLine( "[Updater] An error occurred attempting to backup users." );
                _bot.SendReply(e, "An error occurred attempting to backup users." );
                return;
            }
            Console.WriteLine( "[Updater] Backup of users completed." );
            _bot.SendReply(e, "Backup of users completed." );
        }

        public void OnRestoreUsersCommand(BotShell bot, CommandArgs e)
        {
            try
            {
                RestoreUsers();
            }
            catch
            {
                Console.WriteLine( "[Updater] An error occurred attempting to restore users." );
                _bot.SendReply(e, "An error occurred attempting to restore users." );
                return;
            }
            Console.WriteLine( "[Updater] Restore of users completed." );
            _bot.SendReply(e, "Restore of users completed." );
            
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "update":
                return "Checks whether there are updates to VhaBot LE plug-ins.\n" +
                    "Usage: /tell " + bot.Character + " update";

            case "update plugins":
                return "Updates all VhaBot LE plug-ins.\n" +
                    "Usage: /tell " + bot.Character + " update plugins";

            case "update plugin":
                return "Pull the latest version of a specific VhaBot LE plug-in.\n" +
                    "Usage: /tell " + bot.Character + " update plugin [plugin_name]";

            case "backup":
                return "Manually create a backup of current configuration.\n" +
                    "Usage: /tell " + bot.Character + " backup";

            case "backup users":
                return "Create a backup of users, alts, and guestlist.\n" +
                    "Usage: /tell " + bot.Character + " backup leaders";

            case "restore":
                return "Restore a configuration from backup.  If the backup number is ommitted, the last update is restored.\n" +
                    "Usage: /tell " + bot.Character + " restore [n]";

            case "restore users":
                return "Restore users, alts, and guestlist from backup.\n" +
                    "Usage: /tell " + bot.Character + " restore guests";

            }

            return null;
        }

    }

}

