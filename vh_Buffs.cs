namespace VhaBot.Plugins
{

    public class VhBuffs : PluginBase
    {

        public VhBuffs()
        {
            this.Name = "Buff Listing Function";
            this.InternalName = "VhBuffs";
            this.Author = "Fletche | Flashpointblack";
            this.DefaultState = PluginState.Installed;
            this.Description = "Responds to buffs messages";
            this.Version = 105;
            this.Commands = new Command[] {
                new Command("buffs", true, UserLevel.Guest),
                new Command("basebuffs", true, UserLevel.Guest),
                new Command("skillbuffs", true, UserLevel.Guest),
                new Command("buffitems", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {

            case "buffs":
                this.OnBuffsCommand(bot, e);
                break;

            case "basebuffs":
                this.OnBaseBuffsCommand(bot, e);
                break;

            case "skillbuffs":
                this.OnSkillBuffsCommand(bot, e);
                break;

            case "buffitems":
                this.OnBuffItemsCommand(bot, e);
                break;

            }
        }

		public void OnBuffsCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow buffsWindow = new RichTextWindow(bot);
            buffsWindow.AppendTitle("Class Buffs");
            buffsWindow.AppendLineBreak();

            buffsWindow.AppendBotCommand("Base Attribute Buffs", "basebuffs");
            buffsWindow.AppendLineBreak(2);
            buffsWindow.AppendBotCommand("Skill Buffs", "skillbuffs");

            buffsWindow.AppendLineBreak(2);

            buffsWindow.AppendString("Buffing items");
            buffsWindow.AppendLineBreak(2);
            buffsWindow.AppendBotCommand("Strength", "buffitems strength");
            buffsWindow.AppendLineBreak();
            buffsWindow.AppendBotCommand("Stamina",  "buffitems stamina");
            buffsWindow.AppendLineBreak();
            buffsWindow.AppendBotCommand("Sense", "buffitems sense");
            buffsWindow.AppendLineBreak();
            buffsWindow.AppendBotCommand("Agility", "buffitems agility");
            buffsWindow.AppendLineBreak();
            buffsWindow.AppendBotCommand("Intelligence", "buffitems intelligence");
            buffsWindow.AppendLineBreak();
            buffsWindow.AppendBotCommand("Psychic", "buffitems psychic");

            bot.SendReply(e, " Buffs and Buffing Items »» ", buffsWindow);
        }

		public void OnBaseBuffsCommand(BotShell bot, CommandArgs e)
        {  
            RichTextWindow baseBuffsWindow = new RichTextWindow(bot);
            baseBuffsWindow.AppendTitle("Base Attribute Buffs");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorOrange, ">> Buffs listed in the same color do not stack with one another. <<");
		
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendHighlight("Strength");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Enforcer");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorRed, "  * +5 to +27 - Essence of <...> (5-47 NCU)");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +54 - Imp. Essence of Behemoth (56 NCU, Level 215)");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +40 - Prodigious Strength (42 NCU)");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Martial Artist");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +25 - Muscle Booster (22 NCU)");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +12 - Muscle Stim (7 NCU)");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Doctor");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +10 - Enlarge (12 NCU)");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +20 - Iron Circle (26 NCU)");

            baseBuffsWindow.AppendLineBreak(3);
            baseBuffsWindow.AppendHighlight("Agility");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Agent");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendNormal("  * +25 - Feline Grace (17 NCU)");

            baseBuffsWindow.AppendLineBreak(3);
            baseBuffsWindow.AppendHighlight("Stamina");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Enforcer");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorRed, "  * +5 to +27 - Essence of <...> (5-47 NCU)");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +54 - Imp. Essence of Behemoth (56 NCU, Level 215)");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Doctor");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +10 - Enlarge (12 NCU)");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendColorString(RichTextWindow.ColorGreen, "  * +20 - Iron Circle (26 NCU)");

            baseBuffsWindow.AppendLineBreak(3);
            baseBuffsWindow.AppendHighlight("Intelligence");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Nano-Technician");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendNormal("  * +20 - Neuronal Stimulator (7 NCU)");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Bureaucrat");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendNormal("  * +3 - Imp. Cut Red Tape (29 NCU)");

            baseBuffsWindow.AppendLineBreak(3);
            baseBuffsWindow.AppendHighlight("Sense");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Agent");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendNormal("  * +15 - Enhanced Senses (9 NCU)");

            baseBuffsWindow.AppendLineBreak(3);
            baseBuffsWindow.AppendHighlight("Psychic");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Nano-Technician");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendNormal("  * +20 - Neuronal Stimulator (7 NCU)");
            baseBuffsWindow.AppendLineBreak(2);
            baseBuffsWindow.AppendNormal("  Bureaucrat");
            baseBuffsWindow.AppendLineBreak();
            baseBuffsWindow.AppendNormal("  * +3 - Imp. Cut Red Tape (29 NCU)");

            bot.SendReply(e, " Base Attribute Buffs »» ", baseBuffsWindow);

        }
	
		public void OnSkillBuffsCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow skillBuffsWindow = new RichTextWindow(bot);
            skillBuffsWindow.AppendTitle("Skill Buffs");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendHighlight("Note:  This list does not include self only buffs.");
		
            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Meta-Physicist Nano Skills (Individual)");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  +25 - Teachings (6 NCU)\n");
            skillBuffsWindow.AppendNormal("  +50 - Mastery (16 NCU)\n");
            skillBuffsWindow.AppendNormal("  +90 - Infuse (39 NCU)\n");
            skillBuffsWindow.AppendNormal("  +140 - Mochams (50 NCU)");
		
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendHighlight("Meta-Physicist Nano Skills (Composite)");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  +25 - Teachings (6 NCU)  [SL, Level 15]\n");
            skillBuffsWindow.AppendNormal("  +50 - Mastery (13 NCU) [SL, Level 40]\n");
            skillBuffsWindow.AppendNormal("  +90 - Infuse (25 NCU) [SL, Level 90]\n");
            skillBuffsWindow.AppendNormal("  +140 - Mochams (48 NCU) [SL, Level 175]");

            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendHighlight("The Wrangle");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  Trader");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * All attack and nano skills can be buffed from 3 to 131 [153 w/perks]\n    (3 NCU - 58 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("1 Handed Blunt");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Enforcer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +87 - Brutal Thug (24 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("2 Handed Blunt");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Enforcer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +87 - Brutal Thug (24 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Assault Rifle");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Soldier");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +60 - Assault Rifle Mastery (16 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Aimed Shot");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Adventurer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +20 - Eagle Eye (24 NCU)");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Agent");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +130 - Take the Shot (47 NCU)");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +15 - Sniper's Bliss (14 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Burst");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Fixer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +7 - Minor Suppressor (1 NCU)");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Soldier");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +110 - Riot Control (39 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Brawl");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Martial Artist");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +45 - Dirty Fighter (11 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Martial Arts");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Martial Artist");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +12 - Lesser Controlled Rage (1 NCU)");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +60 - Martial Arts Mastery (16 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("MG/SMG");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Fixer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +8 - Minor Suppressor (1 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Perception");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Adventurer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +120 - Lupus Oculus (15 NCU)");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +80 - Eagle Eye (24 NCU)");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Fixer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +130 - Karma Harvest (47 NCU)");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +107 - Blood Makes Noise (36 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Pistol");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Bureaucrat");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +20 - Gunslinger (5 NCU)\n    (Does not stack with Pistol Mastery)");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Soldier");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +40 - Pistol Mastery (8 NCU)");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Engineer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +120 - Extreme Prejudice (43 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Riposte");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Martial Artist");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +50 - Return Attack (45 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Rifle");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Agent");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +120 - Unexpected Attack (42 NCU)");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +50 - Sniper's Bliss (14 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Sneak Attack");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Agent");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +30 - Unexpected Attack (42 NCU)");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Fixer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +64 - Backpain (18 NCU)");

            skillBuffsWindow.AppendLineBreak(3);
            skillBuffsWindow.AppendHighlight("Treatment");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Adventurer");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +60 - Robust Treatment (+42 NCU)");
            skillBuffsWindow.AppendLineBreak(2);
            skillBuffsWindow.AppendNormal("  Doctor");
            skillBuffsWindow.AppendLineBreak();
            skillBuffsWindow.AppendNormal("  * +80 - Superior First Aid (37 NCU)");

            bot.SendReply(e, " Skill Buffs »» ", skillBuffsWindow);
        }

        private void AppendValue( ref RichTextWindow buffItemsWindow,
                                  int low )
        {
            AppendRange( ref buffItemsWindow, low, 0 );
        }

        private void AppendRange( ref RichTextWindow buffItemsWindow,
                                  int low, int high )
        {
            if( low < 10 )
            {
                buffItemsWindow.AppendColorStart("#000000");
                buffItemsWindow.AppendString("0");
                buffItemsWindow.AppendColorEnd();
            }
            buffItemsWindow.AppendString( low.ToString() );
            if( high == 0 )
            {
                buffItemsWindow.AppendColorStart("#000000");
                buffItemsWindow.AppendString("-00");
                buffItemsWindow.AppendColorEnd();
            }
            else if( high < 10 )
            {
                buffItemsWindow.AppendString("-");
                buffItemsWindow.AppendColorStart("#000000");
                buffItemsWindow.AppendString("0");
                buffItemsWindow.AppendColorEnd();
                buffItemsWindow.AppendString( high.ToString() );
            }
            else
            {
                buffItemsWindow.AppendString("-");
                buffItemsWindow.AppendString( high.ToString() );
            }

            buffItemsWindow.AppendColorStart("#000000");
            buffItemsWindow.AppendString("00");
            buffItemsWindow.AppendColorEnd();
            
        }

		public void OnBuffItemsCommand(BotShell bot, CommandArgs e)
        {
            if ( e.Args.Length == 0 )
            {
                bot.SendReply( e, "Error: Usage: buffitems [ability]" );
                return;
            }

            string skill = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(e.Args[0]);

            RichTextWindow buffItemsWindow = new RichTextWindow(bot);

            buffItemsWindow.AppendNormalStart();
            buffItemsWindow.AppendTitle("Modifier   Item");
            buffItemsWindow.AppendLineBreak();

            switch ( skill.ToLower() )
            {

            case "stamina":

                buffItemsWindow.AppendHighlight("                           Weapons"); buffItemsWindow.AppendLineBreak();
                AppendRange( ref buffItemsWindow, 40, 60 ); buffItemsWindow.AppendItem("Spear of Forbidden Ceremonies", 247080, 247080, 300); buffItemsWindow.AppendString(" - QL Dependant"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 27 ); buffItemsWindow.AppendItem("Elite MTI Aleph 99", 152338, 152338, 200); buffItemsWindow.AppendString(" - QL 200 - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("HSR Arms N.M.", 123890, 123891, 100); buffItemsWindow.AppendString(" - QL 100 to 120 - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Freedom Arms 3927 G2", 123825, 123825, 200); buffItemsWindow.AppendString(" - QL Dependant"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 24 ); buffItemsWindow.AppendItem("Krutt Assault 219 Waltzing Queen Special", 124284, 124284, 184); buffItemsWindow.AppendString(" - QL 184 - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 24 ); buffItemsWindow.AppendItem("OT M500 SWAT Monster", 124356, 124356, 180); buffItemsWindow.AppendString("  QL 180 - Rollable - Dual Wield "); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("OT Eskorpio 21 Hrx", 124509, 124509, 188); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Freedom Arms 3927k Ultra", 123836, 123836, 100); buffItemsWindow.AppendString(" - QL 100 to 107 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Freedom Arms 3927 Notum", 123819, 123820, 108); buffItemsWindow.AppendString(" - QL 108 to 149 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Excellent Concrete Cushion", 125458, 125458, 160); buffItemsWindow.AppendString(" - QL 160 - Rollable - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 18 ); buffItemsWindow.AppendItem("High Quality MTI Aleph 99", 152336, 152337, 151); buffItemsWindow.AppendString(" - QL 151 to 199 - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 18 ); buffItemsWindow.AppendItem("Krutt Assault 219 Waltzing Queen", 124282, 124283, 159); buffItemsWindow.AppendString (" - QL 159 to 183 - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Krutt Assault 219 Fandango", 124280, 124281, 84); buffItemsWindow.AppendString (" - QL 84 to 157  - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Explosif's Polychromatic Pillows", 258344, 258344, 300); buffItemsWindow.AppendString(" - QL 300 - Dual Wield - Low Reqs"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Concrete Cushion", 125456, 125457, 10); buffItemsWindow.AppendString(" - QL 10 to 159  - Dual Wield - Rollable"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                           Armors"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Burden of Competence", 244718, 244718, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Combat Chestpiece", 269993, 269993, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Vest of Torpid Sunrays", 245854, 245854, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Covering", 269997, 269997, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Frederickson Micro-kinetic Sleeves", 152710, 152710, 200); buffItemsWindow.AppendString(" - Can Wear Two"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Miy's Body Armor", 268850, 268851, 300); buffItemsWindow.AppendString(" - Available in different types "); buffItemsWindow.AppendBotCommand("View", "Items Miy Body Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Masterpiece Ancient Bracer", 267780, 267780, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Ring of Endurance", 269188, 269189, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Miy's Scary/Tank Armor Boots", 268894, 268895, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 11 ); buffItemsWindow.AppendItem("Cosmic Guide of the Pisces", 244641, 244641, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Body of the Servants of Eight", 160430, 160430, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Dillon Armor", 160407, 160408, 200); buffItemsWindow.AppendString(" - Each piece gives 10 but boots @ QL 200"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Ring of Essence", 269190, 269191, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("I am the Bear", 163637, 163637, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Nova Dillon Armor Boots", 163941, 163942, 200); buffItemsWindow.AppendString(" - +4 per piece - Use instead of Dillon Boots"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 2 ); buffItemsWindow.AppendItem("Anything", 165215, 165215, 200); buffItemsWindow.AppendString(" - Goes ANYWHERE"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                        HUDs/Utilities"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 50 ); buffItemsWindow.AppendItem("Maar's Red Belt of Double Power", 244988, 244988, 300); buffItemsWindow.AppendString(" - Drops from The Night Heart"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Adrenaline Factory", 251800, 251801, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Codex Divello", 269827, 269827, 250); buffItemsWindow.AppendString(" - Penumbra Quest"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Component Platform of Stamina", 260694, 260694, 250); buffItemsWindow.AppendString(" - The Best of Inferno Quest"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Endorphin Factory", 251808, 251808, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Enhanced Safeguarded NCU Memory Unit", 269986, 269986, 200); buffItemsWindow.AppendString(" - Dust Brigade"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("The Dantian Belt of Life", 245429, 245429, 125); buffItemsWindow.AppendString(" - QL 80 or higher Belt + Pit Demon Heart"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                         Implants"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 55 ); buffItemsWindow.AppendItem("Chest Implant - Shiny - QL 200", 101787, 101788, 200); buffItemsWindow.AppendString(" - QL 1 to 200"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 33 ); buffItemsWindow.AppendItem("Leg Implant - Bright - QL 200", 101785, 101786, 200); buffItemsWindow.AppendString(" - QL 1 to 200"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Waist - Faded - QL 200", 101783, 101784, 200); buffItemsWindow.AppendString(" - QL 1 to 200"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Sinew of Tarasque", 158764, 158764, 1); buffItemsWindow.AppendString(" - NODROP From Tarasque - Right Arm"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Keeper's Vitality", 204650, 204650, 1); 

                break;

            case "strength":

                buffItemsWindow.AppendHighlight("                           Weapons"); buffItemsWindow.AppendLineBreak();
                AppendRange( ref buffItemsWindow, 40, 60 ); buffItemsWindow.AppendItem("Spear of Forbidden Ceremonies", 247080, 247080, 300); buffItemsWindow.AppendString(" - QL Dependant"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Freedom Arms 3927 G2", 123825, 123825, 200); buffItemsWindow.AppendString(" - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("OT Eskorpio 21 Hrx", 124509, 124509, 188); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Excellent Concrete Cushion", 125458, 125458, 160); buffItemsWindow.AppendString(" - QL 160 - Rollable - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Freedom Arms 3927k Ultra", 123836, 123836, 100); buffItemsWindow.AppendString(" - QL 100 to 107 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Freedom Arms 3927 Notum", 123819, 123820, 108); buffItemsWindow.AppendString(" - QL 108 to 149 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 18 ); buffItemsWindow.AppendItem("OT M500 SWAT Monster", 124356, 124356, 180); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Explosif's Polychromatic Pillows", 258344, 258344, 300); buffItemsWindow.AppendString(" - QL 300 - Dual Wield - Low Reqs"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Freedom Arms 3927k", 123834, 123835, 96); buffItemsWindow.AppendString(" - QL 96 TO 99 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8  ); buffItemsWindow.AppendItem("Concrete Cushion", 125456, 125457, 10); buffItemsWindow.AppendString(" - QL 10 to 159  - Dual Wield - Rollable"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                           Armors"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Burden of Competence", 244718, 244718, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Combat Chestpiece", 269993, 269993, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Covering", 269997, 269997, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Frederickson Micro-kinetic Sleeves", 152710, 152710, 200); buffItemsWindow.AppendString(" - Can Wear Two"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Ring of Essence", 269190, 269191, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Masterpiece Ancient Bracer", 267780, 267780, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 17 ); buffItemsWindow.AppendString( "Miy's "); buffItemsWindow.AppendItem("Ranged", 268862, 268863, 200); buffItemsWindow.AppendString("/"); buffItemsWindow.AppendItem("Tank", 268874, 268875, 200); buffItemsWindow.AppendString(" Body Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Miy's Armor Boots", 268858, 268859, 300); buffItemsWindow.AppendBotCommand(" - View Others", "Items Miy Armor Boots");buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Shoulderpads of Asteroid Calling", 245858, 245858, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 14 ); buffItemsWindow.AppendItem("Bracer of Striking Force", 251755, 251756, 300); buffItemsWindow.AppendLineBreak();
                AppendRange( ref buffItemsWindow, 9, 14 ); buffItemsWindow.AppendItem("Supporting Carbonan Holster", 151714, 151715, 200); buffItemsWindow.AppendLineBreak();
                AppendRange( ref buffItemsWindow, 10, 13 ); buffItemsWindow.AppendItem("Boots of Violent Motion", 207290, 207291, 200); buffItemsWindow.AppendString(" - QL 200 to 250"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Ring of Endurance", 269188, 269189, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("I am the Bear", 163637, 163637, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Might of the Revenant", 206013, 206013, 1); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Lamnidae Gloves", 252990, 252991, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Specialized Dustbrigade Vambrace - Atrox Breed", 168881, 168881, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Meta-Cerebellum Pocket", 245721, 245721, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Meta-Cerebellum Ring", 245722, 245722, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Storage Device", 245723, 245723, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Upper Part of Ljotur", 165364, 165364, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Sekutek Chilled Plasteel Armor", 160391, 160392, 200); buffItemsWindow.AppendString(" - 5 STR per piece ");  buffItemsWindow.AppendBotCommand("View Others", "Items 200 Sekutek Chilled Plasteel Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 3 ); buffItemsWindow.AppendItem("Barrow Strength", 204653, 204653, 1); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 2 ); buffItemsWindow.AppendItem("Anything", 165215, 165215, 200); buffItemsWindow.AppendString(" - Goes ANYWHERE"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                        HUDs/Utilities"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 50 ); buffItemsWindow.AppendItem("Maar's Red Belt of Double Power", 244988, 244988, 300); buffItemsWindow.AppendString(" - Drops from The Night Heart"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Dreadloch Endurance Booster - Nanomage Edition", 267167, 267167, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Adrenaline Factory", 251800, 251801, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Component Platform of Strength", 260695, 260695, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Dreadloch Endurance Booster", 267166, 267166, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Sash of Scorpio Strength", 244648, 244648, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Enhanced Safeguarded NCU Memory Unit", 269986, 269986, 200); buffItemsWindow.AppendString(" - Dust Brigade"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("The Cause of Headache", 245321, 244988, 150); buffItemsWindow.AppendString(" - NOT unique"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                          Implants"); buffItemsWindow.AppendLineBreak();
AppendValue( ref buffItemsWindow, 55 ); buffItemsWindow.AppendItem("Right Arm Implant - Shiny - QL 200", 101775, 101776, 200); buffItemsWindow.AppendLineBreak();
AppendValue( ref buffItemsWindow, 33 ); buffItemsWindow.AppendItem("Left Arm Implant - Bright - QL 200", 101773, 101774, 200); buffItemsWindow.AppendLineBreak();
AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Chest Implant - Faded - QL 200", 101771, 101772, 200); buffItemsWindow.AppendLineBreak();
AppendValue( ref buffItemsWindow, 3  ); buffItemsWindow.AppendItem("Keeper's Vitality", 204650, 204650, 1); 

                break;

            case "intelligence":

                buffItemsWindow.AppendHighlight("                           Weapons"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 50 ); buffItemsWindow.AppendItem("Prohibited Hand-Mortar of Bacam-Xum", 246873, 246873, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 32 ); buffItemsWindow.AppendItem("Sword of Dawn", 246244, 246244, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 32 ); buffItemsWindow.AppendItem("Sword of Dusk", 246256, 246256, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Sentinels Personal Edition G1-Pro X", 124528, 124528, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("V-five Breathing Space", 129081, 129081, 180); buffItemsWindow.AppendString(" - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 28 ); buffItemsWindow.AppendItem("Sentinels Personal Edition G1-Pro VII", 124526, 124527, 161); buffItemsWindow.AppendString(" - QL 161 to 199"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("MTI SL70 Garganthua", 124182, 124182, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("O.E.T. Co. Maharanee", 123867, 123867, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 24 ); buffItemsWindow.AppendItem("Krutt Assault 219 Waltzing Queen Special", 124284, 124284, 184); buffItemsWindow.AppendString(" - QL 184 - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 24 ); buffItemsWindow.AppendItem("Sentinels Personal Edition G1-Pro IV", 124524, 124525, 141); buffItemsWindow.AppendString(" - QL 141 to 160"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("O.E.T. Co. Jess", 123863, 123864, 100); buffItemsWindow.AppendString(" - QL 100 to 159 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("O.E.T. Co. Pelastio V3", 123865, 123866, 160); buffItemsWindow.AppendString(" - QL 160 to 199 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Sentinels Personal Edition G1-Pro", 124522, 124523, 121); buffItemsWindow.AppendString(" - QL 121 to 140"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("V-four Breathing Space", 129079, 129080, 150); buffItemsWindow.AppendString(" - QL 150 to 179 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 18 ); buffItemsWindow.AppendItem("Krutt Assault 219 Waltzing Queen", 124282, 124283, 159); buffItemsWindow.AppendString (" - QL 159 to 183 - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Krutt Assault 219 Fandango", 124280, 124281, 84); buffItemsWindow.AppendString (" - QL 84 to 157  - Rollable"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 16 ); buffItemsWindow.AppendItem("Sentinels Personal Edition G1-C-42", 124520, 124521, 101); buffItemsWindow.AppendString(" - QL 101 to 120"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("MTI SL70 013309a", 124174, 124175, 121); buffItemsWindow.AppendString(" - QL 121 to 140"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("O.E.T. Co. Pelastio V2", 123861, 123862, 90); buffItemsWindow.AppendString(" - QL 90 to 99 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("OT MPS-07 Police Shotgun", 124275, 124275, 190); buffItemsWindow.AppendString(" - QL 190 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Manex STR 77 Shagma Edition", 124498, 124499, 72); buffItemsWindow.AppendString(" - QL 72 to 98"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Sentinels Personal Edition G1-C27", 124518, 124519, 81); buffItemsWindow.AppendString(" - QL 81 to 100"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Explosif's Polychromatic Pillows", 258345, 258345, 300); buffItemsWindow.AppendString(" - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("O.E.T. Co. Pelastio", 123859, 123860, 70); buffItemsWindow.AppendString(" - QL 70 to 89 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("V-three Breathing Space", 129077, 129078, 120); buffItemsWindow.AppendString(" - QL 120 to 149 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Second-Hand Old English Trading Co.", 123855, 123856, 1); buffItemsWindow.AppendString(" - QL 1 to 39 - Dual Wield"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                           Armors"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Burden of Competence", 244718, 244718, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Shades of Lucubration", 152713, 152713, 165); buffItemsWindow.AppendString(" - Nanomage Only"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Mediative Gloves of the Aquarius", 244223, 244223, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Ring of Computing", 238910, 238911, 300); buffItemsWindow.AppendString(" - Wear Two"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Covering", 269997, 269997, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Miy Armor Helmets", 268848, 268849, 300); buffItemsWindow.AppendString(" - 5 Types: ");  buffItemsWindow.AppendBotCommand(" - View", "Items Miy Armor Helmet");buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Fly Catcher's Specs", 216430, 216430, 150); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Masterpiece Ancient Bracer", 267780, 267780, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Ring of Plausibility", 260693, 260693, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Virral Triumvirate Egg", 157955, 157955, 190); buffItemsWindow.AppendString(" - Wear Two"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 14 ); buffItemsWindow.AppendItem("Bracer of Focused Concentration", 251769, 251770, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 11 ); buffItemsWindow.AppendString("Miy's "); buffItemsWindow.AppendItem("Nano", 268840, 268841, 300); buffItemsWindow.AppendString("/");  buffItemsWindow.AppendItem("Scary", 268890, 268891, 300); buffItemsWindow.AppendString(" Armor Sleeves");  buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Dillon Armor", 160407, 160408, 200); buffItemsWindow.AppendString(" - Each piece gives 10 but boots @ QL 200"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("First Creation of the Sagittarius", 244644, 244644, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Breastplate of Azure Reveries", 165304, 165304, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Dark Biotech Rod Ring", 251757, 251758, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Glowing Biotech Rod Ring", 224085, 251261, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("I am the Owl", 163635, 163635, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Pulsing Biotech Rod Ring", 251759, 251760, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Storage Device", 245723, 245723, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Meta-Cerebellum Pocket", 245721, 245721, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Pocket", 245719, 245719, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Two-Dimensional Hole", 245718, 245718, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Human Skin Hood", 246226, 246226, 100); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Sekutek Chilled Plasteel Armor", 160391, 160392, 200); buffItemsWindow.AppendString(" - 5 Intel per piece ");  buffItemsWindow.AppendBotCommand("View Others", "Items 200 Sekutek Chilled Plasteel Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Stalker Helmet", 247803, 247803, 100); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Nova Dillon Armor", 245723, 245723, 200); buffItemsWindow.AppendString(" - 4 Intel per piece ");  buffItemsWindow.AppendBotCommand("View Others", "Items Nova Dillon Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Ring of Presence", 160425, 160426, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 2 ); buffItemsWindow.AppendItem("Anything", 165215, 165215, 200); buffItemsWindow.AppendString(" - Goes ANYWHERE"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                        HUDs/Utilities"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 50 ); buffItemsWindow.AppendItem("Maar's Blue Belt of Double Prudence", 244989, 244989, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 35 ); buffItemsWindow.AppendItem("Hadrulf's Viral Belt Component Platform", 257119, 257119, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Intuitive Memory of the Aquarius", 244358, 244358, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Codex Clavis", 246088, 246088, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Component Platform of Intelligence", 260697, 260697, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6  ); buffItemsWindow.AppendItem("Enhanced Safeguarded NCU Memory Unit", 269985, 269985, 200); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                          Implants"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 55 ); buffItemsWindow.AppendItem("Head Implant - Shiny - QL 200", 101793, 101794, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 33 ); buffItemsWindow.AppendItem("Eye Implant - Bright - QL 200", 101791, 101792, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Ear Implant - Faded - QL 200", 101789, 101790, 200); 

                break;

            case "psychic":

                buffItemsWindow.AppendHighlight("                           Weapons"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Galahad Inc T70 Beardsley", 123956, 123957, 111); buffItemsWindow.AppendString(" - QL 111 to 152 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("O.E.T. Co. Maharanee", 123867, 123867, 200); buffItemsWindow.AppendString(" - QL 200 ONLY - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Vektor ND DRAGON Shotgun", 124262, 124262, 180); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("O.E.T. Co. Pelastio V3", 123865, 123866, 160); buffItemsWindow.AppendString(" - QL 160 to 199 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("ICC Arms 2Q2N-8 Gun Bag", 124142, 124142, 145); buffItemsWindow.AppendString(" -  QL 145 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Kalo-BBI Model 59", 124116, 124117, 92); buffItemsWindow.AppendString(" - QL 92 to 147"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("O.E.T. Co. Jess", 123863, 123864, 100); buffItemsWindow.AppendString(" - QL 100 to 159 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Shiny Glaive of the Proselyte", 211205, 211205, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Atlas Bern C.V. Sarcastic", 123878, 123878, 100); buffItemsWindow.AppendString(" - QL 100 ONLY - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Atlas Bern C.V. Vanilla Dream", 123876, 123877, 82); buffItemsWindow.AppendString(" - QL 82 to 99"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Vektor ND LIZARD Shotgun", 124260, 124260, 70); buffItemsWindow.AppendString(" - QL 70 to 179"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Kalo-BBI Model 58", 124114, 124115, 50); buffItemsWindow.AppendString(" - QL 50 to 91"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("OT-WC RM72 Junior Bido", 124208, 124209, 143); buffItemsWindow.AppendString(" - QL 143 to 165"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Atlas Bern C.V. Vanilla", 123874, 123875, 70); buffItemsWindow.AppendString(" - QL 70 to 81 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Explosif's Polychromatic Pillows", 258345, 258345, 300); buffItemsWindow.AppendString(" - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("ICC Arms 2Q2C (u) Gun Bag", 124140, 124141, 80); buffItemsWindow.AppendString(" - QL 80 to 144"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Kalo-BBI Model 57", 124112, 124113, 38); buffItemsWindow.AppendString(" -  QL 38 to 49"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Chesterfield Dining Chair", 122828, 122828, 41); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                           Armors"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Burden of Competence", 244718, 244718, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Shades of Lucubration", 152713, 152713, 165); buffItemsWindow.AppendString(" - Nanomage only"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Mediative Gloves of the Aquarius", 244223, 244223, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Ring of Computing", 238910, 238911, 300); buffItemsWindow.AppendString(" - Wear Two"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Covering", 269997, 269997, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Fly Catcher's Specs", 216430, 216430, 150); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Masterpiece Ancient Bracer", 267780, 267780, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Ring of Plausibility", 260693, 260693, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Miy's Nano Armor Helmet", 268833, 268834, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Virral Triumvirate Egg", 157955, 157955, 190); buffItemsWindow.AppendString(" - Wear Two"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 14 ); buffItemsWindow.AppendItem("Miy's Armor Sleeves", 268854, 268855, 300); buffItemsWindow.AppendString(" - Wear Two - Diff Types: ");  buffItemsWindow.AppendBotCommand("View", "Items Miy Armor Sleeves");buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Arms of the Servants of Eight", 160427, 160428, 200); buffItemsWindow.AppendString(" - Wear Two"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("First Creation of the Sagittarius", 244644, 244644, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Shapeshifter's Vest", 246222, 246222, 100); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Urbane Pants of Libra", 244581, 244581, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Helmet of Azure Reveries", 165307, 165307, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("I am the Owl", 163635, 163635, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 7 ); buffItemsWindow.AppendItem("Upper Part of Ljotur", 165364, 165364, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Storage Device", 245723, 245723, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Meta-Cerebellum Pocket", 245721, 245721, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Pocket", 245719, 245719, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Two-Dimensional Hole", 245718, 245718, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Nova Dillon Armor", 245723, 245723, 200); buffItemsWindow.AppendString(" - 4 Psy per piece ");  buffItemsWindow.AppendBotCommand("View Others", "Items Nova Dillon Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Ring of Presence", 160425, 160426, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 2 ); buffItemsWindow.AppendItem("Anything", 165215, 165215, 200); buffItemsWindow.AppendString(" - Goes ANYWHERE"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                        HUDs/Utilities"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 50 ); buffItemsWindow.AppendItem("Maar's Blue Belt of Double Prudence", 244989, 244989, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 35 ); buffItemsWindow.AppendItem("Hadrulf's Viral Belt Component Platform", 257119, 257119, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Cancer's Time-Saving Memory", 244558, 244558, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Capricorn's Guide to Alchemy", 244563, 244563, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Component Platform of Psychic", 260699, 260699, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Enhanced Safeguarded NCU Memory Unit", 269985, 269985, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Alien Augmentation Device - Insight", 268472, 268472, 150); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Eremite Psychic Sensor", 246280, 246280, 100); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                          Implants"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 55 ); buffItemsWindow.AppendItem("Head Implant - Shiny - QL 200", 101805, 101806, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 33 ); buffItemsWindow.AppendItem("Chest Implant - Bright - QL 200", 101803, 101804, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Ear Implant - Faded - QL 200", 101801, 101802, 200);

                break;

            case "agility":

                buffItemsWindow.AppendHighlight("                           Weapons"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 60 ); buffItemsWindow.AppendItem("Master G-Staff", 130050, 130050, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 40 ); buffItemsWindow.AppendItem("Senior G-Staff", 130048, 130049, 150); buffItemsWindow.AppendString(" - QL 150 to 199"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("MTI VP300 Gold", 123807, 123807, 200); buffItemsWindow.AppendString(" - QL 200 ONLY - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("MTI VP300 Red s. Caseless", 123805, 123806, 180); buffItemsWindow.AppendString(" - QL 180 to 199 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("O.E.T. Co. Urban Sniper Kaiser", 124312, 124312, 192); buffItemsWindow.AppendString(" - QL 192 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Tsakachumi PTO-HV6 Counter-Sniper Rifle", 124347, 124348, 175); buffItemsWindow.AppendString(" - QL 175 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Blunt G-Staff", 153089, 153090, 150); buffItemsWindow.AppendString(" - QL 150 to 199"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("G.C. AKMR 1K20 Prospero", 124483, 124483, 200); buffItemsWindow.AppendString(" - QL 200 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("G-Staff", 130046, 130047, 100); buffItemsWindow.AppendString(" - QL 100 to 149"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("MTI-OT PF56 Flechette System", 123988, 123989, 93); buffItemsWindow.AppendString(" - QL 93 to 159"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("MTI VP300 Silver s. Caseless", 123803, 123804, 41); buffItemsWindow.AppendString(" - QL 160 to 179"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Tsakachumi PTO-HV.2 Counter-Sniper Rifle", 124343, 124344, 40); buffItemsWindow.AppendString(" - QL 40 to 79"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 18 ); buffItemsWindow.AppendItem("OT Backup S.C Ultra", 123901, 123902, 122); buffItemsWindow.AppendString(" - QL 122 to 179"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 16 ); buffItemsWindow.AppendItem("Loose G-Staff", 153087, 153088, 100); buffItemsWindow.AppendString(" - QL 100 to 149"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Gripo-Com AKR 1K21", 123978, 123978, 50); buffItemsWindow.AppendString(" - QL 50 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("OT Backup S.C", 123899, 123900, 84); buffItemsWindow.AppendString(" - QL 84 to 121"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("OT M500 SWAT Monster", 124356, 124356, 180); buffItemsWindow.AppendString(" - QL 180 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Explosif's Polychromatic Pillows", 258343, 258343, 300); buffItemsWindow.AppendString(" - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Intelligent Mole Stinger", 163285, 163285, 200); buffItemsWindow.AppendString(" - QL 200 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("MTI VP300 Black s. Caseless", 123801, 123802, 140); buffItemsWindow.AppendString(" - QL 140 to 159"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                           Armors"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Burden of Competence", 244718, 244718, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Eye of the Evening Star", 238912, 238913, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Covering", 269997, 269997, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Miy's Armor Legs", 268856, 268857, 300);  buffItemsWindow.AppendString(" - Other types available: ");  buffItemsWindow.AppendBotCommand("View", "items miy armor legs"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Sleeves", 269996, 269996, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Gemini's Double Band of Linked Information", 244572, 244572, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Lya's Sangi Patch", 269194, 269195, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Masterpiece Ancient Bracer", 267780, 267780, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Ring of Divine Teardrops", 238914, 238915, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Ring of Plausibility", 260693, 260693, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 17 ); buffItemsWindow.AppendString("Miy's "); buffItemsWindow.AppendItem("Melee", 268852, 268853, 300);  buffItemsWindow.AppendString("/"); buffItemsWindow.AppendItem("Ranged ", 268864, 268865, 300); buffItemsWindow.AppendString("Armor Gloves"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("The sacrificed wedding ring of Lady Ettard", 245905, 245905, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Legs of the Servants of Eight", 160435, 160436, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Boots of Azure Reveries", 165305, 165305, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("I am the Eel", 163633, 163633, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Kirch Kevlar Armor", 160336, 160337, 200); buffItemsWindow.AppendString(" - 5 Agil per piece ");  buffItemsWindow.AppendBotCommand("View Others", "Items Kirch Kevlar Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Sekutek Chilled Plasteel Armor", 160391, 160392, 200); buffItemsWindow.AppendString(" - 5 Agil per piece ");  buffItemsWindow.AppendBotCommand("View Others", "Items 200 Sekutek Chilled Plasteel Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 2 ); buffItemsWindow.AppendItem("Anything", 165215, 165215, 200); buffItemsWindow.AppendString(" - Goes ANYWHERE"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                        HUDs/Utilities"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 50 ); buffItemsWindow.AppendItem("Maar's Yellow Belt of Double Speed", 244990, 244990, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 35 ); buffItemsWindow.AppendItem("Hadrulf's Viral Belt Component Platform", 257119, 257119, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Mystery of Pisces", 244640, 244640, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Component Platform of Agility", 260698, 260698, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Enhanced Safeguarded NCU Memory Unit", 269987, 269987, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Alien Reflex Modifier", 268498, 268498, 150); buffItemsWindow.AppendLineBreak(1);

                buffItemsWindow.AppendHighlight("                          Implants"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 55 ); buffItemsWindow.AppendItem("Leg Implant - Shiny - QL 200", 101781, 101782, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 33 ); buffItemsWindow.AppendItem("Feet Implant - Bright - QL 200", 101779, 101780, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Waist Implant - Faded - QL 200", 101777, 101778, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 7 ); buffItemsWindow.AppendItem("The Primeval Skull", 206246, 206246, 1); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Skull of the Ancient", 204647, 204647, 1);

                break;

            case "sense":

                buffItemsWindow.AppendHighlight("                           Weapons"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Freedom Arms Super 90 Queen", 123854, 123854, 160); buffItemsWindow.AppendString(" - QL 160 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("ICC Arms 2Q2N-8 Gun Bag", 124142, 124142, 145); buffItemsWindow.AppendString(" - QL 145 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("Polizziotto M204 Hierophant", 243848, 243848, 200); buffItemsWindow.AppendString(" - QL 200 ONLY - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 25 ); buffItemsWindow.AppendItem("YES Support 1011", 124547, 124547, 100); buffItemsWindow.AppendString(" - QL 100 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("BBI AS-90 Gold Star", 124382, 124382, 185); buffItemsWindow.AppendString(" - QL 185 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("BBI PS500 Marine", 124089, 124089, 175); buffItemsWindow.AppendString(" - QL 175 to 199"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Galahad Inc T70 Zig Zag", 123952, 123952, 77); buffItemsWindow.AppendString(" - QL 77 to 98 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("ICC Arms 2Q2C (u) Gun Bag", 124140, 124141, 80); buffItemsWindow.AppendString(" - QL 80 to 144"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Polizziotto M203 Druid", 123773, 123774, 121); buffItemsWindow.AppendString(" - QL 121 to 199 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 16 ); buffItemsWindow.AppendItem("BBI Minami-90 Muddy Intestines", 124443, 124443, 186); buffItemsWindow.AppendString(" - QL 186 ONLY"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 16 ); buffItemsWindow.AppendItem("Fantaghiro BBI-Viral Silver Liner", 248586, 248587, 130); buffItemsWindow.AppendString(" - QL 130 to 184"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Polizziotto M202F Hailstorm Senior", 123771, 123772, 101); buffItemsWindow.AppendString(" - QL 101 to 120 - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Explosif's Polychromatic Pillows", 258343, 258343, 300); buffItemsWindow.AppendString(" - Dual Wield"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("OT-Windchaser M06 Emerald", 124156, 124157, 38); buffItemsWindow.AppendString(" - QL 38 to 40"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Uncle Bazzit Custom 22mm", 123963, 123964, 36); buffItemsWindow.AppendString(" - QL 36 to 95 - Dual Wield"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                           Armors"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Burden of Competence", 244718, 244718, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Ring of Divine Teardrops", 238914, 238915, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Covering", 269997, 269997, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Enhanced Dustbrigade Sleeves", 269996, 269996, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Fly Catcher's Specs", 216430, 216430, 150); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Gemini's Double Band of Linked Information", 244572, 244572, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Lya's Sangi Patch", 269194, 269195, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Masterpiece Ancient Bracer", 267780, 267780, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Moon Watcher Helmet", 245859, 245859, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Ring of Plausibility", 260693, 260693, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Eye of the Evening Star", 238912, 238913, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("Miy's Armor Gloves", 268852, 268853, 300); buffItemsWindow.AppendBotCommand(" - View Others", "Items Miy Armor Gloves"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendString( "Miy's "); buffItemsWindow.AppendItem("Melee", 268856, 268857, 300);  buffItemsWindow.AppendString("/"); buffItemsWindow.AppendItem("Scary ", 268892, 268893, 300); buffItemsWindow.AppendString("Armor Gloves"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 15 ); buffItemsWindow.AppendItem("The sacrificed wedding ring of Lady Ettard", 245905, 245905, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 14 ); buffItemsWindow.AppendItem("Bracer of Killing Intent", 251764, 251764, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Head of the Servants of Eight", 160437, 160438, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("I am the Eel", 163633, 163633, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Pants of Azure Reveries", 165308, 165308, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Storage Device", 245723, 245723, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Meta-Cerebellum Pocket", 245721, 245721, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Subspace Pocket", 245719, 245719, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Human Skin Hood", 246226, 246226, 100); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 5 ); buffItemsWindow.AppendItem("Kirch Kevlar Armor", 160336, 160337, 200); buffItemsWindow.AppendString(" - 5 Sense per piece ");  buffItemsWindow.AppendBotCommand("View Others", "Items Kirch Kevlar Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Nova Dillon Armor", 245723, 245723, 200); buffItemsWindow.AppendString(" - 4 Psy per piece ");  buffItemsWindow.AppendBotCommand("View Others", "Items Nova Dillon Armor"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 2 ); buffItemsWindow.AppendItem("Anything", 165215, 165215, 200); buffItemsWindow.AppendString(" - Goes ANYWHERE"); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                        HUDs/Utilities"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 50 ); buffItemsWindow.AppendItem("Maar's Yellow Belt of Double Speed", 244990, 244990, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 35 ); buffItemsWindow.AppendItem("Hadrulf's Viral Belt Component Platform", 257119, 257119, 300); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 30 ); buffItemsWindow.AppendItem("Star of Enticement", 244703, 244703, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 20 ); buffItemsWindow.AppendItem("Aim of Libra", 244637, 244637, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 12 ); buffItemsWindow.AppendItem("Mystery of Pisces", 244640, 244640, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Codex Divello", 269827, 269827, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 10 ); buffItemsWindow.AppendItem("Component Platform of Sense", 260696, 260696, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 8 ); buffItemsWindow.AppendItem("Rat Catcher Goggles", 152714, 152714, 100); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 6 ); buffItemsWindow.AppendItem("Enhanced Safeguarded NCU Memory Unit", 269987, 269987, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 4 ); buffItemsWindow.AppendItem("Eremite Psychic Sensor", 246280, 246280, 100); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 3 ); buffItemsWindow.AppendItem("Enel Gil's Earring of Attention", 223721, 223721, 50); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 3 ); buffItemsWindow.AppendItem("Spiritech Medical Analyzer", 244993, 244993, 250); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 3 ); buffItemsWindow.AppendItem("Spiritech Network Analyzer", 244992, 244992, 250); buffItemsWindow.AppendLineBreak(2);

                buffItemsWindow.AppendHighlight("                          Implants"); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 55 ); buffItemsWindow.AppendItem("Chest Implant - Shiny - QL 200", 101799, 101800, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 33 ); buffItemsWindow.AppendItem("Waist Implant - Bright - QL 200", 101797, 101798, 200); buffItemsWindow.AppendLineBreak();
                AppendValue( ref buffItemsWindow, 22 ); buffItemsWindow.AppendItem("Head Implant - Faded - QL 200", 101795, 101796, 200); 

                break;

            }

            buffItemsWindow.AppendLineBreak(2); 
            bot.SendReply(e, skill + " Buffing Items »» ", buffItemsWindow);

        }

    }
}
