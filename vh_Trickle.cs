using System;

namespace VhaBot.Plugins
{
    public class VhTrickle : PluginBase
    {
        public VhTrickle()
        {
            this.Name = "Trickle Amount Calculator";
            this.InternalName = "VhTrickle";
            this.Version = 100;
            this.Author = "Neksus / Naturalistic";
            this.Description = "Shows how much skill you will get from ability trickle-downs.";
            this.DefaultState = PluginState.Installed;
            this.Commands = new Command[] {
                new Command("trickle str", true, UserLevel.Guest),
                new Command("trickle sta", true, UserLevel.Guest),
                new Command("trickle agi", true, UserLevel.Guest),
                new Command("trickle sen", true, UserLevel.Guest),
                new Command("trickle int", true, UserLevel.Guest),
                new Command("trickle psy", true, UserLevel.Guest),
                new Command("trickle", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        { }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            lock (this)
            {
                if (e.Args.Length < 1)
                {
                    bot.SendReply(e, "Correct Usage: trickle str/sta/agi/sen/int/psy [amount]");
                    return;
                }
                switch (e.Command.ToLower())
                {
                    case "trickle str":
                        this.TrickleStr(bot, e);
                        break;
                    case "trickle sta":
                        this.TrickleSta(bot, e);
                        break;
                    case "trickle agi":
                        this.TrickleAgi(bot, e);
                        break;
                    case "trickle sen":
                        this.TrickleSen(bot, e);
                        break;
                    case "trickle int":
                        this.TrickleInt(bot, e);
                        break;
                    case "trickle psy":
                        this.TricklePsy(bot, e);
                        break;
                    case "trickle":
                        this.TrickleBase(bot, e);
                        break;
                }
            }
        }

        public override void OnUnload(BotShell bot) { }

        private void AppendValue( ref RichTextWindow window,
                                  int value )
        {
            int digits = (int)Math.Floor(Math.Log10(Math.Abs(value)))+1;
            if ( value == 0 )
                digits = 1;
            for ( int i=4-digits; i>0; i-- )
            {
                window.AppendColorStart("#000000");
                window.AppendString("0");
                window.AppendColorEnd();
            }
            if ( value > 0 )
                window.AppendColorStart("#00FF00");
            else if ( value < 0 )
                window.AppendColorStart("#FF0000");
            window.AppendString( value.ToString() );
            window.AppendColorEnd();

            window.AppendColorStart("#000000");
            window.AppendString("00");
            window.AppendColorEnd();
        }

        private void TrickleStr(BotShell bot, CommandArgs e)
        {
            Double Test;
            if( ! double.TryParse( e.Args[0], out Test ) )
            {
                bot.SendReply( e, "Error: Unable to parse \"" + e.Args[0] +
                               "\" as a number." );
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendNormalStart();
            window.AppendTitle();
            window.AppendHighlight("             Body");  window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20)) ); window.AppendString("Adventuring"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60)) ); window.AppendString("Brawling"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20)) ); window.AppendString("Martial Arts"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20)) ); window.AppendString("Swimming"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("1 Handed Blunt"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("1 Handed Edged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("2 Handed Blunt"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("2 Handed Edged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Multiple Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Parry"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Piercing"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Misc"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Heavy Weapons"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Sharp Object"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Assault Rifle"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Bow Special Attack"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Bow"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Burst"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Full Auto"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Shotgun"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("SMG/MG"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Speed"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Runspeed"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Trade & Repair"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Weapon Smithing"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Nano and Aiding"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Sensory Improvement"); window.AppendLineBreak();

            bot.SendReply(e, bot.ColorHeader + "Trickling " + bot.ColorHighlight + e.Args[0] + bot.ColorHeader + " To Strength " + bot.ColorHighlight + " »» ", window);
        }

        private void TrickleSta(BotShell bot, CommandArgs e)
        {
            Double Test;
            if( ! double.TryParse( e.Args[0], out Test ) )
            {
                bot.SendReply( e, "Error: Unable to parse \"" + e.Args[0] +
                               "\" as a number." );
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendNormalStart();
            window.AppendTitle();
            window.AppendHighlight("             Body"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Adventuring"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * 1))); window.AppendString("Body Dev"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Brawling"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Nano Pool"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Swimming"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("1 Handed Blunt"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("1 Handed Edged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("2 Handed Blunt"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("2 Handed Edged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Melee Energy"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Multiple Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Piercing"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Assault Rifle"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Burst"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Full Auto"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("SMG/MG"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Speed"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Runspeed"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Trade & Repair"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Chemistry"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Electrical Engineering"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Nano and Aiding"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Matter Creations"); window.AppendLineBreak();

            bot.SendReply(e, bot.ColorHeader + "Trickling " + bot.ColorHighlight + e.Args[0] + bot.ColorHeader + " To Stamina " + bot.ColorHighlight + " »» ", window);
        }

        private void TrickleAgi(BotShell bot, CommandArgs e)
        {
            Double Test;
            if( ! double.TryParse( e.Args[0], out Test ) )
            {
                bot.SendReply( e, "Error: Unable to parse \"" + e.Args[0] +
                               "\" as a number." );
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendNormalStart();
            window.AppendTitle();
            window.AppendHighlight("             Body"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Adventuring"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Martial Arts"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Riposte"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Swimming"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("1 Handed Blunt"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("1 Handed Edged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Fast Attack"); window.AppendLineBreak(); 
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Multiple Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Parry"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Piercing"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Misc"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Grenade"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Heavy Weapons"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Sharp Object"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Assault Rifle"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Bow Special Attack"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Bow"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Burst"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * 1))); window.AppendString("Fling Shot"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Multiple Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Pistol"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Rifle"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Shotgun"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("SMG/MG"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Speed"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Dodge Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Duck Explosions"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Evade Close"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Melee Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Nano Cast Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Physical Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Ranged Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Runspeed"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Trade & Repair"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Electrical Engineering"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Mechanical Engineering"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Nano and Aiding"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("First Aid"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Time and Space"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Treatment"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("           Spying"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Breaking and Entering"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Concealment"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Trap Disarm"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("          Navigation"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Vehicle Air"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Vehicle Ground"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Vehicle Water"); window.AppendLineBreak();

            bot.SendReply(e, bot.ColorHeader + "Trickling " + bot.ColorHighlight + e.Args[0] + bot.ColorHeader + " To Agility " + bot.ColorHighlight + " »» ", window);
        }

        private void TrickleSen(BotShell bot, CommandArgs e)
        {
            Double Test;
            if( ! double.TryParse( e.Args[0], out Test ) )
            {
                bot.SendReply( e, "Error: Unable to parse \"" + e.Args[0] +
                               "\" as a number." );
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendNormalStart();
            window.AppendTitle();
            window.AppendHighlight("             Body"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Dimach"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Nano Pool"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Riposte"); window.AppendLineBreak(); window.AppendLineBreak(); 
            window.AppendHighlight("             Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Fast Attack"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Parry"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Sneak Attack"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Misc"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Grenade"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Sharp Object"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * 1))); window.AppendString("Aimed Shot"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Assault Rifle"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Bow Special Attack"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Bow"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Pistol"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Ranged Energy"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Rifle"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("SMG/MG"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Speed"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Dodge Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Duck Explosions"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Evade Close"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Melee Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Nano Cast Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Physical Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Ranged Initiative"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Trade & Repair"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Tutoring"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Nano and Aiding"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("First Aid"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Psychological Modifications"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Treatment"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("           Spying"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Breaking and Entering"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .70))); window.AppendString("Concealment"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .70))); window.AppendString("Perception"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Trap Disarm"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("          Navigation"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Map Navigations"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Vehicle Air"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Vehicle Ground"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .60))); window.AppendString("Vehicle Water"); window.AppendLineBreak();

            bot.SendReply(e, bot.ColorHeader + "Trickling " + bot.ColorHighlight + e.Args[0] + bot.ColorHeader + " To Sense " + bot.ColorHighlight + " »» ", window);
        }

        private void TrickleInt(BotShell bot, CommandArgs e)
        {
            Double Test;
            if( ! double.TryParse( e.Args[0], out Test ) )
            {
                bot.SendReply( e, "Error: Unable to parse \"" + e.Args[0] +
                               "\" as a number." );
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendNormalStart();
            window.AppendTitle();
            window.AppendHighlight("             Body"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Nano Pool"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Melee Energy"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Misc"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Grenade"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Multiple Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Ranged Energy"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Speed"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Dodge Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Duck Explosions"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Evade Close"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Melee Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Nano Resist"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Physical Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Ranged Initiative"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Trade & Repair"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Chemistry"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * 1))); window.AppendString("Computer Literacy"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Electrical Engineering"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Mechanical Engineering"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * 1))); window.AppendString("Nano Programming"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Pharmacological Technologies"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Psychology"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Quantum FT"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .70))); window.AppendString("Tutoring"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Weapon Smithing"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Nano and Aiding"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Biological Metamorphasis"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("First Aid"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Matter Creations"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Matter Metamophasis"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Psychological Modifications"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Sensory Improvement"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Time and Space"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Treatment"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("           Spying"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Perception"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Trap Disarm"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("          Navigation"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Map Navigation"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Vehicle Air"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Vehicle Ground"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Vehicle Water"); window.AppendLineBreak();

            bot.SendReply(e, bot.ColorHeader + "Trickling " + bot.ColorHighlight + e.Args[0] + bot.ColorHeader + " To Intelligence " + bot.ColorHighlight + " »» ", window);
        }

        private void TricklePsy(BotShell bot, CommandArgs e)
        {
            Double Test;
            if( ! double.TryParse( e.Args[0], out Test ) )
            {
                bot.SendReply( e, "Error: Unable to parse \"" + e.Args[0] +
                               "\" as a number." );
                return;
            }
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendNormalStart();
            window.AppendTitle();
            window.AppendHighlight("             Body"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Dimach"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Martial Arts"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("             Melee"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Sneak Attack"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Ranged"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .40))); window.AppendString("Ranged Energy"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("            Speed"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Melee Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .80))); window.AppendString("Nano Resist"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Physical Initiative"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Ranged Initiative"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Trade & Repair"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Psychology"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .50))); window.AppendString("Quantum FT"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Tutoring"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("        Nano and Aiding"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Biological Metamorphasis"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .20))); window.AppendString("Matter Metamorphasis"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("           Spying"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .30))); window.AppendString("Break And Enter"); window.AppendLineBreak(); window.AppendLineBreak();
            window.AppendHighlight("          Navigation"); window.AppendLineBreak();
            AppendValue( ref window, (int)Math.Floor(((Test / 4) * .10))); window.AppendString("Map Navigation"); window.AppendLineBreak();

            bot.SendReply(e, bot.ColorHeader + "Trickling " + bot.ColorHighlight + e.Args[0] + bot.ColorHeader + " To Psychic " + bot.ColorHighlight + " »» ", window);
        }

        private void TrickleBase(BotShell bot, CommandArgs e)
        {
            bot.SendReply(e, "Usage: trickle str/sta/agi/sen/int/psy [amount]");
            return;
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {
                case "trickle":
                    return "Displays trickle information for the attribute specified.\n" +
                        "Usage: /tell " + bot.Character + " trickle str/sta/agi/sen/int/psy [amount]";
            }
            return null;
        }
    }
}
