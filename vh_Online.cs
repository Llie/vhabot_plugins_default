using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class OnlineBase : PluginBase
    {

        private bool IncludeNotifyList = true;
        private bool IncludePrivateChannel = true;
        private bool SeparateSections = false;
        private string DisplayOrganization = "Never";
        private string DisplayRanks = "None";
        private string DisplayMode = "Profession";
        private string DisplayHeaders = "Text";
        private bool DisplayAfk = true;
        private bool DisplayAlts = true;
        private bool SendLogon = false;
        private bool SendToOther = false;
        private bool AutoAFK = false;
        private Dictionary<string, string> Afk;
        private Dictionary<string, int> Icons; 
        private Dictionary<string, string> Banners;
        private BotShell _bot;
        private Timer _welcome;

        public OnlineBase()
        {
            this.Name = "Online Tracker";
            this.InternalName = "vhOnline";
            this.Author = "Vhab / Kilmanagh / Veremit";
            this.Version = 150;
            this.Description = "Provides an interface to the user to view who is online and/or on the private channel. Profession Icon modification ported from Akarah's BeBot module.";
            this.DefaultState = PluginState.Installed;
            this.Commands = new Command[] {
                new Command("online", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("afk", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("brb", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("kiting", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("back", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {

            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "include_notify", "Include Notify List", this.IncludeNotifyList);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "include_pg", "Include Private Channel", this.IncludePrivateChannel);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "seperate_sections", "Separate Sections", this.SeparateSections);
            bot.Configuration.Register(ConfigType.String, this.InternalName, "display_org", "Display Organization", this.DisplayOrganization, "Never", "Foreign", "Always");
            bot.Configuration.Register(ConfigType.String, this.InternalName, "display_ranks", "Display Ranks", this.DisplayRanks, "None", "Bot", "Org", "Both");
            bot.Configuration.Register(ConfigType.String, this.InternalName, "display_mode", "Display Mode", this.DisplayMode, "Profession", "Alphabetical", "Level");
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "display_afk", "Display AFK", this.DisplayAfk);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "display_alts", "Display Alts", this.DisplayAlts);
            bot.Configuration.Register(ConfigType.String, this.InternalName, "display_headers", "Display Sub-Headers", this.DisplayHeaders, "Off", "Text", "Icons", "Banners");
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "send_other", "Send AFK message to both guild/private chat", this.SendToOther);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "send_logon", "Send Online on Logon", this.SendLogon);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "auto_afk", "Automatically set user afk on channel message", this.AutoAFK);

            this.IncludeNotifyList = bot.Configuration.GetBoolean(this.InternalName, "include_notify", this.IncludeNotifyList);
            this.IncludePrivateChannel = bot.Configuration.GetBoolean(this.InternalName, "include_pg", this.IncludePrivateChannel);
            this.SeparateSections = bot.Configuration.GetBoolean(this.InternalName, "seperate_sections", this.SeparateSections);
            this.DisplayOrganization = bot.Configuration.GetString(this.InternalName, "display_org", this.DisplayOrganization);
            this.DisplayRanks = bot.Configuration.GetString(this.InternalName, "display_ranks", this.DisplayRanks);
            this.DisplayMode = bot.Configuration.GetString(this.InternalName, "display_mode", this.DisplayMode);
            this.DisplayAfk = bot.Configuration.GetBoolean(this.InternalName, "display_afk", this.DisplayAfk);
            this.DisplayAlts = bot.Configuration.GetBoolean(this.InternalName, "display_alts", this.DisplayAlts);
            this.DisplayHeaders = bot.Configuration.GetString(this.InternalName, "display_headers", this.DisplayHeaders);
            this.SendToOther = bot.Configuration.GetBoolean(this.InternalName, "send_other", this.SendToOther);
            this.SendLogon = bot.Configuration.GetBoolean(this.InternalName, "send_logon", this.SendLogon);
            this.AutoAFK = bot.Configuration.GetBoolean(this.InternalName, "auto_afk", this.AutoAFK);

            this.Afk = new Dictionary<string, string>();
            this.LoadIcons();
            this.LoadBanners();

            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(Events_ConfigurationChangedEvent);
            
            bot.Events.ChannelMessageEvent += new ChannelMessageHandler(Events_ChannelMessageEvent);
            bot.Events.PrivateChannelMessageEvent += new PrivateChannelMessageHandler(Events_PrivateChannelMessageEvent);
            bot.Events.UserLogonEvent += new UserLogonHandler(Events_UserLogonEvent);
            bot.Events.UserLogoffEvent += new UserLogoffHandler(Events_UserLogoffEvent);
            bot.Events.BotStateChangedEvent += new BotStateChangedHandler(Events_BotStateChangedEvent);

            this._bot = bot;

            this._welcome = new Timer(7000);
            this._welcome.Elapsed += new ElapsedEventHandler(WelcomeTimerElapsed);
            this._welcome.AutoReset = false;
            this._welcome.Enabled = true;
            
        }

        public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
            {
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= OnlineWindow;
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += OnlineWindow;
                this._bot.Events.UserLogonEvent -= new UserLogonHandler(Events_UserLogonEvent);
            }
        }

        public void WelcomeUnload( BotShell bot )
        {
            this._bot.Events.UserLogonEvent += new UserLogonHandler(Events_UserLogonEvent);
        }

        public override void OnUnload(BotShell bot)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= OnlineWindow;
            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(Events_ConfigurationChangedEvent);

            bot.Events.ChannelMessageEvent -= new ChannelMessageHandler(Events_ChannelMessageEvent);
            bot.Events.PrivateChannelMessageEvent -= new PrivateChannelMessageHandler(Events_PrivateChannelMessageEvent);
            bot.Events.UserLogonEvent -= new UserLogonHandler(Events_UserLogonEvent);
            bot.Events.UserLogoffEvent -= new UserLogoffHandler(Events_UserLogoffEvent);
            bot.Events.BotStateChangedEvent -= new BotStateChangedHandler(Events_BotStateChangedEvent);
        }

        private void HandleAutoAFK( BotShell bot, string sender, string message,
                                    bool source_is_privchat )
        {
            if (this.AutoAFK)
            {

                if ( message.ToLower().StartsWith("afk"))
                {
                    lock (this.Afk)
                    {
                        if (this.Afk.ContainsKey(sender))
                            this.Afk[sender] = "AFK";
                        else
                            this.Afk.Add(sender, "AFK");
                        if( source_is_privchat || this.SendToOther )
                            bot.SendPrivateChannelMessage(bot.ColorHighlight + sender + " is now afk.");
                        if( ! source_is_privchat || this.SendToOther )
                            bot.SendOrganizationMessage(bot.ColorHighlight + sender + " is now afk.");
                    }
                }
                else if ( message.ToLower().StartsWith("brb") )
                {
                    lock (this.Afk)
                    {
                        if (this.Afk.ContainsKey(sender))
                            this.Afk[sender] = "BRB";
                        else
                            this.Afk.Add(sender, "BRB");
                        if ( source_is_privchat || this.SendToOther )
                            bot.SendPrivateChannelMessage(bot.ColorHighlight + sender + " will be right back.");
                        if ( ! source_is_privchat || this.SendToOther )
                            bot.SendOrganizationMessage(bot.ColorHighlight + sender + " will be right back.");
                    }
                }
                else
                {
                    lock (this.Afk)
                    {
                        string afkStatus = "";
                        if (this.Afk.TryGetValue(sender, out afkStatus))
                        {
                            this.Afk.Remove(sender);
                            if ( source_is_privchat || this.SendToOther )
                                bot.SendPrivateChannelMessage(bot.ColorHighlight + sender + " is back from " + afkStatus + ".");
                            if ( ! source_is_privchat || this.SendToOther )
                                bot.SendOrganizationMessage(bot.ColorHighlight + sender + " is back from " + afkStatus + ".");
                        }
                    }
                }
            }
        }

        private void Events_BotStateChangedEvent(BotShell bot, BotStateChangedArgs e)
        {
            if (e.State==0)
            {
                bot.SendOrganizationMessage(bot.ColorHighlight + bot.Character + " has connected!");
            }
        }

        private void Events_ChannelMessageEvent(BotShell bot, ChannelMessageArgs e)
        {
            if (e.Type != ChannelType.Organization) return;
            if (e.Command) return;
            if (this.AutoAFK)
                HandleAutoAFK( bot, e.Sender, e.Message, false );
        }

        private void Events_PrivateChannelMessageEvent(BotShell bot, PrivateChannelMessageArgs e)
        {
            if (e.Command) return;
            if (this.AutoAFK)
                HandleAutoAFK( bot, e.Sender, e.Message, true );
        }

        private void Events_UserLogoffEvent(BotShell bot, UserLogoffArgs e)
        {
             lock (this.Afk)
             {
                  if (this.Afk.ContainsKey(e.Sender))
                    this.Afk.Remove(e.Sender);
             }
        }

        private void Events_UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (this.SendLogon && e.Sections.Contains("notify"))
            {
                // Fake the user sending !online to the bot ;)
                CommandArgs args = new CommandArgs(CommandType.Tell, 0, e.SenderID, e.Sender, e.SenderWhois, "online", "", false, null);
                this.OnOnlineCommand(bot, args);
            }

        }

        private void Events_ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section.Equals(this.InternalName, StringComparison.CurrentCultureIgnoreCase))
                switch (e.Key.ToLower())
                {
                    case "include_notify":
                        this.IncludeNotifyList = (bool)e.Value;
                        break;
                    case "include_pg":
                        this.IncludePrivateChannel = (bool)e.Value;
                        break;
                    case "seperate_sections":
                        this.SeparateSections = (bool)e.Value;
                        break;
                    case "display_org":
                        this.DisplayOrganization = (string)e.Value;
                        break;
                    case "display_ranks":
                        this.DisplayRanks = (string)e.Value;
                        break;
                    case "display_mode":
                        this.DisplayMode = (string)e.Value;
                        break;
                    case "display_afk":
                        this.DisplayAfk = (bool)e.Value;
                        break;
                    case "display_alts":
                        this.DisplayAlts = (bool)e.Value;
                        break;
                    case "display_headers":
                        this.DisplayHeaders = (string)e.Value;
                        break;
                    case "send_other":
                        this.SendToOther = (bool)e.Value;
                        break;
                    case "send_logon":
                        this.SendLogon = (bool)e.Value;
                        break;
                    case "auto_afk":
                        this.AutoAFK = (bool)e.Value;
                        break;
                }
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {
                case "online":
                    this.OnOnlineCommand(bot, e);
                    break;
                case "afk":
                case "brb":
                case "kiting":
                    this.OnAfkCommand(bot, e);
                    break;
                case "back":
                    this.OnBackCommand(bot, e);
                    break;
            }
        }

        public void OnOnlineCommand(BotShell bot, CommandArgs e)
        {
            List<string> titles = new List<string>();
            List<RichTextWindow> windows = new List<RichTextWindow>();
            string args = string.Empty;
            if (e.Args.Length > 0)
                args = e.Args[0];

            // Notify List
            if (this.IncludeNotifyList)
            {
                string[] online = bot.FriendList.Online("notify");
                if (online.Length > 0)
                {
                    RichTextWindow notifyWindow = new RichTextWindow(bot);
                    notifyWindow.AppendHeader(online.Length + " Users Online");
                    int results = 0;
                    this.BuildList(bot, online, ref notifyWindow, ref results, args);
                    if (results > 0)
                    {
                        titles.Add(HTML.CreateColorString(bot.ColorHeaderHex, results.ToString()) + " Users Online");
                        windows.Add(notifyWindow);
                    }
                }
            }

            // Private Channel
            if (this.IncludePrivateChannel)
            {
                Dictionary<UInt32, Friend> list = bot.PrivateChannel.List();
                List<string> guests = new List<string>();
                foreach (KeyValuePair<UInt32, Friend> user in list)
                    guests.Add(user.Value.User);
                if (guests.Count > 0)
                {
                    RichTextWindow guestsWindow = new RichTextWindow(bot);
                    guestsWindow.AppendHeader(guests.Count + " Users on the Private Channel");
                    int results = 0;
                    this.BuildList(bot, guests.ToArray(), ref guestsWindow, ref results, args);
                    if (results > 0)
                    {
                        titles.Add(HTML.CreateColorString(bot.ColorHeaderHex, results.ToString()) + " Users on the Private Channel");
                        windows.Add(guestsWindow);
                    }
                }
            }

            // Output
            if (titles.Count < 1)
            {
                if (args != String.Empty)
                    bot.SendReply(e, "The are currently no " + args + " 's online");
                else
                    bot.SendReply(e, "The are currently no users online");
                return;
            }
            if (SeparateSections)
            {
                for (int i = 0; i < windows.Count; i++)
                {
                    RichTextWindow window = new RichTextWindow(bot);
                    window.AppendTitle();
                    window.AppendRawString(windows[i].Text);
                    bot.SendReply(e, titles[i] + " »» ", window);
                }
            }
            else
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle();
                foreach (RichTextWindow subWindow in windows)
                    window.AppendRawString(subWindow.Text);
                bot.SendReply(e, string.Join(", ", titles.ToArray()) + " »» ", window);
            }
        }

        public void OnlineWindow( ref RichTextWindow window, string sender )
        {
            List<RichTextWindow> windows = new List<RichTextWindow>();

            if (this.IncludeNotifyList)
            {
                string[] online = this._bot.FriendList.Online("notify");
                if (online.Length > 0)
                {
                    RichTextWindow notifyWindow = new RichTextWindow(this._bot);
                    notifyWindow.AppendHeader(online.Length + " Users Online");
                    int results = 0;
                    this.BuildList(this._bot, online, ref notifyWindow, ref results, string.Empty );
                    if (results > 0)
                        windows.Add(notifyWindow);
                }
            }

            // Private Channel
            if (this.IncludePrivateChannel)
            {
                Dictionary<UInt32, Friend> list = this._bot.PrivateChannel.List();
                List<string> guests = new List<string>();
                foreach (KeyValuePair<UInt32, Friend> user in list)
                    guests.Add(user.Value.User);
                if (guests.Count > 0)
                {
                    RichTextWindow guestsWindow = new RichTextWindow(this._bot);
                    guestsWindow.AppendHeader(guests.Count + " Users on the Private Channel");
                    int results = 0;
                    this.BuildList(this._bot, guests.ToArray(), ref guestsWindow, ref results, string.Empty);
                    if (results > 0)
                        windows.Add(guestsWindow);
                }
            }

            // Output
            if (windows.Count < 1)
            {
                window.AppendNormal( "The are currently no users online");
                return;
            }
            foreach (RichTextWindow subWindow in windows)
                window.AppendRawString(subWindow.Text);

            window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
            window.AppendLineBreak();

        }

     	public void SendMessage(BotShell bot, CommandArgs e, string message )
        {

            if ( ( e.Type == CommandType.PrivateChannel ) || this.SendToOther )
                bot.SendPrivateChannelMessage( bot.ColorHighlight + message );

            if ( ( e.Type == CommandType.Organization ) || this.SendToOther )
                bot.SendOrganizationMessage( bot.ColorHighlight + message );

        }

        public void OnAfkCommand(BotShell bot, CommandArgs e)
        {
            lock (this.Afk)
            {
                if ( this.Afk.ContainsKey(e.Sender) )
                     this.Afk.Remove(e.Sender);
            }
            string afkmsg = "";
            if ( (e.Args.Length < 1) && ( e.Command == "kiting" ) )
            {
                afkmsg = "Kiting";
            }
            else if ( (e.Args.Length < 1) && ( e.Command == "brb" ) )
            {
                afkmsg = "BRB";
            }
            else if ( (e.Args.Length >= 1) && ( e.Command == "brb" ) )
            {
                afkmsg = "BRB (" + e.Words[0] + ")";
            }
            else if (e.Args.Length < 1)
            {
                afkmsg = "AFK";
            }
            else
                afkmsg = "AFK (" + e.Words[0] + ")";
            lock (this.Afk)
            {
                this.Afk.Add(e.Sender, afkmsg);
            }

            this.SendMessage( bot, e, e.Sender + " is now marked " + afkmsg );

        }

        public void OnBackCommand(BotShell bot, CommandArgs e)
        {
            lock (this.Afk)
            {
                if ( this.Afk.ContainsKey(e.Sender) )
                {
                    if ( this.Afk[e.Sender] == "Kiting") 
                    {
                        this.Afk.Remove(e.Sender);
                        this.SendMessage( bot, e, e.Sender +
                                          " is no longer Kiting" );
                        return;
                    }
                    else
                    {
                        string afkStatus = "";
                        this.Afk.TryGetValue(e.Sender, out afkStatus);
                        this.Afk.Remove(e.Sender);
                        this.SendMessage( bot, e, e.Sender +
                                          " is no longer marked " + afkStatus );
                        return;
                    }
                    
                }
            }
        }

        public void BuildList(BotShell bot, string[] users, ref RichTextWindow window) { int results = 0; this.BuildList(bot, users, ref window, ref results, string.Empty); }
        public void BuildList(BotShell bot, string[] users, ref RichTextWindow window, ref int results, string profs)
        {
            if (window == null)
                return;
            if (users.Length == 0)
                return;

            SortedDictionary<string, SortedDictionary<string, WhoisResult>> list = new SortedDictionary<string, SortedDictionary<string, WhoisResult>>();
            Dictionary<string, WhoisResult> whoisResults = new Dictionary<string, WhoisResult>();
            foreach (string user in users)
            {
                if (string.IsNullOrEmpty(user))
                    continue;

                string header;
                WhoisResult whois = XML.GetWhois(user, bot.Dimension);
                if (! string.IsNullOrEmpty(profs))
                {
                    if (whois == null || whois.Stats == null)
                        continue;
                    if (!whois.Stats.Profession.StartsWith(profs, StringComparison.CurrentCultureIgnoreCase))
                        continue;
                }
                if (whois != null && whois.Stats != null)
                {
                    if (this.DisplayMode == "Level")
                        header = whois.Stats.Level.ToString();
                    else
                        header = whois.Stats.Profession;

                    if (!whoisResults.ContainsKey(user.ToLower()))
                        whoisResults.Add(user.ToLower(), whois);
                }
                else
                    if (this.DisplayMode == "Level")
                        header = "0";
                    else
                        header = "Unknown";

                if (this.DisplayMode == "Alphabetical")
                    header = user.ToUpper().ToCharArray()[0].ToString();

                if (!list.ContainsKey(header))
                    list.Add(header, new SortedDictionary<string, WhoisResult>());
                list[header].Add(user, whois);
            }
            results = 0;
            foreach (KeyValuePair<string, SortedDictionary<string, WhoisResult>> prof in list)
            {
                if (this.DisplayHeaders == "Text")
                {
                    window.AppendHighlight(prof.Key);
                    window.AppendLineBreak();
                }
                else if (this.DisplayHeaders == "Icons")
                {
                    window.AppendImage("GFX_GUI_FRIENDLIST_SPLITTER");
                    window.AppendLineBreak();
                    window.AppendIcon(Icons[prof.Key]);
                    window.AppendHighlight(prof.Key);
                    window.AppendLineBreak();
                    window.AppendImage("GFX_GUI_FRIENDLIST_SPLITTER");
                    window.AppendLineBreak();
                }
                else if (this.DisplayHeaders == "Banners")
                {
                    window.AppendImage(Banners[prof.Key]);
                    window.AppendLineBreak();
                }
                foreach (KeyValuePair<string, WhoisResult> user in prof.Value)
                {
                    // Name
                    window.AppendNormalStart();
                    window.AppendString("   ");
                    int level = 0;
                    int ailevel = 0;
                    string name = Format.UppercaseFirst(user.Key);

                    // Level
                    if (user.Value != null && user.Value.Stats != null)
                    {
                        level = user.Value.Stats.Level;
                        ailevel = user.Value.Stats.DefenderLevel;
                    }
                    if (level < 10)
                        window.AppendColorString("000000", "00");
                    else if (level < 100)
                        window.AppendColorString("000000", "0");
                    window.AppendString(level + " ");

                    if (ailevel < 10)
                        window.AppendColorString("000000", "0");
                    window.AppendColorString(RichTextWindow.ColorGreen, ailevel + " ");
                    window.AppendString(name);

                    // Organization
                    bool displayOrganization = false;
                    if (this.DisplayOrganization == "Always")
                        displayOrganization = true;

                    if (this.DisplayOrganization == "Foreign")
                    {
                        displayOrganization = true;
                        if (bot.InOrganization && whoisResults.ContainsKey(name.ToLower()) && whoisResults[name.ToLower()].InOrganization &&
                            bot.Organization == whoisResults[name.ToLower()].Organization.Name)
                            displayOrganization = false;
                    }

                    if (displayOrganization)
                    {
                        if (!whoisResults.ContainsKey(name.ToLower()))
                        {
                            window.AppendString(" (Unknown)");
                        }
                        else if (whoisResults[name.ToLower()].InOrganization)
                        {
                            window.AppendString(" (" + whoisResults[name.ToLower()].Organization.ToString() + ")");
                        }
                        else
                        {
                            window.AppendString(" (Not Guilded)");
                        }
                    }

                    // Bot Level
                    if ((this.DisplayRanks == "Bot") || (this.DisplayRanks == "Both"))
                    {
                        UserLevel userLevel = bot.Users.GetUser(name);
                        if (userLevel < UserLevel.Member)
                        {
                            window.AppendString(" [");
                            window.AppendColorString(RichTextWindow.ColorGreen, bot.Users.GetUser(name).ToString());
                            window.AppendString("]");
                        }
                        else if (userLevel > UserLevel.Member)
                        {
                            window.AppendString(" [");
                            window.AppendColorString(RichTextWindow.ColorRed, bot.Users.GetUser(name).ToString());
                            window.AppendString("]");
                        }
                    }

                    // Org Rank
                    if ((this.DisplayOrganization == "Foreign") && (!displayOrganization))
                    {              // ie. No org displayed, but player is IN org - easiest comparison option
                       if ((this.DisplayRanks == "Org") || (this.DisplayRanks == "Both"))
                              window.AppendString(" (" + whoisResults[name.ToLower()].Organization.Rank + ")");
                    }

                    // Alts
                    if (this.DisplayAlts)
                    {
                        string main = bot.Users.GetMain(name);
                        string[] alts = bot.Users.GetAlts(main);
                        if (alts.Length > 0)
                        {
                            window.AppendString(" :: ");
                            if (main == name)
                                window.AppendBotCommand("Alts", "alts " + main);
                            else
                                window.AppendBotCommand(main + "'s Alts", "alts " + main);
                        }
                    }

                    // Afk
                    if (this.DisplayAfk)
                    {
                        lock (this.Afk)
                        {
                            if ( this.Afk.ContainsKey(name) )
                            {
                                window.AppendString(" :: ");

                                if( this.Afk[name] == "Kiting" )
                                    window.AppendColorString(RichTextWindow.ColorOrange, "Kiting");
                                else if ( this.Afk[name].IndexOf("BRB") >=0 )
                                    window.AppendColorString(RichTextWindow.ColorGreen, "BRB");
                                else
                                    window.AppendColorString(RichTextWindow.ColorRed, "AFK");
                                if ( this.Afk[name].IndexOf("(") >= 0 )
                                    window.AppendString( " " +
                                        this.Afk[name].Substring(
                                            this.Afk[name].IndexOf("(") ) );
                            }
                        }
                    }
                    window.AppendColorEnd();
                    window.AppendLineBreak();
                    results++;
                }
                if (this.DisplayHeaders == "Text" || this.DisplayHeaders == "Icons")
                    window.AppendLineBreak();
            }
            if (this.DisplayHeaders == "Off")
                window.AppendLineBreak();
        }

        public void LoadIcons()
        {
            Icons = new Dictionary<string, int>();
            Icons.Add("Meta-Physicist", 16308);
            Icons.Add("Adventurer", 84203);
            Icons.Add("Engineer", 16252);
            Icons.Add("Soldier", 16237);
            Icons.Add("Keeper", 84197);
            Icons.Add("Shade", 39290);
            Icons.Add("Fixer", 16300);
            Icons.Add("Agent", 16186);
            Icons.Add("Trader", 117993);
            Icons.Add("Doctor", 44235);
            Icons.Add("Enforcer", 100998);
            Icons.Add("Bureaucrat", 16341);
            Icons.Add("Martial Artist", 16196);
            Icons.Add("Nano-Technician", 16283);
            Icons.Add("Unknown", 287099);
        }

        public void LoadBanners()
        {
            Banners = new Dictionary<string, string>();
            Banners.Add("Meta-Physicist","GFX_GUI_CC_NEW_PROF1");
            Banners.Add("Adventurer","GFX_GUI_CC_NEW_PROF2");
            Banners.Add("Engineer","GFX_GUI_CC_NEW_PROF3");
            Banners.Add("Soldier","GFX_GUI_CC_NEW_PROF4");
            Banners.Add("Keeper","GFX_GUI_CC_NEW_PROF5");
            Banners.Add("Shade","GFX_GUI_CC_NEW_PROF6");
            Banners.Add("Fixer","GFX_GUI_CC_NEW_PROF7");
            Banners.Add("Agent","GFX_GUI_CC_NEW_PROF8");
            Banners.Add("Trader","GFX_GUI_CC_NEW_PROF9");
            Banners.Add("Doctor","GFX_GUI_CC_NEW_PROF10");
            Banners.Add("Enforcer","GFX_GUI_CC_NEW_PROF11");
            Banners.Add("Bureaucrat","GFX_GUI_CC_NEW_PROF12");
            Banners.Add("Martial Artist","GFX_GUI_CC_NEW_PROF13");
            Banners.Add("Nano-Technician","GFX_GUI_CC_NEW_PROF14");
            Banners.Add("Unknown", "GFX_GUI_INDICATOR_SELECTED ");
        }


        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "online":
                return "Displays all users online and/or on the private channel.\nIf [profession] is specified, it will display only people online and/or on the private channel that are the specified profession.\n" +
                    "Usage: /tell " + bot.Character + " online [[profession]]";

            case "afk":
                return "Sets yourself AFK (Away From Keyboard) on the bot. Reasons are optional.\n" +
                    "Usage: /tell " + bot.Character + " afk [reason]";

            case "brb":
                return "Sets yourself as BRB (Be Right Back) on the bot. Reasons are optional.\n" +
                    "Usage: /tell " + bot.Character + " brb [reason]";

            case "kiting":
                return "Sets yourself as \"Kiting\" (and thus can't chat) on the bot.\n" +
                    "Usage: /tell " + bot.Character + " kiting";

            }
            return null;
        }
    }
}
