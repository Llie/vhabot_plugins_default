using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.IO;
using System.Data;
using System.Timers;
using AoLib.Utils;
using AoLib.Net;

namespace VhaBot.Plugins
{
    public class VhCity : PluginBase
    {

        public enum VhCityState
        {
            Unknown,
            Enabled,
            Disabled
        }

        public enum VhCityRemindState
        {
             Unknown,
             Waiting,
             Halfway,
             FiveMinutes,
             PreAnnounce,
             Announced,
             PostAnnounce
        }

        private VhCityState _state = VhCityState.Unknown;
        private VhCityRemindState _reminders = VhCityRemindState.Unknown;
        public VhCityState State { get { return this._state; } }
        public VhCityRemindState Reminded { get { return this._reminders; } }
        private DateTime _time = DateTime.Now;
        private BotShell _bot;
        private Config _database;
        private Timer _wave_timer;
        private int _current_wave = -1;
        private bool _targeted = false;
        private DateTime _attack_time = DateTime.Now;
        // 1st wave ~5 minutes after city targeted message
        // 2nd wave ~3 minutes after 1st wave
        // 3rd wave ~2 minutes after 2nd wave
        // 4th wave ~2 minutes after 3rd wave
        // 5th wave ~2 minutes after 4th wave
        // 6th wave ~2 minutes after 5th wave
        // 7th wave ~2 minutes after 6th wave
        // 8th wave ~2 minutes after 7th wave
        // General ~2 minutes after 8th wave
        // Attack should end ~15 minutes after General
        private static TimeSpan two_min = new TimeSpan ( 0, 1, 45 );
        private int[] _wave_timings = new int[9] { 285, 165, 120, 120, 120, 120, 120, 120, 115 };
        private string [] _wave_names = new string[9] { "first wave of alien invaders", "second wave of alien invaders", "third wave of alien invaders", "fourth wave of alien invaders", "fifth wave of alien invaders", "sixth wave of alien invaders", "seventh wave of alien invaders", "eighth wave of alien invaders", "Alien General" };

        private Timer _welcome;

        public TimeSpan TimeLeft
        {
            get
            {
                TimeSpan span = DateTime.Now - this._time;
                if (span.TotalHours >= 1) return new TimeSpan(0);
                return new TimeSpan(1, 0, 0) - span;
            }
        }

        public TimeSpan TimeExpired
        {
            get
            {
                TimeSpan span = DateTime.Now - this._time;
                return span;
            }
        }
        private string _username = string.Empty;
        private bool _sendgc = true;
        private bool _sendpg = true;
        private bool _sendirc = true;
        private bool _warnlogon = false;
        private bool _wavetimer = false;

        public VhCity()
        {
            this.Name = "City Cloak and Upkeep Tracker";
            this.InternalName = "VhCity";
            this.Author = "Iriche / Vhab / Llie / Veremit / Kilmanagh";
            this.DefaultState = PluginState.Installed;
            this.Version = 151;
            this.Commands = new Command[] {
                new Command("aliens", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("cloak", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("city", true, UserLevel.Guest, UserLevel.Member, UserLevel.Guest),
                new Command("cloak set", true, UserLevel.Leader ),
                new Command("cloak reset", true, UserLevel.Leader),
                new Command("upkeep", true, UserLevel.Leader)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            bot.Events.ChannelMessageEvent += new ChannelMessageHandler(OnChannelMessageEvent);
            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
            bot.Timers.Minute += new EventHandler(OnMinute);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "sendgc", "Send notifications to the organization channel", this._sendgc);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "sendpg", "Send notifications to the private channel", this._sendpg);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "sendirc", "Send notifications to IRC", this._sendirc);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "warnlogon", "Send warning message to members at logon", this._warnlogon);
            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "wavetimer", "Send warning with each wave", this._wavetimer);
            this._bot = bot;
            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS citycloak (cloak_id integer PRIMARY KEY AUTOINCREMENT, time INT NOT NULL, action VARCHAR(10), player VARCHAR(15))");
            _database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS upkeep (datetime REAL NOT NULL, credits INT NOT NULL)");

            this.LoadConfiguration(bot);

            this._welcome = new Timer(7000);
            this._welcome.Elapsed += new ElapsedEventHandler(WelcomeTimerElapsed);
            this._welcome.AutoReset = false;
            this._welcome.Enabled = true;

            this._wave_timer = new Timer();
            this._wave_timer.Elapsed += new ElapsedEventHandler(WaveTimerElapsed);
            this._wave_timer.AutoReset = false;
            this._wave_timer.Enabled = false;
        }

        public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
            {
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= CityWindow;
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += CityWindow;
                this._bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);
                this._bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            }
        }

        public void WelcomeUnload( BotShell bot )
        {
            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
        }

        public override void OnUnload(BotShell bot)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= CityWindow;
            bot.Events.ChannelMessageEvent -= new ChannelMessageHandler(OnChannelMessageEvent);
            bot.Events.ConfigurationChangedEvent -= new ConfigurationChangedHandler(ConfigurationChangedEvent);
            bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            bot.Timers.Minute -= new EventHandler(OnMinute);
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._sendgc = bot.Configuration.GetBoolean(this.InternalName, "sendgc", this._sendgc);
            this._sendpg = bot.Configuration.GetBoolean(this.InternalName, "sendpg", this._sendpg);
            this._sendirc = bot.Configuration.GetBoolean(this.InternalName, "sendirc", this._sendirc);
            this._warnlogon = bot.Configuration.GetBoolean(this.InternalName, "warnlogon", this._warnlogon);
            this._wavetimer = bot.Configuration.GetBoolean(this.InternalName, "wavetimer", this._wavetimer);
        }

        private void UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (e.First) return;
            if ( this.RaidInProgress() && this._warnlogon )
                bot.SendPrivateMessage(e.SenderID, "Warning: Aliens are currently attacking the city.  Please avoid entering the city." );
        }

        public void WaveTimerElapsed(object source, ElapsedEventArgs e)
        {

            if ( _current_wave < this._wave_timings.Length - 1 )
            {
                 this.SendMessage(this._bot,
                                  "Warning: Sensors have detected the " +
                                  _wave_names[++_current_wave] + " inbound." );

                 if ( _current_wave < this._wave_timings.Length - 1 )
                 {
                     this._wave_timer.Interval=this._wave_timings[_current_wave+1]*1000.0;
                     this._wave_timer.Start();
                 }
                 else 
                 {
                     // Start one more timer for 15 minutes after the
                     // Alien General attacks
                     this._wave_timer.Interval = 15.0*60.0*1000.0;
                     this._wave_timer.Start();
                 }
            }
            else
            {
                // 15 minutes after the General attacks the city raid
                // should have ended
                this._wave_timer.Stop();
                this._targeted = false;
                this._current_wave = -1;
            }

        }

        public void TriggerWaveTimer( bool RandomAttack )
        {
            this._current_wave = -1;
            if ( RandomAttack )
                this._wave_timer.Interval=_wave_timings[0]*1000.0 -
                    two_min.TotalMilliseconds;
            else
                this._wave_timer.Interval=_wave_timings[0]*1000.0;
            this._wave_timer.Start();
        }

        private void OnChannelMessageEvent(BotShell bot, ChannelMessageArgs e)
        {
            if (e.Type != ChannelType.Organization) return;
            if (!e.IsDescrambled) return;
            if (e.Descrambled.CategoryID == 508)
            {
                if (e.Descrambled.EntryID == 192568104)  // Upkeep
                {
                    //508	192652356	%d credits have been deducted from the organization bank for the upkeep of your city. Next payment is due in %d days.
                    string credits = e.Descrambled.Arguments[0].Message;
                    string duedays = e.Descrambled.Arguments[1].Message;
                    DateTime Due = DateTime.Now.AddDays(Convert.ToInt32(duedays)).ToUniversalTime();
                    _database.ExecuteNonQuery("Delete * from upkeep)");
                    _database.ExecuteNonQuery( "INSERT INTO [upkeep] (datetime, credits) VALUES ('" + Due.ToLongDateString() + "'," + credits + ")");
                }
                return;
            }
            if (e.Descrambled.CategoryID != 1001) return;
            string action = "";
            switch (e.Descrambled.EntryID)
            {
                case 1: // %s turned the cloaking device in your city %s.
                    this._time = DateTime.Now;
                    this._username = e.Descrambled.Arguments[0].Text;
                    this._reminders = VhCityRemindState.Waiting;
                    if (e.Descrambled.Arguments[1].Text.Equals("on", StringComparison.CurrentCultureIgnoreCase))
                    {
                        this._state = VhCityState.Enabled;
                        action = "on";
                        this.SendMessage(bot, HTML.CreateColorString(bot.ColorHeaderHex, this._username) + " has " + HTML.CreateColorString(RichTextWindow.ColorGreen, "enabled") + " the city cloaking device");
                        this._targeted = false;
                    }
                    else if (e.Descrambled.Arguments[1].Text.Equals("off", StringComparison.CurrentCultureIgnoreCase))
                    {
                        this._state = VhCityState.Disabled;
                        action = "off";
                        this.SendMessage(bot, HTML.CreateColorString(bot.ColorHeaderHex, this._username) + " has " + HTML.CreateColorString(RichTextWindow.ColorRed, "disabled") + " the city cloaking device");
                        this._attack_time = DateTime.Now;
                        this._targeted = true;
                        if ( this._wavetimer )
                            TriggerWaveTimer( false );
                    }
                    else
                    {
                        this._state = VhCityState.Unknown;
                    }
                    if (this._state != VhCityState.Unknown)
                    {
                        try
                        {
                            using (IDbCommand command = this._database.Connection.CreateCommand())
                            {
                                command.CommandText = "INSERT INTO [citycloak] (time, action, player) VALUES (" + TimeStamp.Now + ", '" + action + "', '" + this._username + "')";
                                command.ExecuteReader();
                            }
                        }
                        catch
                        {
                            this.SendMessage(this._bot, "Error during city cloak history archiving!");
                        }
                    }
                    break;

                case 3: // Your city in %s has been targeted by hostile forces.
                    // if city was target of a random alien attack
                    if ( ! this._targeted )
                    {
                        this._attack_time = DateTime.Now;
                        this._attack_time =
                            this._attack_time.Subtract( VhCity.two_min );
                        this._targeted = true;
                        if ( this._wavetimer )
                            TriggerWaveTimer( true );
                    }
                    try
                    {
                        using (IDbCommand command = this._database.Connection.CreateCommand())
                        {
                            command.CommandText = "INSERT INTO [citycloak] (time, action) VALUES (" + TimeStamp.Now + ", 'attack')";
                            command.ExecuteReader();
                        }
                    }
                    catch
                    {
                        this.SendMessage(this._bot, "Error during city cloak history archiving!");
                    }
                    break;
            }
        }

        public bool RaidInProgress()
        {
            return this._targeted;
        }

        private void OnMinute(object sender, EventArgs e)
        {

            //if (this._state == VhCityState.Unknown) return;
            if (this._reminders == VhCityRemindState.Unknown) return;

            TimeSpan span = this.TimeExpired;
            int minutes = 30;
            if (this._reminders == VhCityRemindState.FiveMinutes)
               minutes += 25;
            else if (this._reminders == VhCityRemindState.Halfway)
               minutes += 15;
            else if (this._reminders == VhCityRemindState.PostAnnounce)
               minutes += 60;
            else if (this._reminders == VhCityRemindState.Announced)
               minutes += 45;
            switch (this._reminders)
            {
                 case VhCityRemindState.FiveMinutes:
                 case VhCityRemindState.Halfway:
                 case VhCityRemindState.Waiting:
                    if (span.TotalMinutes >= minutes)
                    {
                        string state;
                        if (this._state == VhCityState.Enabled)
                            state = HTML.CreateColorString(RichTextWindow.ColorGreen, "enabled");
                        else
                            state = HTML.CreateColorString(RichTextWindow.ColorRed, "disabled");
                        this.SendMessage(this._bot, HTML.CreateColorString(this._bot.ColorHeaderHex, minutes.ToString()) + " minutes have passed since " + HTML.CreateColorString(this._bot.ColorHeaderHex, this._username) + " has " + state + " the city cloaking device");
                        if (this._reminders == VhCityRemindState.Waiting)
                          this._reminders = VhCityRemindState.Halfway;
                        else if (this._reminders == VhCityRemindState.Halfway)
                          this._reminders = VhCityRemindState.FiveMinutes;
                        else
                          this._reminders = VhCityRemindState.PreAnnounce;
                    }
                    break;

                 case VhCityRemindState.PreAnnounce:
                    if (span.TotalMinutes >= 60)
                    {
                        if (this._state == VhCityState.Disabled)
                            this.SendMessage(this._bot, "The city cloaking device has fully recovered from the alien attack. Please enable the city cloaking device");
                        else
                            this.SendMessage(this._bot, "The city cloaking device has finished charging. New alien attacks can now be initiated");
                          this._reminders = VhCityRemindState.Announced;
                    }
                    break;

                 case VhCityRemindState.PostAnnounce:
                 case VhCityRemindState.Announced:
                    if (span.TotalMinutes >= minutes && this._state == VhCityState.Disabled)
                    {
                         minutes -= 60;
                         this.SendMessage(this._bot, HTML.CreateColorString(this._bot.ColorHeaderHex, minutes.ToString()) + " minutes have passed since the city cloaking device has fully recovered. Please enable the city cloaking device");
                         if (this._reminders == VhCityRemindState.Announced)
                             this._reminders = VhCityRemindState.PostAnnounce;
                         else
                             this._reminders = VhCityRemindState.Unknown;
                    }
                    break;
            }
        }

        public void OnAliensCommand(BotShell bot, CommandArgs e)
        {
            if ( this._targeted == true )
            {

                if ( _current_wave < 0 )
                    bot.SendReply(e, "There is currently an alien invasion of the city but the aliens have not yet arrived.");
                else if ( _current_wave > 8 )
                    bot.SendReply(e, "There is currently an alien invasion of the city.  The " + HTML.CreateColorString(RichTextWindow.ColorRed, "Alien General") + " has already attacked the city.");
                else
                    bot.SendReply(e, "There is currently an alien invasion of the city.  The " + HTML.CreateColorString(RichTextWindow.ColorRed, _wave_names[_current_wave] ) + " currently attacking the city");

            }
            else
            {
                bot.SendReply(e, "There is no alien invasion currently in progress.");
            }
        }

        public void OnCloakCommand(BotShell bot, CommandArgs e)
        {
            if (this.State == VhCityState.Enabled && this.TimeLeft.TotalMinutes <= 0)
                bot.SendReply(e, "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorGreen, "enabled") + " and has been fully charged");
            else if (this.State == VhCityState.Enabled)
                bot.SendReply(e, "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorGreen, "enabled") + " and will finish charging in " + Format.Time(this.TimeLeft, FormatStyle.Medium));
            else if (this.State == VhCityState.Disabled && this.TimeLeft.TotalMinutes <= 0)
                bot.SendReply(e, "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorRed, "disabled") + " and requires enabling");
            else if (this.State == VhCityState.Disabled)
                bot.SendReply(e, "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorRed, "disabled") + " and will require enabling in " + Format.Time(this.TimeLeft, FormatStyle.Medium));
            else
                bot.SendReply(e, "The city cloak state is currently " + HTML.CreateColorString(RichTextWindow.ColorOrange, "unknown"));            
        }

        public void OnCityCommand(BotShell bot, CommandArgs e)
        {
            try
            {
                using (IDbCommand command = this._database.Connection.CreateCommand())
                {
                    RichTextWindow window = new RichTextWindow(bot);
                    window.AppendTitle("City Cloak History");
                    window.AppendLineBreak();
                    command.CommandText = "SELECT [time], [action], [player] FROM [citycloak] ORDER BY [time] DESC LIMIT 10";
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(1))
                        {
                            window.AppendHighlight(Format.DateTime(reader.GetInt64(0), FormatStyle.Compact) + " GMT");
                            window.AppendNormal(" - ");
                            if (reader.GetString(1) == "attack")
                            {
                                window.AppendNormal("Alien attack");
                            }
                            else
                            {
                                window.AppendNormal(reader.GetString(2));
                                if (reader.GetString(1) == "on")
                                    window.AppendColorString(RichTextWindow.ColorGreen," enabled ");
                                else
                                    window.AppendColorString(RichTextWindow.ColorRed," disabled ");
                                window.AppendNormal("the city cloaking device");
                            }
                            window.AppendLineBreak();
                        }
                    }
                    string message = "";
                    if (this.State == VhCityState.Enabled && this.TimeLeft.TotalMinutes <= 0)
                        message = "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorGreen, "enabled") + " and has been fully charged.";
                    else if (this.State == VhCityState.Enabled)
                        message = "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorGreen, "enabled") + " and will finish charging in " + Format.Time(this.TimeLeft, FormatStyle.Medium) + ".";
                    else if (this.State == VhCityState.Disabled && this.TimeLeft.TotalMinutes <= 0)
                        message = "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorRed, "disabled") + " and requires enabling.";
                    else if (this.State == VhCityState.Disabled)
                        message = "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorRed, "disabled") + " and will require enabling in " + Format.Time(this.TimeLeft, FormatStyle.Medium) + ".";
                    else
                        message = "The city cloak state is currently " + HTML.CreateColorString(RichTextWindow.ColorOrange, "unknown.");
                    bot.SendReply(e, message + " History »» ",window);
                    return;
                }
            }
            catch
            {
                bot.SendReply(e, "Error retrieving city cloak history!");
            }
            
        }

        private void OnCloakSetCommand(BotShell bot, CommandArgs e)
        {
            if (this._state != VhCityState.Unknown)
            {
               if (this._state == VhCityState.Enabled)
                 bot.SendReply(e, "Cloak is currently enabled. Use: 'cloak reset' to reset");
               else
                 bot.SendReply(e, "Cloak is currently disabled. Use: 'cloak reset' to reset");
               return;
            }
            if (e.Args.Length < 1)
            {
                bot.SendReply(e, "Correct Usage: cloak set [enabled|disabled]");
                return;
            }
            string setting = e.Args[0].ToLower();
            if (setting != "enabled" && setting != "disabled")
            {
                bot.SendReply(e, "Correct Usage: cloak set [enabled|disabled]");
                return;
            }
            if (setting == "enabled")
            {
              this._state = VhCityState.Enabled;
              bot.SendReply(e, "The city cloak state has been set to Enabled.");
            }
            else
            {
              this._state = VhCityState.Disabled;
              bot.SendReply(e, "The city cloak state has been set to Disabled.");
            }
            this._time = DateTime.Now - new TimeSpan(1, 0, 0);
            this._reminders = VhCityRemindState.Unknown;
        }

        public void CityWindow( ref RichTextWindow window, string user )
        {
            window.AppendHeader( "City Cloak Status");
            window.AppendLineBreak();
            if (this.State == VhCityState.Enabled && this.TimeLeft.TotalMinutes <= 0)
                window.AppendRawString( "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorGreen, "enabled") + " and has been fully charged");
            else if (this.State == VhCityState.Enabled)
                window.AppendRawString( "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorGreen, "enabled") + " and will finish charging in " + Format.Time(this.TimeLeft, FormatStyle.Medium));
            else if (this.State == VhCityState.Disabled && this.TimeLeft.TotalMinutes <= 0)
                window.AppendRawString( "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorRed, "disabled") + " and requires enabling");
            else if (this.State == VhCityState.Disabled)
                window.AppendRawString( "The city cloak is currently " + HTML.CreateColorString(RichTextWindow.ColorRed, "disabled") + " and will require enabling in " + Format.Time(this.TimeLeft, FormatStyle.Medium));
            else
                window.AppendRawString( "The city cloak state is currently " + HTML.CreateColorString(RichTextWindow.ColorOrange, "unknown"));
            window.AppendLineBreak();
            if ( this._targeted == true )
            {

                if ( _current_wave < 0 )
                    window.AppendRawString( "There is currently an alien invasion of the city but the aliens have not yet arrived.");
                else if ( _current_wave > 8 )
                    window.AppendRawString( "There is currently an alien invasion of the city.  The " + HTML.CreateColorString(RichTextWindow.ColorRed, "Alien General") + " has already attacked the city.");
                else
                    window.AppendRawString( "There is currently an alien invasion of the city.  The " + HTML.CreateColorString(RichTextWindow.ColorRed, _wave_names[_current_wave] ) + " currently attacking the city");
            }
            else
            {
                window.AppendRawString( "There is no alien invasion currently in progress.");
            }
            window.AppendLineBreak();
            
            window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
            window.AppendLineBreak();

        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {

            case "aliens":
                OnAliensCommand( bot, e );
                break;

            case "city":
                OnCityCommand( bot, e );
                break;

            case "cloak":
                OnCloakCommand( bot, e );
                break;

            case "cloak set":
                OnCloakSetCommand( bot, e );
                break;

            case "cloak reset":
                this._state = VhCityState.Unknown;
                this._targeted = false;
                bot.SendReply(e, "The city cloak state has been reset to unknown");
                break;

            case "upkeep":
                OnUpkeepCommand(bot, e);
                break;

            }
        }

        public void SendMessage(BotShell bot, string message)
        {
            if (this._sendgc) bot.SendOrganizationMessage(bot.ColorHighlight + message);
            if (this._sendpg) bot.SendPrivateChannelMessage(bot.ColorHighlight + message);
            if (this._sendirc)bot.SendIrcMessage(message);
        }

        private void OnUpkeepCommand(BotShell bot, CommandArgs e)
        {

            try
            {
                IDbCommand dbCommand = _database.Connection.CreateCommand();
                using (dbCommand)
                {
                    dbCommand.CommandText = "Select * FROM upkeep";
                    IDataReader dataReader = dbCommand.ExecuteReader();
                    bool data_found = false;

                    while (dataReader.Read())
                    {
                        data_found = true;
                        string Duedate = dataReader.GetString(0);
                        int credits = dataReader.GetInt32(1);

                        bot.SendReply(e, String.Format("The next payment is due on {0} days. The estimated credits due are: {1}", Duedate, credits.ToString()));

                    }

                    if ( ! data_found )
                        bot.SendReply(e, "No upkeep information has been stored.");
                }

            }
            catch
            {
                bot.SendReply(e, "An error occurred attempting to retrieve upkeep information.");
            }

        }

		public override string OnHelp(BotShell bot, string command)
        {
            switch (command.ToLower())
            {

            case "aliens":
                return "Displays the current wave of aliens if city is currently under attack.\n" +
                    "Usage: /tell " + bot.Character + " aliens";

            case "cloak":
                return "Displays the current state of the city cloak.\n" +
                    "Usage: /tell " + bot.Character + " cloak";

            case "city":
                return "Displays the history of city cloak and alien invasions.\n" +
                    "Usage: /tell " + bot.Character + " city";

            case "cloak reset":
                return "Resets the current state of the city cloak to unknown.\n" +
                    "Usage: /tell " + bot.Character + " cloak reset";

            case "cloak set":
                return "Set the current state of the city cloak.\n" +
                    "Usage: /tell " + bot.Character + " cloak set [enabled|disabled]";

            case "upkeep":
                return "Displays the date and estimated credits owed for city upkeep.\n" +
                    "Usage: /tell " + bot.Character + " upkeep";

            }
            return null;
        }
    }
}
