using System;
using System.Data;
using System.Reflection;
using AoLib.Utils;

namespace VhaBot.Plugins
{

    public class vhWelcome : PluginBase
    {

        // Purpose:

        // This plug-in is intended to reduce logon spam from VhaBot.
        // Integration of other plug-ins into this "Unified Welcome
        // Window" should not mean the removal of existing
        // notifications.  This function is intended only for bots
        // that run many plug-ins with notifications.  If this plug-in
        // is not running, then other plug-ins should fall back to
        // their original behavior.

        // Integration:

        // In order to integrate a plug-in to use the welcome window,
        // a plug-in needs to add a Timer that will wait a few seconds
        // to insure that all plug-ins have loaded.  When the timer
        // expires it must check to see if the Welcome plug-in is
        // configured to load.  If so, then the delegate which appends
        // data to the logon window should be added to the Welcome
        // plug-in's delegates.  If appropriate, the user logon and
        // user join private channel events should be prevented from
        // being added to the plug-in's event delegates if those
        // events do nothing more than notify the user with
        // information at logon or channel join.  If those functions
        // do something else in addition to just report information,
        // then some logic will need to be added to that event to
        // prevent displaying of that information if the Welcome
        // plug-in is loaded. Details to follow:

        // Add to a plug-in's members:

        // private Timer _welcome;

        // Add to a plug-in's OnLoad member:

        // this._welcome = new Timer(7000);
        // this._welcome.Elapsed +=
        //     new ElapsedEventHandler(WelcomeTimerElapsed);
        // this._welcome.AutoReset = false;
        // this._welcome.Enabled = true;

        // Add to a plug-in's OnUnload member:

        // if ( this._bot.Plugins.IsLoaded("vhWelcome") )
        //     ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= AppendWindow;

        // Add the WelcomeTimerElapsed function:

        // This function should remove any logon or user join events
        // to allow the Welcome plug-in to handle all the logon/join
        // information that will be presented.  This function also
        // adds the AppendWindow delegate to the Welcome plug-in.  The
        // remove prior to the add insures that the AppendWindow
        // delegate for this plug-in is only added once.

        // public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        // {
        //     if ( this._bot.Plugins.IsLoaded("vhWelcome") )
        //     {
        //         ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= AppendWindow;
        //         ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += AppendWindow;
        //         this._bot.Events.UserLogonEvent -=
        //             new UserLogonHandler(Events_UserLogonEvent);
        //         this._bot.Events.UserJoinChannelEvent -=
        //             new UserJoinChannelHandler(Events_UserJoinChannelEvent);
        //     }
        // }

        // Add a function that can be called to restore a plug-in's
        // ability to notify without the welcome window

        // public void WelcomeUnload( BotShell bot )
        // {
        //     this._bot.Events.UserLogonEvent -=
        //         new UserLogonHandler(Events_UserLogonEvent);
        //     this._bot.Events.UserJoinChannelEvent -=
        //         new UserJoinChannelHandler(Events_UserJoinChannelEvent);
        // }

        // Add the function that will append information to the Welcome window:

        // public void AppendWindow( ref RichTextWindow window, string user )
        // {
        //     window.AppendHeader( "Header...");
        //     window.AppendLineBreak();
        // }

        // Uniformity:

        // In order to insure that each section of the welcome window
        // is consistent with all other sections, please start each
        // window with:

        // window.AppendHeader( "Section Name" );
        // window.AppendLineBreak();

        // and finish each window delegate function with:

        // window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
        // window.AppendLineBreak();

        public delegate void WelcomeSectionDelegate( ref RichTextWindow args, string sender );

        public WelcomeSectionDelegate Delegates;

        public vhWelcome()
        {
            this.Name = "Welcome";
            this.InternalName = "vhWelcome";
            this.Author = "Llie";
            this.Description = "Consolidate information normally sent from various plug-ins at user logon into a single window.";
            this.Version = 100;
            this.DefaultState = PluginState.Installed;
            this.Commands = new Command[] {
                new Command("welcome", true, UserLevel.Guest),
            };
        }

        public override void OnLoad(BotShell bot)
        {
            bot.Events.UserLogonEvent +=
                new UserLogonHandler(UserLogonEvent);
            bot.Events.UserJoinChannelEvent +=
                new UserJoinChannelHandler(UserJoinChannelEvent);

            // go through list of loaded plug-ins looking for
            // WelcomeTimerElapsed method
            string[] loaded_plugins = bot.Plugins.GetLoadedPlugins();
            foreach( string plugin in loaded_plugins )
            {
                Type plugin_type = bot.Plugins.GetPlugin(plugin).GetType();
                MethodInfo welcome_method = plugin_type.GetMethod("WelcomeTimerElapsed");
                bool has_welcome = ( welcome_method != null);
                if ( has_welcome )
                    welcome_method.Invoke( bot.Plugins.GetPlugin(plugin), new object[]{ null, null } );
            }

        }

        public override void OnUnload(BotShell bot)
        {
            bot.Events.UserLogonEvent -=
                new UserLogonHandler(UserLogonEvent);
            bot.Events.UserJoinChannelEvent -=
                new UserJoinChannelHandler(UserJoinChannelEvent);

            // go through list of loaded plug-ins looking for
            // WelcomeUnload method
            string[] loaded_plugins = bot.Plugins.GetLoadedPlugins();
            foreach( string plugin in loaded_plugins )
            {
                Type plugin_type = bot.Plugins.GetPlugin(plugin).GetType();
                MethodInfo welcome_method = plugin_type.GetMethod("WelcomeUnload");
                bool has_welcome = ( welcome_method != null);
                if ( has_welcome )
                    welcome_method.Invoke( bot.Plugins.GetPlugin(plugin), new object[]{ bot } );
            }

        }

        private void UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (e.First) return;
            if (!e.Sections.Contains("notify")) return;
            this.SendWelcome( bot, e.Sender );
        }

        private void UserJoinChannelEvent(BotShell bot, UserJoinChannelArgs e)
        {
            // do not send another message if user is also on notify list
            if ( Array.IndexOf(bot.FriendList.Online("notify"), e.Sender) >= 0 )
                return;
            this.SendWelcome( bot, e.Sender );
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {
            case "welcome":
                SendWelcome( bot, e.Sender );
                break;
            }
        }

        public void SendWelcome( BotShell bot, string user )
        {
            
            string source = bot.Character;
            if ( ( bot.Organization != null ) &&
                 ( bot.Organization != string.Empty ) )
                source = bot.Organization;

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle( "Welcome to " + source );
            window.AppendLineBreak();
            window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
            window.AppendLineBreak();

            this.Delegates( ref window, user );

            bot.SendPrivateMessage( user, "Welcome to " + source + " »» " +
                                    window.ToString() );

        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "welcome":
                return "Send the welcome message to yourself.\n" +
                    "Usage: /tell " + bot.Character + " welcome";

            }
            return null;
        }

    }
}
