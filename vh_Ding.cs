using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Data;
using AoLib.Utils;
using AoLib.Net;

namespace VhaBot.Plugins
{
    public class VhDing : PluginBase
    {
        private BotShell _bot;

        private int[] titlebreaks= new int[] { 0, 5, 15, 50, 100, 150, 190, 205 };

        private string[] replies_nolevel = new string[] {
            "Umm... ya... Congratulations on whatever level you just dinged, (sender)" ,
            "Yay!, (sender) just dinged... something.",
            "dong!",
            "Yeah yeah gratz, I would give you a better response but you didn't say what you dinged ( Usage: !ding 'level' )",
            "Hmmm, I really want to know what level you dinged, but gratz anyways nub.",
            "When are you people going to start using me right! Gratz for your level though.",
            "Gratz! But what are we looking at? I need a level next time."
        };

        private string[] replies_negative = new string[] {
            "Reclaim sure is doing a number on you if you're going backwards...",
            "That sounds like a problem... so how are your skills looking?",
            "Wtb negative exp kite teams!",
            "That leaves you with... (difflevel) more levels until 220.  I don't see the problem.",
            "How the heck did you get to (level)?"
        };

        private string[] replies_one = new string[] {
            "You didn't even start yet...",
			"Did you somehow start from level 0?",
			"Dinged from 1 to 1? Congratz"
        };

        private string[] replies_150 = new string[] {
			"S10 time!!!",
			"Time to ungimp yourself! Horray!. Congrats =)",
			"What starts with A, and ends with Z? ALIUMZ!",
			"Wow, is it that time already? TL 5 really? You sure are moving along! Gratz",
            "Woot! Congratulations on Title Level 5"
        };

        private string[] replies_180 = new string[] {
            "Congratz! Now go kill some aliumz at S13/28/35!!",
			"Only 20 more froob levels to go! HOORAH!",
			"Yay, only 10 more levels until TL 6! Way to go!"
        };

        private string[] replies_200 = new string[] {
            "Congratz! The big Two Zero Zero!!! Party at (sender)'s place",
			"Best of the best in froob terms.  Congratulations!",
			"What a day indeed. Finally done with froob levels. Way to go!"
        };

        private string[] replies_210 = new string[] {
            "Congratz! Just a few more levels to go!",
			"Enough with the dingin you are making the fr00bs feel bad!",
			"Come on save some dings for the rest!"
        };

        private string[] replies_220 = new string[] {
            "Congratz! You have reached the end of the line! No more fun for you :P",
			"You finally made it! What an accomplishment... Congratulations (sender), for reaching a level reserved for the greatest!",
			"I'm going to miss you a great deal, because after this, we no longer can be together (sender). We must part so you can continue getting your research and AI levels done! Farewell!",
			"How was the inferno grind? I'm glad to see you made it through, and congratulations for finally getting the level you well deserved!",
			"Our congratulations, to our newest level 220 member, (sender), for his dedication. We present him with his new honorary rank, Chuck Norris!"
        };

        private string[] replies_overflow = new string[] {
			"Umm...no.",
			"You must be high, because that number is to high...",
			"Ha, ha... ha, yeah... no...",
			"You must be a GM or one heck of an exploiter!",
			"Hey! Congratz on joining ARK!",
			"Yeah, and I'm Chuck Norris...",
			"Not now, not later, not ever..."            
        };

        private string[] replies_generic = new string[] {
            "Congratulations on (level), (sender)",
            "Woot! (level)! You go, (sender).",
            "Congratz! (level) - (sender) you rock!",
            "Ding ding ding... now ding some more!",
            "Keep em coming!",
            "Don't stop now, you're getting there!",
            "Come on, COME ON! Only (difflevel) more levels to go until 220!"
        };

        public VhDing()
        {
            this.Name = "Org Congratulatory Function";
            this.InternalName = "VhDing";
            this.Author = "Llie";
            this.DefaultState = PluginState.Installed;
            this.Description = "Responds to ding messages";
            this.Version = 100;
            this.Commands = new Command[] {
                new Command("ding", true, UserLevel.Guest)
            };
        }

        public override void OnLoad(BotShell bot)
        {
            this._bot = bot;
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {
                case "ding":
                    this.OnDingCommand(bot, e);
                    break;
            }
        }

        public void OnDingCommand(BotShell bot, CommandArgs e)
        {

            if( e.Type == CommandType.Tell )
            {
                bot.SendReply(e, "Congratulations, " + e.Sender +
                              ". It'll be our little secret. *wink* *wink*" );
                return ;
            }

            int newlevel = 0;
            int newtitle = 0;

            if (e.Words.Length > 0 )
            {
                // parse out all punctuation
                string parsed_arg = Regex.Replace ( e.Args[0], @"\D", "" );

                // keep negative numbers for jokers
                Int32 negative = 1;
                if ( e.Args[0][0] == '-' )
                    negative = -1;

                // parse string into integer
                if ( ! int.TryParse( parsed_arg , out newlevel ) )
                    newlevel = 0;

                // put back the negative value, again, for the jokers
                newlevel *= negative;

                // note if sender hit a new TL
                newtitle = InIntList( titlebreaks, newlevel );
            }

            // Send messages from different arrays of strings depending on level
            if ( (e.Words.Length < 1) )
                SendEverywhere( PickRandom( replies_nolevel, e.Sender, Convert.ToString(newlevel) ) );
            else if ( newlevel <= 0 )
                SendEverywhere( PickRandom( replies_negative, e.Sender, Convert.ToString(newlevel) ) );
            else if ( newlevel == 1 )
                SendEverywhere( PickRandom( replies_one, e.Sender, Convert.ToString(newlevel) ) );
            else if ( newlevel == 150 )
                SendEverywhere( PickRandom( replies_150, e.Sender, Convert.ToString(newlevel) ) );
            else if ( newlevel == 180 )
                SendEverywhere( PickRandom( replies_180, e.Sender, Convert.ToString(newlevel) ) );
            else if ( newlevel == 200 )
                SendEverywhere( PickRandom( replies_200, e.Sender, Convert.ToString(newlevel) ) );
            else if ( newlevel == 210 )
                SendEverywhere( PickRandom( replies_210, e.Sender, Convert.ToString(newlevel) ) );
            else if ( newlevel == 220 )
                SendEverywhere( PickRandom( replies_220, e.Sender, Convert.ToString(newlevel) ) );
            else if ( newtitle > 0 )
                SendEverywhere( "Woot! Congratulations on Title Level " + Convert.ToString(newtitle) + ", " + e.Sender );
            else if ( newlevel > 220 )
                SendEverywhere( PickRandom( replies_overflow, e.Sender, Convert.ToString(newlevel) ) );
            else
                SendEverywhere( PickRandom( replies_generic, e.Sender, Convert.ToString(newlevel) ) );
        }

        public void SendEverywhere( string output )
        {
            _bot.SendPrivateChannelMessage( output );
            if ( _bot.InOrganization )
                _bot.SendOrganizationMessage( output );
            return;
        }

        public int InIntList(int[] list, int testnum )
        {
            int retval = -1;
            for (int iter = 0; iter < list.Length; iter++ )
                if ( list[iter] == testnum )
                    retval = iter;
            return retval;
        }

        public string PickRandom(string[] selection, string sender, string level)
        {
            Random random = new Random();
            string message = selection[random.Next(selection.Length)];
            string endgamediff = Convert.ToString(220-Convert.ToInt32(level));
            message = message.Replace("(sender)", sender );
            message = message.Replace("(level)", level );
            message = message.Replace("(difflevel)", endgamediff );
            return message;
        }
    }
}
