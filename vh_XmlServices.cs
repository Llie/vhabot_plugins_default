using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Globalization;
using AoLib.Utils;
using AoLib.Net;

namespace VhaBot.Plugins
{
    public class XmlService : PluginBase
    {

        static string OrgWebsite = "http://people.anarchy-online.com/org/stats/d/{0}/name/{1}";
        static string CharWebsite = "http://people.anarchy-online.com/character/bio/d/{0}/name/{1}";

        static string CharRKNetWebsite = "http://people.rubi-ka.net/characters/details/?var={0}";
        static string OrgRKNetWebsite = "http://people.rubi-ka.net/organizations/details/?var={0}";

        static string RKNetXML = "http://people.rubi-ka.net/services/characters.asmx/GetCombinedHistory?name={0}";

        // other Rubi-Ka.net look-up URLS
        static string RKNetFindOrg = "http://people.rubi-ka.net/organizations?search=%2B{0}&faction=0&government=0";

        // legacy/pre-server-merge information
        static string CharAunoWebsite = "http://auno.org/ao/char.php?dimension={0}&name={1}";
        static string OrgAunoWebsite = "http://auno.org/ao/char.php?dimension={0}&guild={1}";

        private string TimeoutError = "(This may be because the server was too slow to respond or is currently unavailable)";

        private bool _ancient_history = false;

        public XmlService()
        {
            this.Name = "XML Lookup Services";
            this.InternalName = "vhXmlService";
            this.Author = "Vhab / Veremit / Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 105;
            this.Commands = new Command[] {
                new Command("whois", true, UserLevel.Guest),
                new Command("whoisall", true, UserLevel.Guest),
                new Command("history", true, UserLevel.Guest),
                new Command("organization", true, UserLevel.Guest),
                new Command("org", "organization"),
                new Command("server", true, UserLevel.Guest),
                new Command("is", true, UserLevel.Guest),
                new Command("seen", "is"),
                new Command("lastseen", "is"),
                new Command("xmlcache", true, UserLevel.SuperAdmin),
            };
        }

        public override void OnLoad(BotShell bot)
        {
            bot.Events.ConfigurationChangedEvent += new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "ancient_history", "Automatically append history from before server merge.", this._ancient_history);

            this.LoadConfiguration(bot);

            Dimensions dimensions = Dimensions.ParseXML( string.Empty );
            bool found = false;
            foreach ( Dimension dim in dimensions.Dimension )
                if ( dim.Name ==  bot.Dimension.Name )
                    found = true;
            if ( ! found )
            {
                Console.WriteLine( "[" + bot.Character +
                                   "] Invalid bot dimension: \"" +
                                   bot.Dimension + "\"." );
                Console.WriteLine( "[" + bot.Character +
                                   "]    Dimension may only be one of the following:" );
                foreach ( Dimension dim in dimensions.Dimension )
                    Console.WriteLine( "[" + bot.Character + "]      " +
                                       dim.Name );
                Console.WriteLine( "[" + bot.Character +
                                   "]    Please correct your config.xml." );
                throw new System.ArgumentException("Invalid bot dimension", "original");
            }

        }

        public override void OnUnload(BotShell bot)
        {
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._ancient_history = bot.Configuration.GetBoolean(this.InternalName, "ancient_history", this._ancient_history);
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {
            switch (e.Command)
            {
            case "whois":
                if (e.Args.Length < 1)
                {
                    bot.SendReply(e, "Correct Usage: whois [username]");
                    return;
                }
                OnWhoisCommand(bot, e, bot.Dimension, false);
                break;
            case "whoisall":
                if (e.Args.Length < 1)
                {
                    bot.SendReply(e, "Correct Usage: whoisall [username]");
                    return;
                }
                Dimensions dims = Dimensions.ParseXML( string.Empty );
                if( ( dims.Dimension != null ) &&
                    ( dims.Dimension.Length > 0 ) )
                    for ( int i=0; i<dims.Dimension.Length-1; i++ )
                        OnWhoisCommand(bot, e, new AoLib.Net.Server(dims.Dimension[i].Name), true);
                // OnWhoisCommand(bot, e, new AoLib.Net.Server("Test"), true);
                break;
            case "history":
                if (e.Args.Length < 1)
                {
                    bot.SendReply(e, "Correct Usage: history [username]");
                    return;
                }
                OnHistoryCommand(bot, e);
                break;
            case "organization":
                if (e.Args.Length < 1)
                {
                    bot.SendReply(e, "Correct Usage: organization [username]");
                    return;
                }
                OnOrganizationCommand(bot, e);
                break;
            case "server":
                this.OnServerCommand(bot, e);
                break;
            case "is":
                if (e.Args.Length < 1)
                {
                    bot.SendReply(e, "Correct Usage: is [username]");
                    return;
                }
                this.OnIsCommand(bot, e);
                break;
            case "xmlcache":
                this.OnXmlCacheCommand(bot, e);
                break;
            }
        }

        private void OnWhoisCommand(BotShell bot, CommandArgs e, AoLib.Net.Server dimension, Boolean showDimension)
        {
            if (!showDimension && dimension == "Test")
            {
                bot.SendReply(e, "The whois command is not available on the test server. Please use 'whoisall' instead");
                return;
            }
            if (dimension == bot.Dimension)
            {
                if (bot.GetUserID(e.Args[0]) < 100)
                {
                    bot.SendReply(e, "No such user: " + HTML.CreateColorString(bot.ColorHeaderHex, e.Args[0]));
                    return;
                }
            }
            WhoisResult whois = XML.GetWhois(e.Args[0].ToLower(), dimension);
            if (whois == null || !whois.Success)
            {
                string error = string.Empty;
                if (showDimension)
                    error += HTML.CreateColorString(bot.ColorHeaderHex, dimension.ToString() + ": ");
                if (whois == null)
                {
                   error += "Unable to gather information on that user " + this.TimeoutError;
                }
                else
                {
                   error += "Invalid information on that user " + this.TimeoutError;
                }
                bot.SendReply(e, error);
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            StringBuilder builder = new StringBuilder();

            if (showDimension)
                builder.Append(HTML.CreateColorString(bot.ColorHeaderHex, dimension.ToString() + ": "));

            builder.Append(String.Format("{0} (Level {1}", whois.Name.ToString(), whois.Stats.Level));
            window.AppendTitle(whois.Name.ToString());

            window.AppendHighlight("Breed: ");
            window.AppendNormal(whois.Stats.Breed);
            window.AppendLineBreak();

            window.AppendHighlight("Gender: ");
            window.AppendNormal(whois.Stats.Gender);
            window.AppendLineBreak();

            window.AppendHighlight("Profession: ");
            window.AppendNormal(whois.Stats.Profession);
            window.AppendLineBreak();

            window.AppendHighlight("Level: ");
            window.AppendNormal(whois.Stats.Level.ToString());
            window.AppendLineBreak();

            if (whois.Stats.DefenderLevel != 0)
            {
                window.AppendHighlight("Defender Rank: ");
                window.AppendNormal(String.Format("{0} ({1})", whois.Stats.DefenderRank, whois.Stats.DefenderLevel));
                window.AppendLineBreak();

                builder.Append(" / Defender Rank " + whois.Stats.DefenderLevel);
            }

            if (dimension == bot.Dimension)
            {
                window.AppendHighlight("Status: ");
                UInt32 userid = bot.GetUserID(whois.Name.Nickname);
                OnlineState state = bot.FriendList.IsOnline(userid);
                switch (state)
                {
                    case OnlineState.Online:
                        window.AppendColorString(RichTextWindow.ColorGreen, "Online");
                        break;
                    case OnlineState.Offline:
                        window.AppendColorString(RichTextWindow.ColorRed, "Offline");
                        Int64 seen = bot.FriendList.Seen(whois.Name.Nickname);
                        if (seen > 0)
                        {
                            window.AppendLineBreak();
                            window.AppendHighlight("Last Seen: ");
                            window.AppendNormal(Format.DateTime(seen, FormatStyle.Compact));
                        }
                        break;
                    default:
                        window.AppendColorString(RichTextWindow.ColorOrange, "Unknown");
                        break;
                }
                window.AppendLineBreak();
            }

            builder.Append(")");
            builder.Append(String.Format(" is a {0} {1}", whois.Stats.Faction, whois.Stats.Profession));

            window.AppendHighlight("Alignment: ");
            window.AppendNormal(whois.Stats.Faction);
            window.AppendLineBreak();

            if (whois.InOrganization)
            {
                window.AppendHighlight("Organization: ");
                window.AppendNormal(whois.Organization.Name);
                window.AppendLineBreak();

                window.AppendHighlight("Organization Rank: ");
                window.AppendNormal(whois.Organization.Rank);
                window.AppendLineBreak();

                builder.AppendFormat(", {0} of {1}", whois.Organization.Rank, whois.Organization.Name);
            }

            window.AppendHighlight("Last Updated: ");
            window.AppendNormal(whois.LastUpdated);
            window.AppendLineBreak(2);

            if (dimension == bot.Dimension)
            {
                window.AppendHeader("Options");
                window.AppendCommand("Add to Friendlist", "/cc addbuddy " + whois.Name.Nickname);
                window.AppendLineBreak();
                window.AppendCommand("Remove from Friendlist", "/cc rembuddy " + whois.Name.Nickname);
                window.AppendLineBreak();
                window.AppendBotCommand("Character History", "history " + whois.Name.Nickname);
                window.AppendLineBreak();
                if (whois.Organization != null && whois.Organization.Name != null)
                {
                    window.AppendBotCommand("Organization Information", "organization " + whois.Name.Nickname);
                    window.AppendLineBreak();
                }
                window.AppendLineBreak();
            }

            window.AppendHeader("Links");
            window.AppendCommand("Official Character Website", "/start " + string.Format(XmlService.CharWebsite, (int)dimension, whois.Name.Nickname));
            window.AppendLineBreak();
            if (whois.Organization != null && whois.Organization.Name != null)
            {
                window.AppendCommand("Official Organization Website", "/start " + string.Format(XmlService.OrgWebsite, (int)dimension, whois.Organization.ID));
                window.AppendLineBreak();
            }

            window.AppendCommand("Rubi-Ka.net Character Website", "/start " + string.Format(XmlService.CharRKNetWebsite, whois.Name.Nickname));
            window.AppendLineBreak();
            if (whois.Organization != null && whois.Organization.Name != null)
            {
                
                window.AppendCommand("Rubi-Ka.net Organization Website", "/start " + string.Format(XmlService.OrgRKNetWebsite, whois.Organization.ID.ToString() ));
                window.AppendLineBreak();
            }

            window.AppendCommand("Auno's Character Website", "/start " + string.Format(XmlService.CharAunoWebsite, (int)dimension, whois.Name.Nickname));
            window.AppendNormal(" (Pre-merge)");
            window.AppendLineBreak();
            if (whois.Organization != null && whois.Organization.Name != null)
            {
                window.AppendCommand("Auno's Organization Website", "/start " + string.Format(XmlService.OrgAunoWebsite, (int)dimension, whois.Organization.Name.Replace(' ', '+')));
                window.AppendNormal(" (Pre-merge)");
                window.AppendLineBreak();
            }

            builder.Append(" »» " + window.ToString("More Information"));
            bot.SendReply(e, builder.ToString());
        }

		private void AppendHistory( ref RichTextWindow window,
                                    HistoryResult_Entry entry )
        {
            window.AppendNormalStart();
            window.AppendString(entry.Date);
            window.AppendHighlight(" | ");
            window.AppendString(entry.Level.ToString("000"));
            window.AppendHighlight(" | ");
            window.AppendString(entry.DefenderLevel.ToString("00"));
            window.AppendHighlight(" | ");
            switch (entry.Faction.ToLower())
            {
            case "clan":
                window.AppendString("Clan       ");
                break;
            case "omni":
                window.AppendString("Omni      ");
                break;
            case "neutral":
                window.AppendString("Neutral   ");
                break;
            default:
                window.AppendString("Unknown ");
                break;
            }
            window.AppendHighlight(" | ");
            if ( !string.IsNullOrEmpty(entry.Organization) )
            {
                window.AppendString(entry.Rank);
                window.AppendString(" of ");
                window.AppendString(entry.Organization);
            }
            window.AppendColorEnd();
            window.AppendLineBreak();            
        }

        private void OnHistoryCommand(BotShell bot, CommandArgs e)
        {
            if (bot.GetUserID(e.Args[0]) < 100)
            {
                bot.SendReply(e, "No such user: " + HTML.CreateColorString(bot.ColorHeaderHex, e.Args[0]));
                return;
            }

            bot.SendReply(e, Format.UppercaseFirst(e.Args[0]) + "'s History »» Gathering Data...");

            HistoryResult history = XmlService.GetHistory(e.Args[0].ToLower(), bot.Dimension);
            if (history == null || history.Items == null)
            {
                bot.SendReply(e, "Unable to gather information on that user " + this.TimeoutError);
                return;
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Player History");
            window.AppendHighlight("Character: ");
            window.AppendNormal(Format.UppercaseFirst(e.Args[0]));
            window.AppendLineBreak();
            window.AppendHighlight("Entries: ");
            window.AppendNormal(history.Items.Length.ToString());
            window.AppendLineBreak();
            window.AppendHighlight("URL: ");
            window.AppendNormal(string.Format(XmlService.RKNetXML, Format.UppercaseFirst(e.Args[0])));
            window.AppendLineBreak(2);

            window.AppendHighlightStart();
            window.AppendString("Date             ");
            window.AppendString("LVL   ");
            window.AppendString("DR   ");
            window.AppendColorString("000000", "'");
            window.AppendString("Faction   ");
            window.AppendString("Organization");
            window.AppendColorEnd();
            window.AppendLineBreak();

            foreach (HistoryResult_Entry entry in history.Items)
                AppendHistory( ref window, entry );

            if ( this._ancient_history )
            {

                Server RK1 = new Server("Atlantean");
                RK1._id = "1";
                Server RK2 = new Server("Rimor");
                RK2._id = "2";
                HistoryResult history1 = XML.GetHistory(e.Args[0].ToLower(),
                                                        RK1 );
                HistoryResult history2 = XML.GetHistory(e.Args[0].ToLower(),
                                                        RK2 );

                int dhistory1 = -1;
                int dhistory2 = -1;
                if ( history1 != null )
                    dhistory1 = history.Items[history.Items.Length-1].Level -
                        history1.Items[0].Level;

                if ( history2 != null )
                    dhistory2 = history.Items[history.Items.Length-1].Level -
                        history2.Items[0].Level;

                if ( ( dhistory1 >= 0 ) &&
                     ( ( dhistory1 < dhistory2 ) || ( dhistory2 < 0 ) ) )
                    foreach (HistoryResult_Entry entry in history1.Items)
                        AppendHistory( ref window, entry );
                else if ( ( dhistory2 >= 0 ) &&
                          ( ( dhistory2 < dhistory1 ) || ( dhistory1 < 0 ) ) )
                    foreach (HistoryResult_Entry entry in history2.Items)
                        AppendHistory( ref window, entry );

            }

            bot.SendReply(e, Format.UppercaseFirst(e.Args[0]) + "'s History »» ", window);
        }

        private void OnOrganizationCommand(BotShell bot, CommandArgs e)
        {
            if (bot.GetUserID(e.Args[0]) < 100)
            {
                bot.SendReply(e, "No such user: " + HTML.CreateColorString(bot.ColorHeaderHex, e.Args[0]));
                return;
            }

            bot.SendReply(e, "Organization »» Gathering Data...");

            WhoisResult whoisResult = XML.GetWhois(e.Args[0], bot.Dimension);
            if (whoisResult != null && whoisResult.Organization != null)
            {
                OrganizationResult organization = XML.GetOrganization(whoisResult.Organization.ID, bot.Dimension);
                if ( organization != null && organization.Members != null )
                {
                    RichTextWindow window = new RichTextWindow(bot);
                    RichTextWindow membersWindow = new RichTextWindow(bot);

                    window.AppendTitle(organization.Name);

                    window.AppendHighlight("Leader: ");
                    window.AppendNormal(organization.Leader.Nickname);
                    window.AppendLineBreak();

                    window.AppendHighlight("Alignment: ");
                    window.AppendNormal(organization.Faction);
                    window.AppendLineBreak();

                    window.AppendHighlight("Members: ");
                    window.AppendNormal(organization.Members.Items.Length.ToString());
                    window.AppendLineBreak();

                    SortedDictionary<string, int> profs = new SortedDictionary<string, int>();
                    SortedDictionary<string, int> breeds = new SortedDictionary<string, int>();
                    SortedDictionary<string, int> genders = new SortedDictionary<string, int>();

                    membersWindow.AppendHeader("Members");

                    foreach (OrganizationMember member in organization.Members.Items)
                    {
                        if (!profs.ContainsKey(member.Profession))
                            profs.Add(member.Profession, 0);
                        profs[member.Profession]++;

                        if (!breeds.ContainsKey(member.Breed))
                            breeds.Add(member.Breed, 0);
                        breeds[member.Breed]++;

                        if (!genders.ContainsKey(member.Gender))
                            genders.Add(member.Gender, 0);
                        genders[member.Gender]++;

                        membersWindow.AppendHighlight(member.Nickname);
                        membersWindow.AppendNormal(string.Format(" {0} (L {1} / DR {2}) {3} {4}", member.Rank, member.Level, member.DefenderLevel, member.Breed, member.Profession));
                        membersWindow.AppendLineBreak();
                    }

                    string stats;
                    char[] trimchars = new char[] { ' ', ',' };

                    window.AppendHighlight("Genders: ");
                    stats = string.Empty;
                    foreach (KeyValuePair<string, int> kvp in genders)
                    {
                        stats += kvp.Value + " " + kvp.Key + ", ";
                    }
                    window.AppendNormal(stats.Trim(trimchars));
                    window.AppendLineBreak();

                    window.AppendHighlight("Breeds: ");
                    stats = string.Empty;
                    foreach (KeyValuePair<string, int> kvp in breeds)
                    {
                        stats += kvp.Value + " " + kvp.Key + ", ";
                    }
                    window.AppendNormal(stats.Trim(trimchars));
                    window.AppendLineBreak();

                    window.AppendHighlight("Professions: ");
                    stats = string.Empty;
                    foreach (KeyValuePair<string, int> kvp in profs)
                    {
                        stats += kvp.Value + " " + kvp.Key + ", ";
                    }
                    window.AppendNormal(stats.Trim(trimchars));
                    window.AppendLineBreak();

                    window.AppendHighlight("ID: ");
                    window.AppendNormal(organization.ID.ToString());
                    window.AppendLineBreak();

                    window.AppendHighlight("Last Updated: ");
                    window.AppendNormal(organization.LastUpdated);
                    window.AppendLineBreak(2);

                    window.AppendRawString(membersWindow.Text);

                    bot.SendReply(e, organization.Name + " »» ", window);
                    return;
                }
            }
            bot.SendReply(e, "Unable to gather information on that organization " + this.TimeoutError);
        }

        private void OnServerCommand(BotShell bot, CommandArgs e)
        {
            bot.SendReply(e, "Server Status »» Gathering Data...");

            ServerStatusResult server = XML.GetServerStatus();
            if (server == null || server.Dimensions == null)
            {
                bot.SendReply(e, "Unable to gather server information " + this.TimeoutError);
                return;
            }
            ServerStatusResult_Dimension dimension = server.GetDimension(bot.Dimension);
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Server Information");

            window.AppendHighlight("Server Manager: ");
            if (dimension.ServerManager.Online)
                window.AppendColorString(RichTextWindow.ColorGreen, "Online");
            else
                window.AppendColorString(RichTextWindow.ColorRed, "Offline");
            window.AppendLineBreak();

            window.AppendHighlight("Client Manager: ");
            if (dimension.ClientManager.Online)
                window.AppendColorString(RichTextWindow.ColorGreen, "Online");
            else
                window.AppendColorString(RichTextWindow.ColorRed, "Offline");
            window.AppendLineBreak();

            window.AppendHighlight("Chat Server: ");
            if (dimension.ChatServer.Online)
                window.AppendColorString(RichTextWindow.ColorGreen, "Online");
            else
                window.AppendColorString(RichTextWindow.ColorRed, "Offline");
            window.AppendLineBreak(2);

            foreach (ServerStatusResult_Playfield pf in dimension.Playfields)
            {
                bool skip = false;
                foreach (string arg in e.Args)
                    if (!pf.Name.ToLower().Contains(arg.ToLower()))
                        skip = true;

                if (skip)
                    continue;

                switch (pf.Status)
                {
                    case PlayfieldStatus.Online:
                        window.AppendImage("GFX_GUI_FRIENDLIST_STATUS_GREEN");
                        break;
                    default:
                        window.AppendImage("GFX_GUI_FRIENDLIST_STATUS_RED");
                        break;
                }
                window.AppendNormalStart();
                window.AppendString(" ");
                window.AppendColorStart(RichTextWindow.ColorGreen);
                double players = 0;
                while (players <= pf.Players && players <= 8 && pf.Players != 0)
                {
                    players += 0.5;
                    window.AppendString("l");
                }
                window.AppendColorEnd();
                while (players <= 8)
                {
                    players += 0.5;
                    window.AppendString("l");
                }
                window.AppendString(" ");
                window.AppendColorEnd();

                window.AppendHighlight(pf.Name);
                window.AppendNormal(string.Format(" (ID: {0} Players: {1}%)", pf.ID, pf.Players));
                window.AppendLineBreak();
            }
            bot.SendReply(e, "Server Status »» ", window);
        }

        private void OnIsCommand(BotShell bot, CommandArgs e)
        {
            foreach (string username in e.Args)
            {
                UInt32 userid = bot.GetUserID(username);
                OnlineState state = bot.FriendList.IsOnline(userid);
                switch (state)
                {
                    case OnlineState.Timeout:
                        bot.SendReply(e, "Request timed out. Please try again later");
                        break;

                    case OnlineState.Unknown:
                        bot.SendReply(e, "No such user: " + HTML.CreateColorString(bot.ColorHeaderHex, username));
                        break;

                    case OnlineState.Online:
                        string append1 = HTML.CreateColorString(RichTextWindow.ColorGreen, "Online");
                        bot.SendReply(e, String.Format("{0} is currently {1}", Format.UppercaseFirst(username), append1));
                        break;

                    case OnlineState.Offline:
                        string main = bot.Users.GetMain(username);
                        string[] alts = bot.Users.GetAlts(main);
                        StringBuilder append2 = new StringBuilder(HTML.CreateColorString(RichTextWindow.ColorRed, "Offline"));
                        if ((main.Length > 0) || (alts.Length > 0))
                        {
                            userid = bot.GetUserID(main);
                            state = bot.FriendList.IsOnline(userid);
                            Int64 lastseen = bot.FriendList.Seen(main);
                            if (state == OnlineState.Online)
                            {
                                append2.Append(" but his/her main " + Format.UppercaseFirst(main));
                                append2.Append(" is " + HTML.CreateColorString(RichTextWindow.ColorGreen, "Online"));
                                bot.SendReply(e, String.Format("{0} is currently {1}", Format.UppercaseFirst(username), append2.ToString()));
                            }
                            else
                            {
                                StringBuilder append3 = new StringBuilder();
                                string seenalt = main;
                                Int64 seen = 0;
                                foreach (string alt in alts)
                                {
                                    userid = bot.GetUserID(alt);
                                    state = bot.FriendList.IsOnline(userid);
                                    seen = bot.FriendList.Seen(alt);
                                    if (seen > lastseen)
                                    {
                                        lastseen = seen;
                                        seenalt = alt;
                                    }
                                    if (state == OnlineState.Online)
                                    {
                                        append3.Append(append2);
                                        append3.Append(" but his/her alt " + Format.UppercaseFirst(alt));
                                        append3.Append(" is " + HTML.CreateColorString(RichTextWindow.ColorGreen, "Online"));
                                        break;
                                    }
                                }
                                if (append3.Length == 0)          // Nobody online
                                {
                                     if (lastseen > 1)          // but has been Seen
                                     {
                                         append2.Append(" but was last seen online on " + seenalt + " at ");
                                         append2.Append(HTML.CreateColorString(bot.ColorHeaderHex, Format.DateTime(lastseen, FormatStyle.Large) + " GMT"));
                                     }
                                     if (alts.Length > 0)   // has Alts
                                          bot.SendReply(e, String.Format("{0} and his/her alts are currently {1}", Format.UppercaseFirst(username), append2.ToString()));
                                     else
                                          bot.SendReply(e, String.Format("{0} is currently {1}", Format.UppercaseFirst(username), append2.ToString()));
                                }
                                else
                                {
                                     bot.SendReply(e, String.Format("{0} is currently {1}", Format.UppercaseFirst(username), append3.ToString()));
                                }
                            }
                        }
                        else
                        {
                            Int64 seen = bot.FriendList.Seen(username);
                            if (seen > 1)
                                append2.Append(" and was last seen online at " + HTML.CreateColorString(bot.ColorHeaderHex, Format.DateTime(seen, FormatStyle.Large) + " GMT"));
                            bot.SendReply(e, String.Format("{0} is currently {1}", Format.UppercaseFirst(username), append2.ToString()));
                        }
                        break;
                }
            }
        }

        private void OnXmlCacheCommand(BotShell bot, CommandArgs e)
        {
            if ( e.Args.Length == 0 )
            {
                RichTextWindow window = new RichTextWindow(bot);

                window.AppendTitle("XML Cache");
                window.AppendLineBreak( );

                window.AppendNormal("Your bot keeps a cache of player and organization information to speed up the lookup of information.  If the cache becomes corrupted, then many commands such as !whois, !history, and !org will produce errors.  If you are having problems with those commands, you can clear your cache to try to fix your problems.");
                window.AppendLineBreak( 2 );

                window.AppendNormal( "[" );
                window.AppendBotCommand( "Clear the XML Cache now!", "xmlcache clear");
                window.AppendNormal( "]" );

                bot.SendReply(e, "XML Cache »» ", window);                
            }
            else if ( e.Args[0].ToLower() == "clear" )
            {
                Directory.Delete( XML.XmlCachePath, true );
            }
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "whois":
                return "Gathers and displays information about a user. Information like Level, Breed and Profession\n" +
                    "Usage: /tell " + bot.Character + " whois [username]";

            case "whoisall":
                return "Gathers and displays information about users matching the specified username on all dimensions.\n" +
                    "Usage: /tell " + bot.Character + " whois [username]";

            case "history":
                return "Gathers and displays the history of a user.\n" +
                    "Usage: /tell " + bot.Character + " history [username]";

            case "organization":
                return "Displays information about the organization of the specified user.\n" +
                    "Usage: /tell " + bot.Character + " organization [username]";

            case "server":
                return "Displays the current status of this dimension's server.\n" +
                    "Usage: /tell " + bot.Character + " server";

            case "is":
                return "Displays the current online status of the specified user.\n" +
                    "Usage: /tell " + bot.Character + " is [username]";

            case "xmlcache":
                return "Clears the data stored in the bot's XML cache.\n" +
                    "Usage: /tell " + bot.Character + " xmlcache";

            }
            return null;
        }

        public static HistoryResult GetHistory(string character, Server server)
        {
            if ( server == "Test")
                return null;

            character = character.ToLower();

            string response = HTML.GetHtml(String.Format(XmlService.RKNetXML,
                                                         character));
            MemoryStream stream = null;
            RKNHistoryArray History_Result = null;

            // Strip schema from results
            string pattern = "<ArrayOfHistory.*>";
            string replacement = "<ArrayOfHistory>";
            Regex regex = new Regex(pattern);
            try
            {
                response = regex.Replace(response, replacement);
            }
            catch
            {
                return null;
            }

            if ( !string.IsNullOrEmpty(response) )
            {

                try
                {
                    stream = new MemoryStream(Encoding.UTF8.GetBytes(response));
                    XmlSerializer serializer = new XmlSerializer(typeof(RKNHistoryArray));
                    History_Result = (RKNHistoryArray)serializer.Deserialize(stream);
                    stream.Close();
                }
                catch
                {
                    return null;
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }

                // Sort results by timestamp (descending)
                Array.Sort( History_Result.history,
                           delegate(RKNHistory x, RKNHistory y) { return y.time.CompareTo(x.time); });

                HistoryResult HistoryResultAdapter = new HistoryResult();
                HistoryResultAdapter.Items = new HistoryResult_Entry[History_Result.history.Length];

                // Copy results by timestamp (ascending)
                for ( int i=History_Result.history.Length-1; i>=0; i-- )
                {
                    HistoryResultAdapter.Items[i] = new HistoryResult_Entry();
                    HistoryResultAdapter.Items[i].Date =
                        History_Result.history[i].time;
                    // strip time from datestamp
                    HistoryResultAdapter.Items[i].Date =
                        HistoryResultAdapter.Items[i].Date.Remove(
                            HistoryResultAdapter.Items[i].Date.IndexOf('T'));
                    HistoryResultAdapter.Items[i]._level =
                        History_Result.history[i].level;
                    HistoryResultAdapter.Items[i]._defenderLevel =
                        History_Result.history[i].ailevel;
                    if ( !string.IsNullOrEmpty(History_Result.history[i].organization) )
                        HistoryResultAdapter.Items[i].Organization =
                            History_Result.history[i].organization;
                    else
                        HistoryResultAdapter.Items[i].Organization = "";
                    if ( !string.IsNullOrEmpty(History_Result.history[i].rank) )
                        HistoryResultAdapter.Items[i].Rank =
                            History_Result.history[i].rank;
                    else
                        HistoryResultAdapter.Items[i].Rank = "";
                    if ( !string.IsNullOrEmpty(History_Result.history[i].faction) )
                        HistoryResultAdapter.Items[i].Faction =
                            History_Result.history[i].faction;
                    else
                    {
                        
                        if ( !string.IsNullOrEmpty(HistoryResultAdapter.Items[i].Organization) )
                        {
                            // populate missing faction from organization
                            if ( ( i < History_Result.history.Length-1 ) &&
                                 ( HistoryResultAdapter.Items[i+1].Organization == HistoryResultAdapter.Items[i].Organization ) )
                                // copy from previous look-up
                                HistoryResultAdapter.Items[i].Faction =
                                    HistoryResultAdapter.Items[i+1].Faction;
                            else
                            {
                                // look up org faction
                                string FoundOrg = PluginShared.GetUrl(String.Format(XmlService.RKNetFindOrg, HistoryResultAdapter.Items[i].Organization.Replace(" ","+")));
                                string SuccessStr = "class=\"success\">";
                                if ( FoundOrg.Contains(SuccessStr) )
                                {
                                    int leading = FoundOrg.IndexOf(SuccessStr)+SuccessStr.Length;
                                    leading = FoundOrg.IndexOf("</td>",leading)+5;
                                    leading = FoundOrg.IndexOf(">", leading)+1;
                                    int trailing = FoundOrg.IndexOf("</td>", leading);
                                    HistoryResultAdapter.Items[i].Faction = FoundOrg.Remove(trailing).Remove(0,leading).Trim();
                                }
                                else
                                    // unable to find org
                                    HistoryResultAdapter.Items[i].Faction = "";
                            }
                        }
                        else
                            HistoryResultAdapter.Items[i].Faction = "";
                    }
                }

                return HistoryResultAdapter;
            }
            return null;
        }

    }

    [XmlRoot("ArrayOfHistory")]
    public class RKNHistoryArray
    {
        [XmlElement("History")]
        public RKNHistory[] history;
    }

    [XmlRoot("History")]
    public class RKNHistory
    {
        [XmlElement("Time")]
        public string time;
        [XmlElement("Level")]
        public string level;        
        [XmlElement("AILevel")]
        public string ailevel;        
        [XmlElement("Organization")]
        public string organization;        
        [XmlElement("Rank")]
        public string rank;
        [XmlElement("Faction")]
        public string faction;        
    }

}
