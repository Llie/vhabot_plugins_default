using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Data;
using System.Globalization;
using AoLib.Utils;

namespace VhaBot.Plugins
{
    public class Tasks : PluginBase
    {

        private Config _database;
        private CultureInfo _cultureInfo = new CultureInfo("en-GB");
        private TextInfo _ti = new CultureInfo("en-US", false).TextInfo;
        private bool _logon_tasks = false;
        private BotShell _bot;
        private Timer _welcome;

        public Tasks()
        {
            this.Name = "Tasks Manager";
            this.InternalName = "vhTasks";
            this.Author = "Vhab / Veremit / Llie";
            this.DefaultState = PluginState.Installed;
            this.Version = 105;
            this.Description = "Provides a tasks management system";
            this.Commands = new Command[] {
                new Command("tasks", true, UserLevel.Member),
                new Command("tasks add", true, UserLevel.Leader),
                new Command("tasks remove", true, UserLevel.Leader),
                new Command("tasks assign", true, UserLevel.Leader),
                new Command("tasks take", true, UserLevel.Member),
                new Command("tasks clear", true, UserLevel.Admin)
            };
        }

        private void ConfigurationChangedEvent(BotShell bot, ConfigurationChangedArgs e)
        {
            if (e.Section != this.InternalName) return;
            this.LoadConfiguration(bot);
        }

        private void LoadConfiguration(BotShell bot)
        {
            this._logon_tasks = bot.Configuration.GetBoolean(this.InternalName, "logon_tasks", this._logon_tasks );

            if ( this._logon_tasks )
            {
                bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(UserJoinChannelEvent);
                bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
            }
            else
            {
                bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
                bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            }
        }

        private void UserJoinChannelEvent(BotShell bot, UserJoinChannelArgs e)
        {
            SendTasks( bot, e.Sender );
        }

        private void UserLogonEvent(BotShell bot, UserLogonArgs e)
        {
            if (e.First) return; 
            SendTasks( bot, e.Sender );
        }

        public override void OnLoad(BotShell bot)
        {
            this._database = new Config(bot.ID, this.InternalName);
            this._database.ExecuteNonQuery("CREATE TABLE IF NOT EXISTS tasks (task_id integer PRIMARY KEY AUTOINCREMENT, task_name varchar (14), task_date integer, task_text varchar (500))");

            bot.Events.ConfigurationChangedEvent +=new ConfigurationChangedHandler(ConfigurationChangedEvent);

            bot.Configuration.Register(ConfigType.Boolean, this.InternalName, "logon_tasks", "Send task reminders on logon?", this._logon_tasks );

            this._bot = bot;

            this._welcome = new Timer(7000);
            this._welcome.Elapsed += new ElapsedEventHandler(WelcomeTimerElapsed);
            this._welcome.AutoReset = false;
            this._welcome.Enabled = true;

        }

        public void WelcomeTimerElapsed(object sender, ElapsedEventArgs e)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
            {
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= TasksWindow;
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates += TasksWindow;
                this._bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
                this._bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            }
        }

        public void WelcomeUnload( BotShell bot )
        {
            this._bot.Events.UserJoinChannelEvent += new UserJoinChannelHandler(UserJoinChannelEvent);
            this._bot.Events.UserLogonEvent += new UserLogonHandler(UserLogonEvent);
        }

        public override void OnUnload(BotShell bot)
        {
            if ( this._bot.Plugins.IsLoaded("vhWelcome") )
                ((vhWelcome)this._bot.Plugins.GetPlugin("vhWelcome")).Delegates -= TasksWindow;
            bot.Events.UserJoinChannelEvent -= new UserJoinChannelHandler(UserJoinChannelEvent);
            bot.Events.UserLogonEvent -= new UserLogonHandler(UserLogonEvent);
            bot.Events.ConfigurationChangedEvent -=new ConfigurationChangedHandler(ConfigurationChangedEvent);
        }

        public override void OnCommand(BotShell bot, CommandArgs e)
        {

            switch (e.Command)
            {

            case "tasks":
                this.OnTasksCommand(bot, e);
                break;

            case "tasks add":
                this.OnTasksAddCommand(bot, e);
                break;

            case "tasks assign":
                this.OnTasksAssignCommand(bot, e);
                break;

            case "tasks take":
                this.OnTasksTakeCommand(bot, e);
                break;

            case "tasks remove":
                this.OnTasksRemoveCommand(bot, e);
                break;

            case "tasks clear":
                this.OnTasksClearCommand(bot, e);
                break;

            }

        }

        private bool ComposeTasksWindow( BotShell bot, ref RichTextWindow window, string sender, string username )
        {

            string message = "Tasks";
            bool found = false;
            Int64 lastPost = 0;
            using ( IDbCommand command =
                    this._database.Connection.CreateCommand())
            {
                if ( ( bot.Users.GetUser(sender) >= UserLevel.Leader ) &&
                     ( username != string.Empty ) &&
                     ( bot.GetUserID(_ti.ToTitleCase(username)) >= 100 ) )
                    command.CommandText = "SELECT [task_id], [task_name] , [task_date], [task_text] FROM [tasks] WHERE task_name = '" + _ti.ToTitleCase(username) + "' OR task_name = '*' ORDER BY [task_date] DESC LIMIT 15";
                else if ( ( bot.Users.GetUser(sender) >= UserLevel.Leader ) &&
                          ( username == string.Empty ) )
                    command.CommandText = "SELECT [task_id], [task_name] , [task_date], [task_text] FROM [tasks] ORDER BY [task_name], [task_date] DESC LIMIT 15";
                else
                    command.CommandText = "SELECT [task_id], [task_name] , [task_date], [task_text] FROM [tasks] WHERE task_name = '" + sender + "' OR task_name = '*' ORDER BY [task_date] DESC LIMIT 15";
                IDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    found = true;
                    if (reader.GetInt64(2) > lastPost)
                        lastPost = reader.GetInt64(2);

                    if (message != reader.GetString(1))
                    {
                        window.AppendLineBreak(true);
                        message = reader.GetString(1);
                        if( message == "*" )
                            message = "Unassigned";
                        
                        window.AppendHighlight(message);
                        window.AppendNormal(" [");
                        window.AppendBotCommand("Remove All", "tasks remove " +
                                                message );
                        window.AppendNormal("]");
                        window.AppendLineBreak(true);

                        if( message == "Unassigned" )
                            message = "*";
                    }

                    window.AppendNormal( "ID: " + reader.GetString(0) + " " );
                    window.AppendHighlight(DateTime(reader.GetInt64(2)).ToLower() + ": ");
                    window.AppendNormal( reader.GetString(3).Trim()+" [");
                    window.AppendBotCommand( "Remove", "tasks remove " +
                                             reader.GetString(0));
                    window.AppendNormal("][");
                    if ( reader.GetString(1) == "*" )
                        window.AppendBotCommand("Take", "tasks take " +
                                                reader.GetString(0));
                    else
                        window.AppendBotCommand("Refuse", "tasks assign * " +
                                                reader.GetString(0));
                    window.AppendNormal("]");
                    window.AppendLineBreak();

                }
                reader.Close();
            }

            if ( bot.Users.GetUser(sender) > UserLevel.Admin )
            {
                window.AppendLineBreak();
                window.AppendNormal("[");
                window.AppendBotCommand("Clear", "tasks clear");
                window.AppendNormal("]");
            }

            return found;
        }

        public void TasksWindow( ref RichTextWindow window, string sender )
        {
            window.AppendHeader( "Tasks");
            window.AppendLineBreak();
            ComposeTasksWindow( this._bot, ref window, sender, string.Empty );
            window.AppendLineBreak();

            window.AppendColorString(window.ColorDetail, "¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
            window.AppendLineBreak();
        }

        private void SendTasks( BotShell bot, string username )
        {
            RichTextWindow window = new RichTextWindow(bot);

            window.AppendTitle("Tasks");
            bool found = ComposeTasksWindow( bot, ref window, username,
                                             string.Empty );

            if ( found )
                bot.SendPrivateMessage( username,
                                        "You have assigned tasks »» " +
                                        window.ToString() );
            
        }

        private void OnTasksCommand(BotShell bot, CommandArgs e)
        {
            RichTextWindow window = new RichTextWindow(bot);

            if ( ( bot.Users.GetUser(e.Sender) < UserLevel.Leader ) &&
                 ( e.Args.Length != 0 ) )
            {
                bot.SendReply(e, "Sorry, only leaders can view other people's tasks.");
                return;
            }
            else if ( ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) &&
                      ( e.Args.Length == 1 ) &&
                      ( bot.GetUserID(_ti.ToTitleCase(e.Args[0])) < 100 ) )
            {
                bot.SendReply(e, "No such user: " +
                              _ti.ToTitleCase(e.Args[0]));
                return;
            }

            string username = string.Empty;
            if (  e.Args.Length == 1 )
                username = e.Args[0];
            window.AppendTitle("Tasks");
            bool found = ComposeTasksWindow( bot, ref window, e.Sender,
                                             username );

            if (!found)
                bot.SendReply(e, "No tasks assigned");
            else if ( ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) &&
                      ( e.Args.Length == 1 ) )
                bot.SendReply(e, _ti.ToTitleCase(e.Args[0]) +
                              "'s Tasks »» ", window);
            else if ( ( bot.Users.GetUser(e.Sender) >= UserLevel.Leader ) &&
                      ( e.Args.Length == 0 ) )
                bot.SendReply(e, "All Tasks »» ", window);
            else
                bot.SendReply(e, e.Sender + "'s Tasks »» ", window);

        }

        private void OnTasksAddCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length < 2)
            {
                bot.SendReply(e, "Correct Usage: tasks add [username] [task]");
                return;
            }
            string username = Format.UppercaseFirst(e.Args[0]);
            if ( (bot.GetUserID(username) < 100) && ( username != "*" ) )
            {
                bot.SendReply(e, "No such user: " +
                              HTML.CreateColorString( bot.ColorHeaderHex,
                                                      username));
                return;
            }
            string task = e.Words[1];

            this._database.ExecuteNonQuery(string.Format("INSERT INTO tasks (task_name, task_date, task_text) VALUES ('{0}', '{1}', '{2}')", username, TimeStamp.Now, Config.EscapeString(task)));

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Tasks");
            ComposeTasksWindow( bot, ref window, e.Sender, string.Empty );
            bot.SendReply(e, "The following task: " +
                          HTML.CreateColorString(bot.ColorHeaderHex, task) +
                          " has been added »» ", window  );

            if ( username != "*" )
            {

                bot.SendOrganizationMessage(bot.ColorHighlight + HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) + " has assigned " + HTML.CreateColorString(bot.ColorHeaderHex, username) + " the following task: " + HTML.CreateColorString(bot.ColorHeaderHex, task));

                bot.SendPrivateChannelMessage(bot.ColorHighlight + HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) + " has assigned " + HTML.CreateColorString(bot.ColorHeaderHex, username) + " the following task: " + HTML.CreateColorString(bot.ColorHeaderHex, task));

                bot.SendPrivateMessage(username, bot.ColorHighlight + HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) + " has assigned you the following task: " + HTML.CreateColorString(bot.ColorHeaderHex, task));
            }
            else
            {

                bot.SendOrganizationMessage(bot.ColorHighlight + HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) + " has posted the following task: " + HTML.CreateColorString(bot.ColorHeaderHex, task));

                bot.SendPrivateChannelMessage(bot.ColorHighlight + HTML.CreateColorString(bot.ColorHeaderHex, e.Sender) + " has posted the following task: " + HTML.CreateColorString(bot.ColorHeaderHex, task));

            }
        }

        private void OnTasksRemoveCommand(BotShell bot, CommandArgs e)
        {
            double d;
            if (e.Args.Length == 0)
            {
                bot.SendReply(e, "Correct Usage: tasks remove [id|username]");
                return;
            }

            try
            {
                if (double.TryParse(e.Args[0],
                                    System.Globalization.NumberStyles.Integer,
                                    _cultureInfo, out d) == true)
                {
                    this._database.ExecuteNonQuery(
                        "DELETE FROM [tasks] WHERE [task_id] = " + e.Args[0]);
                    bot.SendReply(e, "Tasks ID: " + e.Args[0] + " removed.");
                }
                else if ( bot.GetUserID(_ti.ToTitleCase(e.Args[0])) >= 100 )
                {
                    this._database.ExecuteNonQuery(
                        "DELETE FROM [tasks] WHERE [task_name] = '" +
                        _ti.ToTitleCase(e.Args[0]) + "'" );
                    bot.SendReply(e, "All tasks assigned to " + e.Args[0] + " have been removed.");
                }
                else
                {
                    bot.SendReply(e, "Invalid Task ID or no such user.");
                    return;
                }
                    
            }
            catch
            {
                bot.SendReply(e, "An error occured during task removal.");
                return;
            }

        }

        private void OnTasksAssignCommand(BotShell bot, CommandArgs e)
        {

            double d;
            if (e.Args.Length != 2)
            {
                bot.SendReply(e, "Correct Usage: tasks assign [username] [id]");
                return;
            }

            try
            {
                if (
                    ( ( bot.GetUserID(_ti.ToTitleCase(e.Args[0])) >= 100 ) ||
                      ( e.Args[0] == "*" ) ) &&
                    (double.TryParse(e.Args[1],
                                     System.Globalization.NumberStyles.Integer,
                                     _cultureInfo, out d) == true) )
                {
                    this._database.ExecuteNonQuery(
                        "UPDATE [tasks] SET [task_name] = '" +
                        _ti.ToTitleCase(e.Args[0]) + "' WHERE [task_id] = "
                        + e.Args[1]);
                }
                else
                {
                    bot.SendReply(e, "Invalid Task ID or user does not exist.");
                    return;
                }
                    
            }
            catch
            {
                bot.SendReply(e, "An error occured during task assignment.");
            }

            
            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Tasks");
            ComposeTasksWindow( bot, ref window, e.Sender, string.Empty );

            if ( e.Args[0] != "*" )
                bot.SendReply(e, "Tasks ID: " + e.Args[1] + " is now assigned to " + e.Args[0] + " »» ", window);
            else
                bot.SendReply(e, "Tasks ID: " + e.Args[1] + " is now unassigned. »» ", window );

        }

        private void OnTasksTakeCommand(BotShell bot, CommandArgs e)
        {

            double d;
            if (e.Args.Length == 0)
            {
                bot.SendReply(e, "Correct Usage: tasks take [id]");
                return;
            }

            try
            {
                if (double.TryParse(e.Args[0],
                                    System.Globalization.NumberStyles.Integer,
                                    _cultureInfo, out d) == true)
                {
                    this._database.ExecuteNonQuery(
                        "UPDATE [tasks] SET [task_name] = '" + e.Sender + "' WHERE [task_id] = " + e.Args[0]);
                }
                else
                {
                    bot.SendReply(e, "Invalid Task ID.");
                    return;
                }
                    
            }
            catch
            {
                bot.SendReply(e, "An error occured during task removal.");
            }

            RichTextWindow window = new RichTextWindow(bot);
            window.AppendTitle("Tasks");
            ComposeTasksWindow( bot, ref window, e.Sender, string.Empty );

            bot.SendReply(e, "Tasks ID: " + e.Args[0] + " is now assigned to you»» ", window);

        }

        private void OnTasksClearCommand(BotShell bot, CommandArgs e)
        {
            if (e.Args.Length == 1 && e.Args[0] == "confirm")
            {
                this._database.ExecuteNonQuery("DELETE FROM tasks");
                bot.SendReply(e, "All tasks have been cleared");
                return;
            }
            if (bot.Users.Authorized(e.Sender, UserLevel.Admin) && e.Type == CommandType.Tell)
            {
                RichTextWindow window = new RichTextWindow(bot);
                window.AppendTitle("Clear Tasks Confirmation");

                window.AppendLineBreak(true);
                window.AppendNormal("This command will permanently remove all tasks. If you wish to continue? [");
                window.AppendBotCommand("Confirm", "tasks clear confirm" );
                window.AppendNormal("]");
                bot.SendReply(e, "Clear Tasks »» ", window );
                return;
            }
        }

        private static string DateTime(long timestamp)
        {
            return DateTime(TimeStamp.ToDateTime(timestamp));
        }

        private static string DateTime(DateTime date)
        {
            DateTimeFormatInfo dtfi =
                new CultureInfo("en-US", false).DateTimeFormat;
            return date.ToString("dd/MM h:mmtt", dtfi);
        }

        public override string OnHelp(BotShell bot, string command)
        {
            switch (command)
            {

            case "tasks":
                return "Displays your assigned tasks. (Or all tasks if your rankis leader or higher.)\n" +
                    "Usage: /tell " + bot.Character + " tasks";

            case "tasks add":
                return "Allows you to assign a task to [username].\n" +
                    "Usage: /tell " + bot.Character + " tasks add [username] [task]";

            case "tasks assign":
                return "Allows you to assign or reassign a task to [username].\n" +
                    "Usage: /tell " + bot.Character + " tasks assign [username] [id]";

            case "tasks take":
                return "Allows you to take an task that is unassigned or assigned to someone else.\n" +
                    "Usage: /tell " + bot.Character + " tasks take [id]";

            case "tasks remove":
                return "Allows you to remove all tasks assigned to a user or one specific task.\n" +
                    "Usage: /tell " + bot.Character + " tasks remove [username|id]";

            case "tasks clear":
                return "Clears all assigned tasks.\n" +
                    "Usage: /tell " + bot.Character + " tasks clear";

            }
            return null;
        }
    }
}
