using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Timers;
using AoLib.Utils;

namespace VhaBot.Plugins
{

    [XmlRoot("playfields")]
    public class Playfields
    {
        [XmlElement("playfield")]
        public Playfield[] List;
    }

    [XmlRoot("playfield")]
    public class Playfield
    {
        [XmlElement("name")]
        public string Name;
        [XmlElement("pfid")]
        public string PFID;
    }

    [XmlRoot("timezones")]
    public class TimeZones
    {
        [XmlElement("timezone")]
        public TimeZone[] List;

        public static DateTime ConvertTime( DateTime InputTime,
                                            TimeZone FromZone, TimeZone ToZone)
        {
            return TimeZones.ConvertTime( InputTime, FromZone, ToZone, false );
        }

        public static DateTime ConvertTime( DateTime InputTime,
                                            TimeZone FromZone, TimeZone ToZone,
                                            bool DaylightTime )
        {
            DateTime OutputTime = InputTime;
            if ( DaylightTime )
                OutputTime = OutputTime.AddHours( -1 );
            OutputTime = OutputTime.AddHours( -FromZone.UtcOffsetHours );
            if ( ToZone.StandardName != "GMT" )
                OutputTime = OutputTime.AddHours( ToZone.UtcOffsetHours );
            return OutputTime;
        }

        public TimeZone Utc
        {
            get
            {
                foreach ( TimeZone tz in List )
                {
                    if( tz.DisplayName == "Europe/London" )
                        return tz;
                }
                return List[0];
            }
        }
                              
    }

    [XmlRoot("timezone")]
    public class TimeZone
    {
        [XmlElement("displayname")]
        public string DisplayName;
        [XmlElement("standardname")]
        public string StandardName;
        [XmlElement("daylightname")]
        public string DaylightName;
        [XmlElement("baseutcoffset")]
        public string BaseUtcOffset;

        public float UtcOffsetHours
        {
            get
            {
                try
                {
                    float hrs = Convert.ToSingle(this.BaseUtcOffset.Substring(0, this.BaseUtcOffset.IndexOf(':') ) );
                    float min = Convert.ToSingle( this.BaseUtcOffset.Substring( this.BaseUtcOffset.IndexOf(':') + 1, 2 ) ) * Math.Sign( hrs );
                    return hrs + min/60;
                }
                catch
                {
                    return 0;
                }
            }
        }
    }

    public delegate void CascadingTimerEvent( double time_remaining,
                                              string name );

    public class CascadingTimer : System.Timers.Timer
    {

        private double _time_remaining;
        private double _last_interval;
        private DateTime _timer_started;
        private string _name;
        private double[] _intervals;
        private bool _running;

        public event CascadingTimerEvent IntermediateEvent;
        public event CascadingTimerEvent FinalEvent;

        private static double[] _default_intervals =
            new double[] { 21600000, // 6 hours
                           3600000,  // 1 hour
                           900000,   // 15 minutes
                           300000,   // 5 minutes
                           60000,    // 1 minute
                           30000,    // 30 seconds
                           10000,    // 10 seconds
                           1000 };   // 1 second 

        public CascadingTimer( )
        {

            this._running = false;
            this.AutoReset = false;
            this.Elapsed += new ElapsedEventHandler(CascadingTimerElapsed);

        }

        public void StartCascade( int hours, int minutes, int seconds,
                                  string name )
        {
            this._time_remaining =  ( hours * 60 * 60 * 1000 ) +
                ( minutes * 60 * 1000 ) + ( seconds * 1000);

            this._name = name;
            this._intervals = CascadingTimer._default_intervals;

            CascadingTimerElapsed( null, null );
        }

        public void CancelCascade( )
        {
            this._time_remaining = 0;

            this._name = string.Empty;

            this._running = false;

            this.Stop();
        }

        private void CascadingTimerElapsed(object source, ElapsedEventArgs e)
        {
            int counter = 0;
            while ( (this._time_remaining / this._intervals[counter] ) < 2 )
                counter ++;

            double increment = 0;
            long n_units = Convert.ToInt64(this._time_remaining) /
                Convert.ToInt64(this._intervals[counter]);

            increment = this._time_remaining -
                (n_units * this._intervals[counter]);
            if ( increment == 0 )
                increment = this._intervals[counter];

            this._time_remaining -= increment;

            if ( this._time_remaining > 1000 )
            {

                if( source != null )
                    IntermediateEvent( this._time_remaining, this._name );

                this._last_interval = increment;
                this.Interval = increment;
                this._timer_started = DateTime.Now;
                this._running = true;
                this.Enabled = true;
            }
            else
            {
                this._running = false;
                FinalEvent( this._time_remaining, this._name );
            }

        }

        public bool IsRunning( )
        {
            return this._running;
        }

        public string Name( )
        {
            return this._name;
        }

        public string TimeRemaining( )
        {
            return CascadingTimer.TimeRemaining( this._time_remaining,
                                                 this._name );
        }

        public string ElapsedTimeRemaining( )
        {
            TimeSpan elapsed = DateTime.Now - this._timer_started;
            return CascadingTimer.TimeRemaining( this._time_remaining +
                (this._last_interval-elapsed.TotalMilliseconds),
                                                     this._name );
        }

        public static string TimeRemaining( double total_remaining,
                                            string name )
        {
            if ( total_remaining >= 3600000 )
            {
                long hrs = Convert.ToInt64(total_remaining / 3600000);
                return hrs.ToString() + " hours remaining until " + name;
            }
            else if ( total_remaining >= 60000 )
            {
                long min = Convert.ToInt64(total_remaining / 60000);
                return min.ToString() + " minutes remaining until " + name;
            }
            else
            {
                long sec = Convert.ToInt64(total_remaining / 1000);
                return sec.ToString() + " seconds remaining until " + name;
            }
        }

    }

    public class PluginShared
    {

        public static object ParseXml(Type type, string file)
        {
            try
            {
                using (FileStream stream =
                       File.OpenRead( "data" +
                                     Path.DirectorySeparatorChar + file))
                {
                    XmlSerializer serializer = new XmlSerializer(type);
                    return serializer.Deserialize(stream);
                }
            }
            catch { return null; }
        }

		public static string GetUrl ( string uri )
        {
            ServicePointManager.ServerCertificateValidationCallback =
                Updater.CertValidator;
            HttpWebRequest request  =
                (HttpWebRequest)WebRequest.Create( uri );
            HttpWebResponse response =
                (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();

            byte[] read_buffer = new byte[8192];
            int count = 0;
            int total = 0;

            byte[] file_buffer = new byte[response.ContentLength];
            total = 0;
            do
            {
                count = resStream.Read( read_buffer, 0,
                                        read_buffer.Length );
                if (count != 0)
                {
                    Array.Copy( read_buffer, 0, file_buffer, total, count );
                    total += count;
                }
            }
            while (count > 0);
            return System.Text.Encoding.UTF8.GetString(file_buffer);
        }

        public static object ParseXmlUrl(Type type, string uri)
        {
            string xml = HTML.GetHtml(uri, 60000);
            if (string.IsNullOrEmpty(xml))
                return null;
            try
            {
                MemoryStream stream =
                    new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlSerializer serializer = new XmlSerializer(type);
                return serializer.Deserialize(stream);
            }
            catch { return null; }
        }

		public static string CreateWaypoint( int x, int y, int pfid )
        {
            return CreateWaypoint( x, y, pfid, string.Empty );
        }

        public static string CreateWaypoint( int x, int y, int pfid,
                                             string command )
        {
            if ( command == string.Empty )
                command = "Waypoint";
            string message = "<a href=\"text://<a href='chatcmd:///waypoint " +
                x.ToString() + " " +
                y.ToString() + " " +
                pfid.ToString() + "'>" + command + "</a>\">»»</a>";
            return message;
        }

        public static void AppendWaypoint( RichTextWindow window,
                                           int x, int y, int pfid,
                                           string command )
        {
            window.AppendCommand( command, "/waypoint " + x.ToString() +
                                  " " + y.ToString() +
                                  " " + pfid.ToString() );
        }

        public static void AppendLink( RichTextWindow window,
                                       string url, string link )
        {
            window.AppendRawString( String.Format(HTML.CommandLink, "'",
                                                  "/start " + url, "") );
            window.AppendRawString( link );
            window.AppendRawString( "</a>" );
        }

        public static int GetPFIDExact(string zone)
        {
            foreach (Playfield Playfield in PluginShared.GetPlayfields().List)
            {
                if (Playfield.Name == zone)
                {
                    return Convert.ToInt32(Playfield.PFID);
                }
            }
            return 0;
        }

    	public static string GetPFName ( int n )
        {
            return PluginShared.GetPFName (n, PluginShared.GetPlayfields());
        }

    	public static string GetPFName ( int n, Playfields PFs )
        {
            int i=0;
            for( i=0; i<PFs.List.Length; i++ )
                if ( n == Convert.ToInt32(PFs.List[i].PFID) )
                    return PFs.List[i].Name;
            return string.Empty;
        }

        public static int GetPFID(string zone)
        {

            int j=0;
            // construct some regexes
            string tmp = "^";
            for(j=0;j<zone.Length;j++)
            {
                if ( Char.IsLetter(zone[j]) )
                {
                    tmp += "[";
                    tmp += zone.ToLower()[j];
                    tmp += zone.ToUpper()[j];
                    tmp += "]";
                }
                else if ( Char.IsDigit( zone[j] ) )
                    tmp += zone[j];
                else
                    tmp += ".*";
            }
            tmp += ".*";
            Regex lss = new Regex ( tmp );
        
            tmp = String.Empty;
            for(j=0;j<zone.Length;j++)
            {
                if ( j > 0 )
                    tmp += "[ (-][";
                else
                    tmp += "^[";
                tmp += zone.ToLower()[j];
                tmp += zone.ToUpper()[j];
                tmp += "].*";
            }
            Regex abb = new Regex ( tmp, RegexOptions.Multiline );

            foreach (Playfield Playfield in PluginShared.GetPlayfields().List)
            {
                
                // zone match
                if ( lss.IsMatch ( Playfield.Name ) )
                    return Convert.ToInt32(Playfield.PFID);

                // abbreviation matching
                if ( ( Playfield.Name.IndexOf( " " ) >= 0 ) &&
                     abb.IsMatch( Playfield.Name ) )
                    return Convert.ToInt32(Playfield.PFID);

            }
            return 0;
        }

        public static Playfields GetPlayfields()
        {
            return (Playfields)PluginShared.ParseXml(typeof(Playfields),
                                                     "pfid.xml");
        }

        public static TimeZones GetTimeZones()
        {
            return (TimeZones)PluginShared.ParseXml(typeof(TimeZones),
                                                    "tz.xml");
        }

        public static string ParseProfession( string arg )
        {
            // parse prof
            switch ( arg )
            {
            case "adventurer":
            case "advy":
            case "adv":
                return "Adventurer";

            case "agent":
            case "age":
                return "Agent";

            case "bureaucrat":
            case "crat":
            case "bur":
                return "Bureaucrat";

            case "doctor":
            case "doc":
                return "Doctor";

            case "enforcer":
            case "enfo":
            case "enf":
                return "Enforcer";

            case "engineer":
            case "engi":
            case "engy":
            case "eng":
                return "Engineer";

            case "fixer":
            case "fix":
                return "Fixer";

            case "general":
            case "gen":
                return "General";

            case "keeper":
            case "keep":
            case "kep":
            case "kpr":
                return "Keeper";

            case "martial-artist":
            case "martial artist":
            case "mat":
            case "ma":
                return "Martial+Artist";

            case "meta-physicist":
            case "meta physicist":
            case "met":
            case "mp":
                return "Meta-Physicist";

            case "nano-technician":
            case "nano technician":
            case "nano":
            case "nan":
            case "nt":
                return "Nano-Technician";

            case "shade":
            case "sha":
                return "Shade";

            case "soldier":
            case "sold":
            case "sol":
                return "Soldier";

            case "trader":
            case "trad":
            case "trady":
            case "tra":
                return "Trader";
            }

            return string.Empty;
        }

    }

}
